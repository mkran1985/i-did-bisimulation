#include "Forest.h"
#include "IDID.h"

CForest::CForest(void)
{
}

CForest::~CForest(void)
{
}
void CForest::CalWeight(vector<int>& weight)
{
	map<int,int> hist;

	for (size_t i = 0; i < m_arrWeight.size(); i++)
	{
		hist[m_arrWeight.at(i)]++;
	}
	
	map<int,int>::iterator iter;
	for (iter = hist.begin(); iter!=hist.end(); ++iter)
	{
		weight.push_back(iter->second);
	}
}

vector<float>& CForest::GetPhyDist( size_t index )
{
	vector<int> indice = vec_FindSubVec(m_arrWeight,index);
	
	float r = (float)rand()/(float)RAND_MAX;
	int ind = static_cast<int>(r*indice.size());
	return m_arrPhyDist.at(ind); 
}

//combine the distributions whose distance is smaller than the given parameter
//let the mean value represent the distribution 
void CForest::DisCombination(Combination_Method method, float pare)
{
	vector<float> temp_dis;
	vector<int> item_num;
	float distance;
	bool flag = false;

	switch (method)// combination method
	{
	case Epsilon_Dis:// combine by distance
		for (size_t i = 0; i < m_arrPhyDist.size(); i++)
		{
			temp_dis = m_arrPhyDist.at(i);
	
			if (m_arrCombinedDist.size()>0)
			{
				//look at every 'point', if they are close enough, then combine them.
				for( size_t j = 0; j < m_arrCombinedDist.size(); j++)
				{
					//calculate the distance between the mean value and new distribution
					distance = cal_Distance(temp_dis,m_arrCombinedDist.at(j));
					if (sqrt(distance) < pare)
					{
						//calculate the new mean value of the distribution
						cal_mean(m_arrCombinedDist.at(j),temp_dis,item_num.at(j));
						item_num.at(j)=item_num.at(j)+1;
						flag=true;
						//we use 
						m_arrWeight.at(i) = m_arrIndofCombd.at(j);
						break;
					}
				}
				if (!flag)// a new distribution    flag == false, this point is far away from every exist point
				{
					m_arrCombinedDist.push_back(temp_dis);
					item_num.push_back(1);
					m_arrIndofCombd.push_back(i);
				}
				flag = false;
			}
			else// the first distribution
			{
				m_arrCombinedDist.push_back(temp_dis);
				item_num.push_back(1);
				m_arrIndofCombd.push_back(i);

			}
			temp_dis.clear();			
		}
		break;
	default:
		cout<<"Warning:[Forest] ---- unknown combine method for distributions\n";
	}

}