#include "IDID.h"
#include "BasDID.h"
#include "AdvDID.h"

//Bat Op means -> batch operation -> Pi Chu Li -> ������
//Evaluation procedure consisted of many operations,
//perhaps, this is could also be in the main function.

void EvaluateAllinOne(int times, string nSteps, string nPara,Merge_Method method);
void EvaluateAllinOne(int times, string nSteps, string nPara,Merge_Method method, string nAgents);
void EvaluateAllinOne(int nTimes, string nSteps, string nPara, Merge_Method method, string nAgents, string lambda, string epsilon_c);


//functions which i used as testing function during the development 
void DoEvaluation();
void testHugin();
void testDMU();
