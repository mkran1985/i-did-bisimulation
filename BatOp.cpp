#include "BatOp.h"
#include <iostream>
#include <string>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Cartesian_d.h>
#include <CGAL/barycenter.h>
#include <CGAL/Triangulation_data_structure.h>
#include <CGAL/internal/Combination_enumerator.h>
#include <CGAL/assertions.h>

#include "emd_hat.hpp"
#include "emd_hat_signatures_interface.hpp"
#include "tictoc.hpp"

#include <vector>
#include <utility>
#include <iterator>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <math.h>

double THRESHOLD = 3.0;
double EPSILON = 0.1;
double GAMMA = 0.9;
double cR = GAMMA;
double cT = 1-GAMMA;
static int SPLITCOUNT = 0;
bool checkBeliefTransition = false;
bool doMergeStep = true;

typedef float                      FT;
typedef CGAL::Simple_cartesian<FT>  K;
typedef K::Point_2                  Point_2;
typedef K::Point_3                  Point_3;

typedef CGAL::Cartesian_d<float>::Point_d                  Point_d;
//typedef CGAL::Cartesian_d<float>::

typedef pair<CBasDID*,vector<float> > beliefPoint;
typedef pair<beliefPoint,beliefPoint> pairOfBeliefPoints;
typedef pair<simpleBlock,simpleBlock> pairOfSimpleBlocks;
typedef pair<vector<float>, double> pairBeliefTransitionProb;
typedef pair<double,double> transProbs;
typedef vector<beliefPoint> block;
typedef vector<block> _partition;
//typedef vector<float> simpleBeliefPoint;
//typedef vector<simpleBeliefPoint> simpleBlock;
//typedef vector<simpleBlock> _simplePartition;

template< typename TDS >
void find_face_from_vertices(const TDS & tds,
        const std::vector<typename TDS::Vertex_handle> & face_vertices,
        typename TDS::Face & face);

template< typename TDS >
void barycentric_subdivide(TDS & tds, typename TDS::Full_cell_handle fc)
{ /* This function builds the barycentric subdivision of a single
     full cell |fc| from a triangulation data structure |tds|.  */
    typedef typename TDS::Vertex_handle Vertex_handle;
    typedef typename TDS::Face Face;
    const int dim = tds.current_dimension();

    // First, read handles to the cell's vertices
    std::vector<Vertex_handle> vertices;
    std::vector<Vertex_handle> face_vertices;
    for( int i = 0; i <= dim; ++i ) vertices.push_back(fc->vertex(i));

    // Then, subdivide the cell |fc| once by inserting one vertex
    tds.insert_in_full_cell(fc);
    // From now on, we can't use the variable |fc|...

    // Then, subdivide faces of |fc| in order of decreasing dimension
    for( int d = dim-1; d > 0; --d )
    {
        face_vertices.resize(d+1);
        // The following class
        // enumerates all (d+1)-tuple of the set {0, 1, ..., dim}
        CGAL::internal::Combination_enumerator combi(d+1, 0, dim);
        while( ! combi.end() )
        {
            for( int i = 0; i <= d; ++i )
                face_vertices[i] = vertices[combi[i]];
            // we need to find a face with face_vertices
            Face face(dim);
            find_face_from_vertices(tds, face_vertices, face);
            tds.insert_in_face(face);
            ++combi;
        }
    }
}

template< typename TDS >
void find_face_from_vertices( const TDS & tds,
        const std::vector<typename TDS::Vertex_handle> & face_vertices,
        typename TDS::Face & face)
{ /* The main goal of this function is to find a full cell that
     contains a given set of vertices |face_vertices|. Then, it
     builds a corresponding |face|. */
    typedef typename TDS::Vertex_handle           Vertex_handle;
    typedef typename TDS::Full_cell_handle        Full_cell_handle;
    typedef typename TDS::Full_cell::Vertex_handle_iterator Vertex_h_iterator;

    // get the dimension of the face we want to build
    std::size_t fdim(face_vertices.size() - 1);
    if( fdim <= 0) exit(-1);

    // find all full cells incident to the first vertex of |face|
    typedef std::vector<Full_cell_handle> Cells;
    Cells cells;
    std::back_insert_iterator<Cells> out(cells);
    tds.incident_full_cells(face_vertices[0], out);
    // Iterate over the cells to find one which contains the face_vertices
    for( typename Cells::iterator cit = cells.begin(); cit != cells.end(); ++cit){
        // find if the cell *cit contains the Face |face|
        std::size_t i = 0;
        for( ; i <= fdim; ++i ) {
            Vertex_handle face_v(face_vertices[i]);
            bool found(false);
            Vertex_h_iterator vit = (*cit)->vertices_begin();
            for( ; vit != (*cit)->vertices_end(); ++vit ) {
                if( *vit == face_v ) {
                    found = true;
                    break;
                }
            }
            if( ! found )
                break;
        }
        if( i > fdim ) {// the full cell |*cit| contains |face|
            face.set_full_cell(*cit);
            for( std::size_t i = 0; i <= fdim; ++i )
            {
              face.set_index(static_cast<int>(i),
                             (*cit)->index(face_vertices[i]));
            }
            return;
        }
    }
    std::cerr << "Could not build a face from vertices"<<std::endl;
    CGAL_assertion(false);
}


void split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

//vector<float> combine(vector<float> a, vector<float> b)
//{
//	vector<float> c;
//    for (int i = 0; i < a.size(); ++i ){
//        for (int j = 0; j < b.size(); ++j )
//            c.push_back( a[i] * pow(10,log10(b[j]) + 1) + b[j] );
//    }
//    return c;
//}
//
//vector<float> nDigit(vector<float> a, int n)
//{
//	vector<float> out;
//    out = combine(a, a);
//    for (int i = 1; i < n; ++i )
//        out = combine(a, out);
//    return out;
//}
//
//vector<int> combine(vector<int> a, vector<int> b)
//{
//	vector<int> c;
//    for (int i = 0; i < a.size(); ++i ){
//        for (int j = 0; j < b.size(); ++j )
//            c.push_back( a[i] * pow(10,log10(b[j]) + 1) + b[j] );
//    }
//    return c;
//}
//
//vector<int> nDigit(vector<int> a, int n)
//{
//	vector<int> out;
//    out = combine(a, a);
//    for (int i = 1; i < n; ++i )
//        out = combine(a, out);
//    return out;
//}

void printVector(vector<float> vec){
	for (int i = 0; i< vec.size(); ++i)
	    cout << vec[i] << ' ';
	cout<<endl;
}

void printVectorNoNewLine(vector<float> vec){
	cout<<"[ ";
	for (int i = 0; i< vec.size(); ++i)
	    cout << vec[i] << ' ';
	cout<<"]";
}

void printVector(vector<int> vec){
	for (int i = 0; i< vec.size(); ++i)
	    cout << vec[i] << ' ';
	cout<<endl;
}

void printVector(vector<beliefPoint> vec){
	for (int i = 0; i< vec.size(); ++i){
		cout<<vec[i].first<<" - ";
	    printVector(vec[i].second);
	}
	cout<<endl;
}

void printVectorOfVector(vector<vector<int> > vec){
	for (int i = 0; i< vec.size(); ++i)
		printVector(vec[i]);
}

void printVectorOfVector(vector<vector<float> > vec){
	for (int i = 0; i< vec.size(); ++i)
		printVector(vec[i]);
}

void printVectorOfVectorNoNewLine(vector<vector<float> > vec){
	cout<<"{ ";
	for (int i = 0; i< vec.size(); ++i){
		printVectorNoNewLine(vec[i]);
		cout<<" ";
	}
	cout<<"};";
}

vector<pair<CBasDID*,vector<float> > > GetBeliefPoints( vector<CBasDID*> bDIDArr){
	vector<beliefPoint> vecPairs;

	for(int k=0;k<bDIDArr.size();k++){

		CBasDID* bdid = bDIDArr[k];
		vector<float> arrFlt;
		vector<vector<float> > pArrDist;

		for(int i=0; i<bDIDArr[i]->m_iNbState;i++){
			pArrDist.push_back(arrFlt);
		}

//		ifstream ifile(paraFile.c_str());
//		string str;
//		vector<string> arrStr;

//		vector<vector<float> > pArrDist;
//		if (ifile.is_open())
//		{
//			while (!ifile.eof())
//			{
//				getline(ifile,str);
//				arrStr.clear();
//				stringsplit(str,arrStr,",");
//				arrFlt.clear();
//				str2num(arrStr,arrFlt);
//				pArrDist.push_back(arrFlt);
//			}
//		}
//		ifile.close();


//		for (int i=0;i<pArrDist.size();i++){
//			for (int j=0;j<pArrDist[i].size();j++){
//				cout << pArrDist[i][j] << " ";
//			}
//			cout<<endl;
//		}

		for (int i=0;i<pArrDist.size();i++){
			vecPairs.push_back(make_pair(bdid,pArrDist[i]));
		}
	}
	return vecPairs;
}

vector<pair<CBasDID*,vector<float> > > GetBeliefPoints( vector<string> fileParaArr, vector<CBasDID*> bDIDArr){
	vector<beliefPoint> vecPairs;

	for(int k=0;k<fileParaArr.size();k++){
		string paraFile = 	fileParaArr[k];
		CBasDID* bdid = bDIDArr[k];

		if (!IsFileExist(paraFile))
		{
			cout<<"ERROR:[BasDID] ---- where is the parameter file? \n";
			exit(0);
		}

		ifstream ifile(paraFile.c_str());
		string str;
		vector<string> arrStr;
		vector<float> arrFlt;
		vector<vector<float> > pArrDist;
		if (ifile.is_open())
		{
			while (!ifile.eof())
			{
				getline(ifile,str);
				arrStr.clear();
				stringsplit(str,arrStr,",");
				arrFlt.clear();
				str2num(arrStr,arrFlt);
				pArrDist.push_back(arrFlt);
			}
		}
		ifile.close();

//		for (int i=0;i<pArrDist.size();i++){
//			for (int j=0;j<pArrDist[i].size();j++){
//				cout << pArrDist[i][j] << " ";
//			}
//			cout<<endl;
//		}

		for (int i=0;i<pArrDist.size();i++){
			vecPairs.push_back(make_pair(bdid,pArrDist[i]));
		}
	}
	return vecPairs;
}

//vector<pair<CBasDID*,vector<float> > >*
void GetBeliefPnts( vector<string> fileParaArr, vector<CBasDID*> bDIDArr, vector<vector<beliefPoint> > &beliefPoints){


	for(int k=0;k<fileParaArr.size();k++){
//		cout<<"reading "<<fileParaArr[k]<<endl;
		vector<beliefPoint> vecPairs;
		string paraFile = 	fileParaArr[k];
		CBasDID* bdid = bDIDArr[k];

		if (!IsFileExist(paraFile))
		{
			cout<<"ERROR:[BasDID] ---- where is the parameter file? \n";
			exit(0);
		}

		ifstream ifile(paraFile.c_str());
		string str;
		vector<string> arrStr;
		vector<float> arrFlt;
		vector<vector<float> > pArrDist;
		if (ifile.is_open())
		{
			while (!ifile.eof())
			{
				getline(ifile,str);
				if(str!=""){
					arrStr.clear();
					stringsplit(str,arrStr,",");
					arrFlt.clear();
					str2num(arrStr,arrFlt);
					pArrDist.push_back(arrFlt);
				}
			}
		}
		ifile.close();

//		for (int i=0;i<pArrDist.size();i++){
//			for (int j=0;j<pArrDist[i].size();j++){
//				cout << pArrDist[i][j] << " ";
//			}
//			cout<<endl;
//		}

		for (int i=0;i<pArrDist.size();i++){
			vecPairs.push_back(make_pair(bdid,pArrDist[i]));
		}

		beliefPoints.push_back(vecPairs);

	}
//	return vecPairs;
}

//This will work for only 2 states.
bool isNeighbor(simpleBlock B, simpleBlock C){
	if(B.size() == 2){
		for(simpleBeliefPoint bp_B: B){
			for(simpleBeliefPoint bp_C: C){
				if(bp_B == bp_C){
					return true;
				}
			}
		}
	}else{
		cout<<"The isNeighbor method does not work for states > 2!"<<endl;
	}
	return false;
}

vector<pairOfBeliefPoints> listPairs(vector<beliefPoint> vecPairs, int n){
	int count = 0;
	vector<pairOfBeliefPoints> vecOfVecPairs;
	// Pick all elements one by one
	for (int i = 0; i < n; i++)
	{
		// See if there is a pair of this picked element
		for (int j = i+1; j < n; j++){
//			cout<<"[";
//			cout<<vecPairs[i].first<<" - ";
//			for(int k=0;k<vecPairs[i].second.size();k++){
//				cout<<vecPairs[i].second[k]<<",";
//			}
//			cout<<"] [";
//			cout<<vecPairs[j].first<<" - ";
//			for(int k=0;k<vecPairs[j].second.size();k++){
//				cout<<vecPairs[j].second[k]<<",";
//			}
//			cout<<"]"<<endl;
			vecOfVecPairs.push_back(make_pair(vecPairs[i],vecPairs[j]));
//			count++;
		}
	}


	return vecOfVecPairs;

}

//Return pairs of blocks that are neighbors
vector<pairOfSimpleBlocks> listPairs(_simplePartition P, int n){
	vector<pairOfSimpleBlocks> blockPairs;
	for (int i = 0; i < n; i++){
		for (int j = i+1; j < n; j++){
			simpleBlock B = P[i];
			simpleBlock C = P[j];
			if(isNeighbor(B,C)){
				blockPairs.push_back(make_pair(B,C));
			}
		}
	}
	return blockPairs;
}




//Given a belief point and its corresponding DID, compute Probability of selecting the belief reward R_b(b,a)
vector<NumberList> GetStochasticBeliefRewards(beliefPoint vecPair){
	vector<NumberList> stochBeliefRewards;
	NumberList beliefRewards;

	CBasDID* bdid  = vecPair.first;
	DiscreteNode *UNode;
	UNode = (DiscreteNode*)getNodeByLabel(bdid->m_pDomain,bdid->m_arrUNode.at(1));
	NumberList nbList = UNode->getTable()->getData();


	for(int a=0;a<vecPair.second.size();a++){
		cout<<"+ "<<vecPair.second[a]<<endl;
	}
	cout<<"done"<<endl;


	for(int a=0;a<bdid->m_iNbAction;a++){
		double temp = 0.0;
		for(int s=0;s<bdid->m_iNbState;s++){
			int index = bdid->getSerialRewardsIndex(s,a);
			temp += ((double)vecPair.second[s] * nbList[index]);
//			cout<<temp<<" = "<<(double)vecPair.second[s]<<" * "<<nbList[index]<<endl;
		}
		beliefRewards.push_back(temp);
//		cout<<beliefRewards.at(a)<<endl;
	}

	double maxR = *max_element(beliefRewards.begin(),beliefRewards.end());
	double minR = *min_element(beliefRewards.begin(),beliefRewards.end());

//	cout<<maxR<<endl;
//	cout<<minR<<endl;

	for(int a=0;a<bdid->m_iNbAction;a++){
		NumberList actionProbDist(2);
		double selectProb = (double)(beliefRewards[a] - minR)/(maxR-minR);
		actionProbDist[0] = selectProb;
		actionProbDist[1] = 1-selectProb;
		stochBeliefRewards.push_back(actionProbDist);
//		cout<<stochBeliefRewards.at(a)[0]<<","<<stochBeliefRewards.at(a)[1]<<endl;
	}
	return stochBeliefRewards;
}

//Given a belief point and its corresponding DID, compute Probability of selecting the belief reward R_b(b,a)
NumberList GetStochasticBeliefRewards(beliefPoint vecPair, int action){

	NumberList beliefRewards;

	CBasDID* bdid  = vecPair.first;
	DiscreteNode *UNode;
	UNode = (DiscreteNode*)getNodeByLabel(bdid->m_pDomain,bdid->m_arrUNode.at(1));
	NumberList nbList = UNode->getTable()->getData();


	for(int a=0;a<bdid->m_iNbAction;a++){
		double temp = 0.0;
		for(int s=0;s<bdid->m_iNbState;s++){
			int index = bdid->getSerialRewardsIndex(s,a);
			temp += ((double)vecPair.second[s] * nbList[index]);
//			cout<<temp<<" = "<<(double)vecPair.second[s]<<" * "<<nbList[index]<<endl;
		}
		beliefRewards.push_back(temp);
//		cout<<beliefRewards.at(a)<<endl;
	}

	double maxR = *max_element(beliefRewards.begin(),beliefRewards.end());
	double minR = *min_element(beliefRewards.begin(),beliefRewards.end());

	NumberList actionProbDist(2);
	double selectProb = (double)(beliefRewards[action] - minR)/(maxR-minR);
	actionProbDist[0] = selectProb;
	actionProbDist[1] = 1-selectProb;

	return actionProbDist;
}

vector<pairBeliefTransitionProb> GetStochasticBeliefTransitions(beliefPoint initialBelief, int action){
	vector<pairBeliefTransitionProb> beliefTransitions;

	vector<vector<float> > uniqueBeliefs;
	CBasDID* bdid  = initialBelief.first;

	int noObs = bdid->m_iNbObs;

	for(int obs = 0;obs<noObs;obs++){
		vector<float> upBelief;
		upBelief = bdid->GetUpdatedBelief(initialBelief.second,action,obs);

		if(uniqueBeliefs.empty()){
			uniqueBeliefs.push_back(upBelief);
		}else if((find(uniqueBeliefs.begin(), uniqueBeliefs.end(), upBelief) != uniqueBeliefs.end()) == false) {
			uniqueBeliefs.push_back(upBelief);
		}
	}
	for(int i = 0;i<uniqueBeliefs.size();i++){
		double transB = 0.;
		transB = bdid->GetBeliefTransitionProbability(initialBelief.second,action,uniqueBeliefs[i]);
		beliefTransitions.push_back(make_pair(uniqueBeliefs[i],transB));
	}

	//Sanity Check
	double sum=0.;
	for(int i=0;i<beliefTransitions.size();i++){
		sum += beliefTransitions[i].second;
	}
//	if(sum < 1.0){
//		cout<<"[SANITY CHECK FAILED] Belief Transitions must sum to 1. They currently sum to "<<sum<<endl;
//	}

//	for(int i=0;i<beliefTransitions.size();i++){
//		cout<<"Updated belief: ";
//		for(int a=0;a<beliefTransitions[i].first.size();a++){
//			cout<<beliefTransitions[i].first[a]<<",";
//		}
//		cout<<" = "<<beliefTransitions[i].second<<endl;
//	}

	return beliefTransitions;
}

//bool liesIn(vector<float> b, block C){
//	//works with 2 states; This means that block C will contain 2 boundary belief points;
//	//Sanity Check
//	if(C.size() != 2){
//		cout<<"Block C contains "<<C.size()<<" boundary beliefs. This function is insufficient!"<<endl;
//	}
//	if(b.size() != 2){
//		cout<<"Belief Point b is a distribution over "<<b.size()<<" physical states; This function is insufficient!"<<endl;
//	}
//	if((b[0] >= C[0].second[0]) && (b[0] <= C[1].second[0])){
//		return false;
//	}else{
//		return true;
//	}
//}

bool liesIn(vector<float> b, simpleBlock C){
	//works with 2 states; This means that block C will contain 2 boundary belief points;
	//Sanity Check
	if(C.size() != 2){
		cout<<"Block C contains "<<C.size()<<" boundary beliefs. This function is insufficient!"<<endl;
	}
	if(b.size() != 2){
		cout<<"Belief Point b is a distribution over "<<b.size()<<" physical states; This function is insufficient!"<<endl;
	}
	if((b[0] > C[0][0]) && (b[0] <= C[1][0])){
		return true;
	}else if((b[0] == 0) && (C[0][0] == 0) ){
		return true;
	}else{
		return false;
	}
}

//vector<beliefPoint> lieIn(vector<beliefPoint> beliefPoints, simpleBlock C){
//	vector<beliefPoint> beliefPointsInBlock;
//	for(int i=0;i<beliefPoints.size();i++){
//		if(liesIn(beliefPoints[i].second,C)){
//			beliefPointsInBlock.push_back(beliefPoints[i]);
//		}
//	}
//	return beliefPointsInBlock;
//}

//vector<simpleBeliefPoint> lieIn1(vector<beliefPoint> beliefPoints, simpleBlock C){
//	vector<simpleBeliefPoint> simpleBeliefPointsInBlock;
//	for(int i=0;i<beliefPoints.size();i++){
//		if(liesIn(beliefPoints[i].second,C)){
//			simpleBeliefPointsInBlock.push_back(beliefPoints[i].second);
//		}
//	}
//	return simpleBeliefPointsInBlock;
//}

void lieIn(vector<beliefPoint> beliefPoints, simpleBlock C, block& beliefPointsInBlock, simpleBlock& simpleBeliefPointsInBlock){
	for(int i=0;i<beliefPoints.size();i++){
		if(liesIn(beliefPoints[i].second,C)){
			beliefPointsInBlock.push_back(beliefPoints[i]);
			simpleBeliefPointsInBlock.push_back(beliefPoints[i].second);
		}
	}
}

vector<pairBeliefTransitionProb> GetStochasticBeliefTransitions(beliefPoint initialBelief, int action, simpleBlock C){
	vector<pairBeliefTransitionProb> beliefTransitions;

	vector<vector<float> > uniqueBeliefs;
	CBasDID* bdid  = initialBelief.first;

	int noObs = bdid->m_iNbObs;

	for(int obs = 0;obs<noObs;obs++){
		vector<float> upBelief;
		upBelief = bdid->GetUpdatedBelief(initialBelief.second,action,obs);
//		cout<<"Obs: "<<obs<<endl;
//		printVector(upBelief);

		if(liesIn(upBelief,C) == false){
			continue;
		}

		if(uniqueBeliefs.empty()){
			uniqueBeliefs.push_back(upBelief);
		}else if((find(uniqueBeliefs.begin(), uniqueBeliefs.end(), upBelief) != uniqueBeliefs.end()) == false) {
			uniqueBeliefs.push_back(upBelief);
		}
	}
//	cout<<"Unique beliefs (Size: "<<uniqueBeliefs.size()<<")"<<endl;
//	printVectorOfVector(uniqueBeliefs);

	for(int i = 0;i<uniqueBeliefs.size();i++){
		double transB = 0.;
		transB = bdid->GetBeliefTransitionProbability(initialBelief.second,action,uniqueBeliefs[i]);
//		cout<<"Belief transition probability["<<i<<"] "<<transB<<endl;
		beliefTransitions.push_back(make_pair(uniqueBeliefs[i],transB));
	}

	//Sanity Check
	double sum=0.;
	for(int i=0;i<beliefTransitions.size();i++){
		sum += beliefTransitions[i].second;
	}
//	if(sum < 1.0){
//		cout<<"[SANITY CHECK FAILED] Belief Transitions must sum to 1. They currently sum to "<<sum<<endl;
//	}

//	for(int i=0;i<beliefTransitions.size();i++){
//		cout<<"Updated belief: ";
//		for(int a=0;a<beliefTransitions[i].first.size();a++){
//			cout<<beliefTransitions[i].first[a]<<",";
//		}
//		cout<<" = "<<beliefTransitions[i].second<<endl;
//	}

	return beliefTransitions;
}

vector<vector<double> > GetCostMatrix(vector<double> b1,vector<double> b2,double threshold){
	vector<vector<double> > cost_mat;
	for(int i=0;i<b1.size();i++){
		vector<double> cmrow(b2.size());
		for(int j=0;j<b2.size();j++){
			double abs_diff = b1.at(i)-b2.at(j);
			if(abs_diff < 0){
				abs_diff = b2.at(j)-b1.at(i);
			}
			if(abs_diff < threshold){
				cmrow[j]= abs_diff;
			}else{
				cmrow[j]= threshold;
			}
		}
		cost_mat.push_back(cmrow);
	}
	return cost_mat;
}

double GetEMD(vector<double> b1_a, vector<double> b2_a, double threshold){
//	cout<<"inside emd computation function.."<<endl;
	tictoc timer;
	double emd1 = 0.;
//	double emd2 = 0.;

	vector<vector<double> > cost_mat = GetCostMatrix(b1_a,b2_a,threshold);

//	cout<<"Finished computing cost matrix.."<<endl;

	timer.tic();
		emd1 = emd_hat_gd_metric<double>()(b1_a,b2_a,cost_mat,threshold);
	timer.toc();
//	cout <<"EMD1 = "<<emd1<<endl<< "emd_hat_gd_metric time in seconds: " << timer.totalTimeSec() << std::endl;

	timer.clear();

//	timer.tic();
//		emd2 = emd_hat<double>()(b1_a,b2_a,cost_mat,threshold);
//	timer.toc();
//	cout <<"EMD2 = "<<emd2<<endl<< "emd_hat time in seconds: " << timer.totalTimeSec() << std::endl;

	return emd1;
}

double GetEMDforAction(beliefPoint belief1, beliefPoint belief2, int actionIndex1, int actionIndex2, double threshold){
	double emd4action;

	vector<double> b1_rew = GetStochasticBeliefRewards(belief1,actionIndex1);
	vector<double> b2_rew = GetStochasticBeliefRewards(belief2,actionIndex2);

	double d_rew_a = GetEMD(b1_rew, b2_rew, threshold);

	vector<double> b1_trans, b2_trans;

//	cout<<"Agent 1:"<<endl;
	vector<pairBeliefTransitionProb> b1_trans_temp = GetStochasticBeliefTransitions(belief1,actionIndex1);
//	cout<<"Agent 2:"<<endl;
	vector<pairBeliefTransitionProb> b2_trans_temp = GetStochasticBeliefTransitions(belief2,actionIndex2);

	map<vector<float>, transProbs > uniqueBeliefs;
	for(int i=0;i<b1_trans_temp.size();i++){
		uniqueBeliefs.insert(make_pair(b1_trans_temp[i].first,make_pair(b1_trans_temp[i].second,0.)));
	}

	for(int i=0;i<b2_trans_temp.size();i++){
//		cout<<"uniqueBeliefs.find(b2_trans_temp[i].first) = "<<endl;
//		for(int j=0;j<b2_trans_temp[i].first.size();j++){
//			cout<<" "<<b2_trans_temp[i].first[j];
//		}
//		cout<<" Tbab = "<<b2_trans_temp[i].second<<endl;
		if(uniqueBeliefs.find(b2_trans_temp[i].first) == uniqueBeliefs.end()){
			//not found
			uniqueBeliefs.insert(make_pair(b2_trans_temp[i].first,make_pair(0.,b2_trans_temp[i].second)));
		}else{
			//found
			uniqueBeliefs[b2_trans_temp[i].first].second = b2_trans_temp[i].second;

		}
	}

//	cout<<b1_trans_temp.size()<<" "<<b2_trans_temp.size()<<" "<<uniqueBeliefs.size()<<endl;
	for(map<vector<float>, transProbs>::iterator it=uniqueBeliefs.begin(); it!=uniqueBeliefs.end(); ++it){
//		cout<<"["<<it->first[0]<<","<<it->first[1]<<"] = "<<it->second.first<<","<<it->second.second<<endl;
		b1_trans.push_back(it->second.first);
		b2_trans.push_back(it->second.second);
	}

//	for(int i =0;i<b1_trans.size();i++){
//		cout<<b1_trans[i]<<","<<b2_trans[i]<<endl;
//	}

//	double d_trans_a = GetEMD(b1_trans, b2_trans, threshold);
	double d_trans_a;

	if(b1_trans == b2_trans){
		d_trans_a = 0;
	}else{
		d_trans_a = GetEMD(b1_trans, b2_trans, threshold);
	}

	emd4action = cR * d_rew_a + cT * d_trans_a;

//	cout<<"[ai="<<actionIndex1<<" aj="<<actionIndex2<<"] d_rew = "<<d_rew_a<<"; d_trans = "<<d_trans_a<<" d_full = "<<emd4action<<endl;;

	return emd4action;
}

double GetEMDforAction(beliefPoint belief1, beliefPoint belief2, int actionIndex1, int actionIndex2, simpleBlock C, double threshold){
	double emd4action;

	vector<double> b1_rew = GetStochasticBeliefRewards(belief1,actionIndex1);
	vector<double> b2_rew = GetStochasticBeliefRewards(belief2,actionIndex2);

	double d_rew_a = GetEMD(b1_rew, b2_rew, threshold);

	vector<double> b1_trans, b2_trans;

//	cout<<"Agent 1:"<<endl;
	vector<pairBeliefTransitionProb> b1_trans_temp = GetStochasticBeliefTransitions(belief1,actionIndex1, C);
//	cout<<"Agent 2:"<<endl;
	vector<pairBeliefTransitionProb> b2_trans_temp = GetStochasticBeliefTransitions(belief2,actionIndex2, C);

	map<vector<float>, transProbs > uniqueBeliefs;
	for(int i=0;i<b1_trans_temp.size();i++){
		uniqueBeliefs.insert(make_pair(b1_trans_temp[i].first,make_pair(b1_trans_temp[i].second,0.)));
	}

	for(int i=0;i<b2_trans_temp.size();i++){
//		cout<<"uniqueBeliefs.find(b2_trans_temp[i].first) = "<<endl;
//		for(int j=0;j<b2_trans_temp[i].first.size();j++){
//			cout<<" "<<b2_trans_temp[i].first[j];
//		}
//		cout<<" Tbab = "<<b2_trans_temp[i].second<<endl;
		if(uniqueBeliefs.find(b2_trans_temp[i].first) == uniqueBeliefs.end()){
			//not found
			uniqueBeliefs.insert(make_pair(b2_trans_temp[i].first,make_pair(0.,b2_trans_temp[i].second)));
		}else{
			//found
			uniqueBeliefs[b2_trans_temp[i].first].second = b2_trans_temp[i].second;

		}
	}

//	cout<<b1_trans_temp.size()<<" "<<b2_trans_temp.size()<<" "<<uniqueBeliefs.size()<<endl;
	for(map<vector<float>, transProbs>::iterator it=uniqueBeliefs.begin(); it!=uniqueBeliefs.end(); ++it){
//		cout<<"["<<it->first[0]<<","<<it->first[1]<<"] = "<<it->second.first<<","<<it->second.second<<endl;
		b1_trans.push_back(it->second.first);
		b2_trans.push_back(it->second.second);
	}

//	for(int i =0;i<b1_trans.size();i++){
//		cout<<b1_trans[i]<<","<<b2_trans[i]<<endl;
//	}

//	double d_trans_a = GetEMD(b1_trans, b2_trans, threshold);
	double d_trans_a;

	if(b1_trans == b2_trans){
		d_trans_a = 0;
	}else{
		d_trans_a = GetEMD(b1_trans, b2_trans, threshold);
	}

	emd4action = cR * d_rew_a + cT * d_trans_a;

//	cout<<"[ai="<<actionIndex1<<" aj="<<actionIndex2<<"] d_rew = "<<d_rew_a<<"; d_trans = "<<d_trans_a<<" d_full = "<<emd4action<<endl;;

	return emd4action;
}

double GetEMD(beliefPoint belief1, beliefPoint belief2, double threshold){
	double maxEMD = -1.;
	vector<vector<double> > emd;

	//when all agents have the same action set
	for(int a=0;a<belief1.first->m_iNbAction;a++){
		double temp;
		temp = GetEMDforAction(belief1,belief2,a,a,threshold);
//			cout<<"ai="<<ai<<" aj="<<aj<<" EMD="<<temp<<endl;
		if(temp>maxEMD){
			maxEMD=temp;
		}
	}

//	for(int ai=0;ai<belief1.first->m_iNbAction;ai++){
//		double temp;
//		for(int aj=0;aj<belief2.first->m_iNbAction;aj++){
//			temp = GetEMDforAction(belief1,belief2,ai,aj,threshold);
////			cout<<"ai="<<ai<<" aj="<<aj<<" EMD="<<temp<<endl;
//			if(temp>maxEMD){
//				maxEMD=temp;
//			}
//		}
//	}

	//cout<<"Distance: "

	return maxEMD;
}

double GetEMD(beliefPoint belief1, beliefPoint belief2, simpleBlock C, double threshold){
	double maxEMD = -1.;
	vector<vector<double> > emd;

	//when all agents have the same action set
	for(int a=0;a<belief1.first->m_iNbAction;a++){
		double temp;
		temp = GetEMDforAction(belief1,belief2,a,a,C,threshold);
//			cout<<"ai="<<ai<<" aj="<<aj<<" EMD="<<temp<<endl;
		if(temp>maxEMD){
			maxEMD=temp;
		}
	}

//	for(int ai=0;ai<belief1.first->m_iNbAction;ai++){
//		double temp;
//		for(int aj=0;aj<belief2.first->m_iNbAction;aj++){
//			temp = GetEMDforAction(belief1,belief2,ai,aj,threshold);
////			cout<<"ai="<<ai<<" aj="<<aj<<" EMD="<<temp<<endl;
//			if(temp>maxEMD){
//				maxEMD=temp;
//			}
//		}
//	}

	return maxEMD;
}


void printBin(vector<float> soFar, int iterations, vector<vector<float> > &final) {

	if(iterations == 0) {
//    	printVector(soFar);
    	final.push_back(soFar);
//    	return final;
    }
    else {
    	soFar.push_back(0.);
        printBin(soFar, iterations - 1, final);
        soFar.pop_back();
        soFar.push_back(1.);
        printBin(soFar, iterations - 1, final);
        soFar.pop_back();
    }

//    return final;
}

vector<vector<float> > printBin(int iterations) {
	vector<float> temp;
	vector<vector<float> > final;
	printBin(temp,iterations, final);
	return final;
}

vector<vector<float> > filterValidBeliefs(vector<vector<float> > temp) {
	vector<vector<float> > final;
	for(int i=0;i<temp.size();i++){
		int sum = 0;
		for(int s=0;s<temp[i].size(); s++){
			sum+=temp[i][s];
		}
		if(sum < 1 || sum > 1){
			continue;
		}else{
			final.push_back(temp[i]);
		}
	}
	return final;
}

_simplePartition getInitialPartition(int numStates){
	vector<vector<float> > temp = printBin(numStates);
	simpleBlock boundaryBeliefs = filterValidBeliefs(temp);

	_simplePartition initP(1);
	initP[0] = boundaryBeliefs;
	return initP;
}


// Works only on domains containing 2 states
void barycentric_subdivision(simpleBlock B, vector<simpleBlock> &splitB){

	vector<pair<Point_2, FT> > points_2;
	vector<pair<Point_3, FT> > points_3;

//	cout<<"[BS] Block size: "<<splitB.size()<<endl;
//	cout<<"[BS] Num of Belief Points in B: "<<B.size()<<endl;

	for(int i=0;i<B.size();i++){
		if(B[i].size() == 2){
			points_2.push_back(make_pair(Point_2(B[i][0], B[i][1]),  1.0));
		}else if(B[i].size() == 3){
			points_3.push_back(make_pair(Point_3(B[i][0], B[i][1], B[i][2]),  1.0));
		}else{
			cout<<"Cannot handle more than 3 states.."<<endl;
		}
	}

	Point_2 c2 = CGAL::barycenter(points_2.begin(), points_2.end());
//	cout<<"[BS] Barycenter = "<<c2<<endl;

	simpleBlock B1(B.size()),B2(B.size());

	if(B.size() == 2){
		B1[0] = B[0];
//		printVector(B1[0]);
		simpleBeliefPoint c(2);
		for(int i=0;i<2;i++){
			c[i] = c2[i];
		}
		B1[1] = c;
		B2[0] = c;
		B2[1] = B[1];
	}

	splitB[0] = B1;
	splitB[1] = B2;

}

void printPartition(vector<vector<vector<float> > > P){
	//Number of blocks
	cout<<"[BEGIN Partition --- ";
	for(int i=0;i<P.size();i++){
		//Number of belief points per block
		cout<<"{";
		for(int j=0;j<P[i].size();j++){
			//Number of states per belief point
//			simpleBlock sb(P[i][j].size());
//			sb = P[i][j];
			cout<<"(";
			for(int k=0;k<P[i][j].size();k++){
				cout<<P[i][j][k];
				if(k != P[i][j].size()-1) cout<<",";
			}
			cout<<")";
			if(j != P[i].size()-1) cout<<" ";
		}
		cout<<"}";
		if(i != P.size()-1) cout<<" ";

		if(i%5 == 0 && i>0){
			cout<<endl;
		}
	}
	cout<<" --- END Partition]"<<endl;
}

// Check and split block B if it is not stable with respect to block C and return the resulting partition
void split(simpleBlock B, _simplePartition &P){
//	if(checkStability(B,C) == false){
//	cout<<"[SPLIT] Partition size: "<<P.size()<<endl;
		P.erase(remove(P.begin(),P.end(),B),P.end());
//		cout<<"[SPLIT] New Partition size: "<<P.size()<<endl;
		vector<simpleBlock> splitB(2);
		barycentric_subdivision(B,splitB);
//		cout<<"[SPLIT] barycentric_subdivision: "<<splitB.size()<<endl;
		P.insert(P.end(),splitB.begin(),splitB.end());
		SPLITCOUNT++;
//		printPartition(P);
		cout<<SPLITCOUNT<<" "<<flush;
		if(SPLITCOUNT%10==0){
			cout<<endl;
		}
//	}
}

block makeBlockFromSimpleBlock(simpleBlock sb, vector<CBasDID*> bDIDArr){
	vector<beliefPoint> b;
	for(int i=0;i<bDIDArr.size();i++){
		for(int j=0;j<sb.size();j++){
			b.push_back(make_pair(bDIDArr[i],sb[j]));
		}
	}
	return b;
}

bool isNormStable(block B){
	vector<pairOfBeliefPoints> pairOfBeliefs = listPairs(B, B.size());
	double dist;
	for(int bp=0;bp<pairOfBeliefs.size();bp++){
		double normalizedEpsilon = (EPSILON * abs(pairOfBeliefs[bp].first.second[0] - pairOfBeliefs[bp].second.second[0]));
		cout<<"Normalized Epsilon = "<<normalizedEpsilon<<endl;
		dist = GetEMD(pairOfBeliefs[bp].first,pairOfBeliefs[bp].second,THRESHOLD);
		if(dist > normalizedEpsilon){
			return false;
		}
	}
//	cout<<"Distance (EMD) = "<<dist<<endl;
	return true;
}

bool isStable(block B){
	vector<pairOfBeliefPoints> pairOfBeliefs = listPairs(B, B.size());
	double dist;
	for(int bp=0;bp<pairOfBeliefs.size();bp++){
		dist = GetEMD(pairOfBeliefs[bp].first,pairOfBeliefs[bp].second,THRESHOLD);
		if(dist > EPSILON){
			return false;
		}
	}
//	cout<<"Distance (EMD) = "<<dist<<endl;
	return true;
}

bool isStable(block B, simpleBlock C){
	vector<pairOfBeliefPoints> pairOfBeliefs = listPairs(B, B.size());
	double dist;
	for(int bp=0;bp<pairOfBeliefs.size();bp++){
		dist = GetEMD(pairOfBeliefs[bp].first,pairOfBeliefs[bp].second,C,THRESHOLD);
		if(dist > EPSILON){
			return false;
		}
	}
//	cout<<"Distance (EMD) = "<<dist<<endl;
	return true;
}

//Not used for now
bool checkStability(vector<CBasDID*> bDIDArr, simpleBlock B, simpleBlock C){
	block blockB = makeBlockFromSimpleBlock(B,bDIDArr);
	return isStable(blockB,C);
}


bool checkStability(vector<CBasDID*> bDIDArr, simpleBlock B){
	block blockB = makeBlockFromSimpleBlock(B,bDIDArr);
	return isStable(blockB);
}

//Not used for now
bool checkStability(vector<CBasDID*> bDIDArr, simpleBlock B, _simplePartition P){
	for(int p=0;p<P.size();p++){
//		cout<<"[CS_block_partition] Im here " <<p<<endl;
		simpleBlock C = P[p];
		if(checkStability(bDIDArr,B,C) == false)
			return false;
	}
	return true;
}

void checkStability(vector<CBasDID*> bDIDArr,_simplePartition &P){
	for(int p=0;p<P.size();p++){
//		cout<<"[CS_partition] Im here "<<endl;
		simpleBlock B = P[p];
		if(checkBeliefTransition == false){
			if(checkStability(bDIDArr,B) == false){
				split(B,P);
				checkStability(bDIDArr,P);
			}
		}else{
			if(checkStability(bDIDArr,B,P) == false){
				split(B,P);
				checkStability(bDIDArr,P);
			}
		}
	}
}

_simplePartition getBisimulationPartition(vector<CBasDID*> bDIDArr){
	_simplePartition P;
	int numAgents = bDIDArr.size();
	int numStates = bDIDArr[0]->m_iNbState;

	//Initializing the trivial 1 block partition
	P = getInitialPartition(numStates);

//	cout<<P.size()<<" = Initial Partition Size"<<endl;
//	printPartition(P);
	simpleBlock B = P[0];
	cout<<"Split #: ";
	split(B,P);
//	printPartition(P);
	checkStability(bDIDArr,P);
	cout<<endl;
	printPartition(P);
	cout<<"\tFinal Partition Size: "<<P.size()<<endl;
	return P;
}



simpleBlock mergeSimpleBlocks(simpleBlock B, simpleBlock C){
	simpleBlock merged;
	if(B.size() !=2 && C.size() !=2){
		cout<<"Invalid block size in mergeSimpleBlocks: "<<B.size()<<" "<<C.size()<<endl;
		exit(0);

	}
	if(B[0][0] <= C[0][0]){
		merged.push_back(B[0]);
		merged.push_back(C[1]);
	}else{
		merged.push_back(C[0]);
		merged.push_back(B[1]);
	}
	return merged;
}


_simplePartition doMerge(_simplePartition bisimulation, vector<CBasDID*> bDIDArr){
	cout<<"Applying the Merge step on the stochastic bisimulation partition.."<<endl;
	cout<<"---------------------------------------------------"<<endl;
	int step=0;
	for(int i=0; i<bisimulation.size(); i++){
		if(_IDID_DEBUG_) cout<<"[Merge Step "<<step<<"] Current Partition Size: "<<bisimulation.size()<<endl;
		for(int j=i+1; j<bisimulation.size(); j++){
			simpleBlock b = bisimulation[i];
			simpleBlock c = bisimulation[j];
			if(isNeighbor(b,c)){
				simpleBlock m = mergeSimpleBlocks(b,c);
				block blockM = makeBlockFromSimpleBlock(m, bDIDArr);
				step++;
				if(isStable(blockM)){
					if(_IDID_DEBUG_) {
						cout<<"\t";
						printVectorOfVectorNoNewLine(m);
						cout<<" -> merged and stable!"<<endl;
					}
					bisimulation[i] = m;
					bisimulation.erase(bisimulation.begin()+j);
					i=0;
					break;
				}else{
					if(_IDID_DEBUG_) {
						cout<<"\t";
						printVectorOfVectorNoNewLine(m);
						cout<<" -> NOT merged and stable!"<<endl;
					}
				}
			}
		}
	}
	return bisimulation;
}

_simplePartition doMerge1(_simplePartition bisimulation, vector<CBasDID*> bDIDArr){
	vector<pairOfSimpleBlocks> simpleBlockPairs = listPairs(bisimulation,bisimulation.size());
	for(int sbp=0;sbp<simpleBlockPairs.size();sbp++){
		simpleBlock sb_merge = mergeSimpleBlocks(simpleBlockPairs[sbp].first,simpleBlockPairs[sbp].second);
		block b_merge = makeBlockFromSimpleBlock(sb_merge, bDIDArr);
	}
	return bisimulation;
}

//void printcombos(const vector<vector<string> >&vec,vector<int>&index,int depth) {
//  if(depth==index.size()) {
//    for(int i=0; i<depth; ++i) {
//      cout<<vec[i][index[i]];
//    }
//    cout<<endl;
//  } else {
//    const vector<string> &myvec= vec[depth];
//    int mylength= myvec.size();
//    for(int i=0; i<mylength; ++i) {
//      index[depth]=i;
//      printcombos(vec,index,depth+1);
//    }
//  }
//}

void EvaluateAllinOne(int nTimes, string nSteps, string nPara, Merge_Method method, string nAgents, string lambda, string epsilon_c){
	string line = "-----------------------------------------------";
	string smallline = "------------";

	cout<<line<<endl;




	string fileIDID="";

	clock_t start,final;

	/**
	 * m_iNbOthers : number of other agents
	 */
	int m_iNbOthers = atoi(nAgents.c_str())-1;

	vector<string> fileDIDArr(m_iNbOthers,"");
	vector<string> fileParaArr(m_iNbOthers,"");
	vector<float> res;

	EPSILON = atof(epsilon_c.c_str());
	cout<<"Epsilon = "<<EPSILON<<endl;

	/**
	 * fileDIDArr : take in a vector of DIDs of others (at level 0)
	 * eg: T03.net, T13.net for 3 agents (0,1 are indices of the other agents) and 3 steps
	 **/
	for(int i=0;i<m_iNbOthers;i++){
		stringstream ss;
		ss << "Debug/DID_Model/T" << i << nSteps << ".net";
		fileDIDArr[i] = ss.str();
		cout<<"DID (Other Agent "<<i<<"): "<<fileDIDArr[i]<<endl;
	}

	stringstream ss;
	ss << "Debug/IDID_Model/TIDID" << nSteps << "_" << nAgents.c_str() << "N.net";
	fileIDID = ss.str();
	cout<<"IDID (Subject Agent): "<<fileIDID<<endl;

	/**
	 * nParaArr : vector of parameter file names of others'
	 *            level 0 beliefs (over physical states) separated by commas
	 * eg: 'para0,para1' for 2 other agents
	 **/
	vector<string> nParaArr = split(nPara,',');
	for(int i=0;i<m_iNbOthers;i++){
		fileParaArr[i] = "Debug/para/" + nParaArr[i] + ".txt";
		cout<<"Parameter file (Other Agent "<<i<<"): "<<fileParaArr[i]<<endl;
	}
	cout<<line<<endl<<endl;

	cout<<"Instantiating decision networks for agents.."<<endl;
	cout<<line<<endl;

	/**
	 * bDIDArr : vector of CBasDID objects; one for each other agent
	 */
	vector<CBasDID*> bDIDArr(m_iNbOthers);
	for(int i=0;i<m_iNbOthers;i++){
		cout <<"\tInstantiating "<< fileDIDArr[i] <<".."<< endl;
		bDIDArr[i]=new CBasDID(fileDIDArr[i]);
	}
	cout <<"\tInstantiating "<< fileIDID <<".."<< endl;
	CAdvDID advDID(fileIDID, m_iNbOthers);
	cout<<"Finished instantiating decision networks for all agents.."<<endl<<endl;

	cout<<"Computing the stochastic bisimulation partition of the belief simplex.."<<endl;
	cout<<line<<endl;

	start = clock();
	_simplePartition beforeMerge = getBisimulationPartition(bDIDArr);
	final = clock();
	cout<<"\tThe time consumed for computing the stochastic bisimulation partition is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl<<endl;

	//Merge Step
	_simplePartition bisimulation = beforeMerge;
	if(doMergeStep){
		start = clock();
		bisimulation.clear();
		bisimulation = doMerge(beforeMerge, bDIDArr);
		printPartition(bisimulation);
		cout<<"\tFinal Partition Size: "<<bisimulation.size()<<endl;
		final = clock();
		cout<<"\tThe time consumed for the merge step is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl<<endl;

		//Sanity Check
		if(_IDID_DEBUG_) {
			checkStability(bDIDArr,bisimulation);
			cout<<endl;
			printPartition(bisimulation);
			cout<<"\tFinal Partition Size (Sanity Check): "<<bisimulation.size()<<endl;
		}
	}
	cout<<"Grouping the candidate models of each agent (from Parameter file) according to the bisimulation partition.."<<flush;

	//for each agent, vector of <frame,beliefpoint>
	vector<vector<beliefPoint> > beliefPoints;
	GetBeliefPnts(fileParaArr,bDIDArr,beliefPoints);

	//for each agent, store block ID and the belief points that lie in that block
	vector<map<int,vector<beliefPoint> > > mapArr(m_iNbOthers);
	vector<map<int,vector<simpleBeliefPoint> > >mapSimpleArr(m_iNbOthers);

	for(int i=0;i<m_iNbOthers;i++){
		for(int b=0;b<bisimulation.size();b++){
			simpleBlock sb = bisimulation[b];
			vector<beliefPoint> beliefPointsInBlock;
			vector<simpleBeliefPoint> simpleBeliefPointsInBlock;
			lieIn(beliefPoints[i],sb,beliefPointsInBlock,simpleBeliefPointsInBlock);
			mapArr[i].insert(pair<int,vector<beliefPoint> >(b,beliefPointsInBlock));
			mapSimpleArr[i].insert(pair<int,vector<simpleBeliefPoint> >(b,simpleBeliefPointsInBlock));
		}
	}

	cout<<"done!"<<endl<<endl;


//	for(int i=0;i<m_iNbOthers;i++){
//		for(int b=0;b<bisimulation.size();b++){
//			cout<<"Block "<<b<<":"<<endl;
//			vector<vector<float> > sb = bisimulation[b];
//			printVectorOfVector(sb);
//			vector<beliefPoint> beliefPointsInBlock = mapArr[i].at(b);
//			printVector(beliefPointsInBlock);
//		}
//	}


	map<int,CSequenceTree*> mapSoln;
	cout<<"Solving 1 candidate model per block in the bisimulation partition.."<<endl;
	cout<<line<<endl;
//	cout<<"Number of models solved [begin]: "<<mapSoln.size()<<endl;

	start = clock();
	for(int i=0;i<m_iNbOthers;i++){
//		cout<<"Agent "<<i<<":"<<endl;
//		cout<<smallline<<endl;
		for(int b=0;b<bisimulation.size();b++){
			CSequenceTree* pTree;
//			cout<<"Block "<<b<<":"<<endl;
			if (mapSoln.find(b) == mapSoln.end() ) {
				if(!mapArr[i].at(b).empty()){
					pTree = bDIDArr[i]->SolveDID(mapArr[i].at(b)[0].second);
					mapSoln.insert(pair<int,CSequenceTree*>(b,pTree));
				}else{
//					cout<<"\tAgent "<<i<<" has no candidate model in Block "<<b<<endl;
				}
			} else {
//			  cout<<"\tSolution for this block already found, so not solving again.."<<endl;
			}
		}
	}


	//expand mapSimpleArr
	cout<<"Passing the obtained policy trees to create the forest .."<<flush;
	vector<vector<CSequenceTree*> > pTreesArr;
	for(int i=0;i<m_iNbOthers;i++){
		vector<CSequenceTree*> pTrees;
//		cout<<"Agent "<<i<<" has "<<mapArr[i].size()<<" blocks!"<<endl;
		for(int j=0;j<mapArr[i].size();j++){
//			cout<<"Block "<<j<<" has "<<mapArr[i].at(j).size()<<" belief points. Thus there'll be "<<mapArr[i].at(j).size()<<" repeating policies"<<endl;
			for(int k=0;k<mapArr[i].at(j).size();k++){
				pTrees.push_back(mapSoln.at(j));
//				cout<<i<<"-"<<j<<"-"<<k<<"->"<<mapSoln.at(j)<<endl;
			}
		}
		pTreesArr.push_back(pTrees);
	}

	vector<CForest *> pForestArr(m_iNbOthers);
	for(int i=0;i<m_iNbOthers;i++){
		pForestArr.push_back(NULL);
		bDIDArr[i]->SolveDID(fileParaArr[i], &pForestArr[i], pTreesArr[i], DMU);
		cout<<"****************"<<endl;
//		for(int k=0;k<pForestArr[i]->GetWeight().size();k++){
//			cout<<" "<<pForestArr[i]->GetWeight()[k];
//			cout<<" "<<pForestArr[i]->GetMap(0,0,0);
//		}
		((CGraphForest*) pForestArr[i])->Print();
		cout<<endl;
		cout<<"****************"<<endl;
	}
	cout<<"done!"<<endl;

	final = clock();
	cout<<"\tTotal number of other agents' models solved: "<<mapSoln.size()<<endl;
	cout<<"\tThe time consumed for solving "<<mapSoln.size()<<" models is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl;
	cout<<"Finished solving level-0 DIDs for other agents.."<<endl<<endl;

	cout<<"Solving level-1 model of the subject agent.."<<endl;
	cout<<line<<endl;

	start = clock();
	advDID.ExpandModelwithForest(pForestArr,"Debug/Expanded_DID_Model/test.net");
	final = clock();
	cout<<"\tThe time consumed for expanding the I-DID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl;
	cout<<"Finished expanding the I-DID over time.. "<<endl<<endl;
//	advDID.m_pDomain->compile();
	cout<<"Evaluating Subject Agent's I-DID blah"<<endl;
	cout<<line<<endl;
	start = clock();
	cout<<"Trial #:"<<flush;
	for (int i = 0; i < nTimes; i++)
	{
		cout<<i<<" "<<flush;

		for(int oAg=0;oAg<m_iNbOthers;oAg++){
			srand((unsigned)time(NULL));
			int index = advDID.PickIndexofModel(oAg);
			vector<float> dist = pForestArr[oAg]->GetPhyDist(index);
			bDIDArr[oAg]->ChangePhysicalBelief(dist);
		}
		float r = advDID.Evaluate(bDIDArr);
		if (_IDID_EVAL_OUTPUT_)
			cout<<"--------========"<<r<<"\n";
		res.push_back(r);
		if (_IDID_EVAL_OUTPUT_)
			cout<<endl;
	}
	cout<<"..done!"<<endl;
	final = clock();
	cout<<"\tThe time consumed for evaluation is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s "<<" for ["<<nTimes<<"] times. ["<<(((float)(final-start))/CLOCKS_PER_SEC)/nTimes<<"] for each\n";
	cout<<"\tThe mean utility is -> "<<vec_Sum(res)/res.size()<<". For each step is "<<vec_Sum(res)/(res.size()*atoi(nSteps.c_str()))<<". \n";

	vector<float> rBase,rTimes;
	sort(res.begin(),res.end());
	rBase.resize(res.size());
	unique_copy(res.begin(),res.end(),rBase.begin());
	float res2 = 0,tp2=0;
	for (size_t i = 0; i < rBase.size();++i)
	{
		int high = vec_FindTimes(res,rBase.at(i));
		if (rBase.at(i)>0)
		{
			cout<<"["<<rBase.at(i)<<"]-["<<high<<"],";
			rTimes.push_back(high);
		}
	}
	for (size_t i = 0; i < rTimes.size();++i)
	{
		if (rTimes.at(i) > nTimes*0.1)
		{
			res2 = res2 + rTimes.at(i)*rBase.at(i);
			tp2 = tp2 + rTimes.at(i);
		}
	}
	cout<<endl<<"The utility need to occur more than 10% times in the array, otherwise it is a noise. The smoothed value is "<<(res2/tp2)<<endl<<endl;


//	const int sdim = 7; // dimension of TDS with compile-time dimension
//		typedef CGAL::Triangulation_data_structure<CGAL::Dimension_tag<sdim> >
//			TDS;
//		TDS  tds(sdim);
//
//		TDS::Vertex_handle one_vertex = tds.insert_increase_dimension();
//		for( int i = 1; i < sdim+2; ++i )
//			tds.insert_increase_dimension(one_vertex);
//		// we get a triangulation of space of dim sdim homeomorphic to
//		// the boundary of simplex of dimension sdim+1 with sdim+2 vertices
//		CGAL_assertion( sdim   == tds.current_dimension() );
//		CGAL_assertion( 2+sdim == tds.number_of_vertices() );
//		CGAL_assertion( 2+sdim == tds.number_of_full_cells() );
//
//		barycentric_subdivide(tds, tds.full_cells_begin());
//
//		// The number of full cells should be twice the factorial of
//		// |tds.current_dimension()+1|. Eg, 1440 for dimension 5.
//		std::cout << "Barycentric Subdivision of d="<<sdim<<" simplex has "
//			<< tds.number_of_full_cells()/2 << " partitions";
//	//	    CGAL_assertion( tds.is_valid() );
//	//	    std::cout << " and is valid!"<<std::endl;
//		cout<<tds.number_of_vertices()<<endl;
//		TDS::Vertex_iterator fc=tds.vertices_begin();
//		cout<<"enumerating"<<endl;
//		//for(int kk=0; kk<sdim; kk++)
////				cout<<fc<<endl;
//
//		cout<<"enumerating--"<<endl;
////		cout<<tds.vertices()[0]<<endl;
//
//	//	    TDS::Vertex_container vc = tds..vertices_;
////		TDS::Vertex_container vc = tds.vertices_;
////		cout<<vc.first_item->Triangulation_ds_vertex()<<endl;
//		TDS::Vertex_iterator vto = tds.vertices_begin();
//		int cnt = 1;
//		while(vto != tds.vertices_end()){
////			for(int i=0;i<2;i++){
////				cout<<tds.vertices_.all_items[i].second<<" "<<endl;
////			}
//
//			cout<<cnt<<endl;
//			vto++;
//			cnt++;
//		}
//
//
//		// barycenter of 3D weighted points
//		vector<pair<Point_d, FT> > points_d;
//
//		for(int kk = 0 ; kk<sdim; kk++){
//			vector<float> dims;
//			for(int kkk=0;kkk<sdim;kkk++){
//				dims.push_back( kk==kkk ? 1.0 :0.0);
//
//			}
//			//printVector(dims);
//			points_d.push_back(make_pair(Point_d(sdim,dims.begin(),dims.end()),  1.0));
//		}
//
//		//for(int kk=0;kk<sdim;kk++)
//		//	cout<<points_d[kk].first<<endl;
//		for(vector<pair<Point_d, FT> >::iterator it =points_d.begin();it != points_d.end();++it)
//			cout<<it->first<<endl;
//		//cout<<points_d.empty()<<endl;
//		//Point_d cd = CGAL::barycenter(points_d.begin(),points_d.end());
//		vector<FT> dims;
//		for(int kkk=0;kkk<sdim;kkk++){
//							dims.push_back( 0.0);
//
//						}
//		Point_d v = Point_d(sdim,dims.begin(),dims.end());
//		FT norm = 0;
//		for(vector<pair<Point_d, FT> >::iterator it =points_d.begin();it != points_d.end();++it){
//			v = v + it->second * (it->first - CGAL::ORIGIN);
//			norm += it->second;
//		  }
//		vector<FT> dims2;
//		for(int kkk=0;kkk<sdim;kkk++){
//										dims2.push_back( v[kkk]/norm);
//
//									}
//		Point_d cd = Point_d(sdim,dims2.begin(),dims2.end()); //CGAL::ORIGIN.Origin + v / norm;
//		cout << cd << endl;
//
//		// barycenter of 2D weighted points
//		vector<pair<Point_2, FT> > points_2;
//		points_2.push_back(make_pair(Point_2(1.0, 0.0),  1.0));
//		points_2.push_back(make_pair(Point_2(2.0, 2.0),  1.0));
//		points_2.push_back(make_pair(Point_2(3.0, 5.0), 1.0));
//
//
//		//Point_d c2 = CGAL::barycenter(points_d.begin(), points_d.end());
//
//		// barycenter of 3D weighted points
//		vector<pair<Point_3, FT> > points_3;
//		points_3.push_back(make_pair(Point_3(1.0, 0.0, 0.0),  1.0));
//		points_3.push_back(make_pair(Point_3(0.0, 1.0, 0.0),  1.0));
//		points_3.push_back(make_pair(Point_3(0., 0., 01), 1.0));
//
//		Point_3 c3 = CGAL::barycenter(points_3.begin(), points_3.end());
//		cout << c3 << endl;
//
//		return ;
//
//
//		exit(0);



	//	// Solve all models of each of the other agents and update a vector of pForests
	//	cout<<"Solving all level-0 models of other agents.."<<endl;
	//	cout<<line<<endl;
	//	vector<CForest *> pForestArr(m_iNbOthers);
	//	for(int i=0;i<m_iNbOthers;i++){
	//		pForestArr.push_back(NULL);
	//		bDIDArr[i]->SolveDID(fileParaArr[i],&pForestArr[i],method);
	//	}
	//	cout<<"Finished solving level-0 DIDs for other agents.."<<endl<<endl;
	//
	//	cout<<"Solving level-1 model of the subject agent.."<<endl;
	//	cout<<line<<endl;
	//
	//	start = clock();
	//	advDID.ExpandModelwithForest(pForestArr,"Debug/Expanded_DID_Model/test.net");
	//	final = clock();
	//	cout<<"\tThe time consumed for expanding the I-DID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl;
	//	cout<<"Finished expanding the I-DID over time.. "<<endl<<endl;









//	string check = "0.25,0.75-0.375,0.625:0.625,0.375-0.75,0.25:0,1-0.0625,0.9375:0.0625,0.9375-0.125,0.875:0.125,0.875-0.1875,0.8125:0.1875,0.8125-0.25,0.75:0.375,0.625-0.4375,0.5625:0.5625,0.4375-0.625,0.375:0.75,0.25-0.8125,0.1875:0.8125,0.1875-0.875,0.125:0.875,0.125-0.9375,0.0625:0.9375,0.0625-1,0:0.4375,0.5625-0.46875,0.53125:0.53125,0.46875-0.5625,0.4375:0.46875,0.53125-0.484375,0.515625:0.484375,0.515625-0.5,0.5:0.5,0.5-0.515625,0.484375:0.515625,0.484375-0.53125,0.46875";




//	//SANITY TEST
//	simpleBlock B;
//	vector<float> beliefPoint1, beliefPoint2;
//	beliefPoint1.push_back(0.1875);
//	beliefPoint1.push_back(0.8125);
//
//	beliefPoint2.push_back(0.5);
//	beliefPoint2.push_back(0.5);
//
//	B.push_back(beliefPoint1);
//	B.push_back(beliefPoint2);
//
//	vector<beliefPoint> blockB;
//
//	for(int i=0;i<bDIDArr.size();i++){
//		cout<<"i="<<i<<endl;
//		for(int j=0;j<B.size();j++){
//			cout<<"j="<<j<<endl;
//			blockB.push_back(make_pair(bDIDArr[i],B[j]));
//		}
//	}
//
//	cout<<"number of belief points: "<<blockB.size()<<endl;
//	vector<pairOfBeliefPoints> beliefPairs = listPairs(blockB,blockB.size());
//
//	for(int i=0;i<beliefPairs.size();i++){
//		double emd = GetEMD(beliefPairs[i].first,beliefPairs[i].second,THRESHOLD);
//		cout<<"Final d(bi,bj) = "<<emd<<endl;
//	}
//	//END of SANITY TEST

//	vector<beliefPoint> beliefPoints;
//
//	//Get all belief points of all level-0 DIDs from corresponding parameter files
//	beliefPoints = GetBeliefPoints(fileParaArr,bDIDArr);
//	printVector(beliefPoints);
//
//	cout<<"number of belief points: "<<beliefPoints.size()<<endl;
//	listPairs(beliefPoints,6);
//
//
//	double emd = GetEMD(beliefPoints[0],beliefPoints[1],THRESHOLD);
//
//	cout<<"Final d(bi,bj) = "<<emd<<endl;


//	vector<vector<float> > output = printBin(2);
//	printVectorOfVector(output);
//	cout<<line;
//	vector<vector<float> > boundaryBeliefs = filterValidBeliefs(output);
//	printVectorOfVector(boundaryBeliefs);



//	//-----------------------------------------------
//	// Read images
//	const char* im1_name= "Debug/test/cameraman.txt";
//	const char* im2_name= "Debug/test/rice.txt";
//	unsigned int im1_R, im1_C, im2_R, im2_C;
//	std::vector<int> im1,im2;
//	readImage(im1_name, im1_R, im1_C, im1);
//	readImage(im2_name, im2_R, im2_C, im2);
//	if ( (im1_R!=im2_R) || (im1_C!=im2_C) ) {
//		std::cerr << "Images should be of the same size" << std::endl;
//	}
//	//-----------------------------------------------
//
//
//	//-----------------------------------------------
//	// The ground distance - thresholded Euclidean distance.
//	// Since everything is ints, we multiply by COST_MULT_FACTOR.
//	const int COST_MULT_FACTOR= 1000;
//	const int THRESHOLD= 3*COST_MULT_FACTOR;//1.412*COST_MULT_FACTOR;
//	// std::vector< std::vector<int> > cost_mat; // here it's defined as global for Rubner's interfaces.
//												   // If you do not use Rubner's interface it's a better idea
//												   // not to use globals.
//	std::vector<int> cost_mat_row(im1_R*im1_C);
//	for (unsigned int i=0; i<im1_R*im1_C; ++i) cost_mat.push_back(cost_mat_row);
//	int max_cost_mat= -1;
//	int j= -1;
//	for (unsigned int c1=0; c1<im1_C; ++c1) {
//		for (unsigned int r1=0; r1<im1_R; ++r1) {
//			++j;
//			int i= -1;
//			for (unsigned int c2=0; c2<im1_C; ++c2) {
//				for (unsigned int r2=0; r2<im1_R; ++r2) {
//					++i;
//					cost_mat[i][j]= std::min(THRESHOLD,static_cast<int>(COST_MULT_FACTOR*sqrt((r1-r2)*(r1-r2)+(c1-c2)*(c1-c2))));
//					if (cost_mat[i][j]>max_cost_mat) max_cost_mat= cost_mat[i][j];
//				}
//			}
//		}
//	}
//	//-----------------------------------------------
//
//	tictoc timer;
//	timer.tic();
//	int emd_hat_gd_metric_val= emd_hat_gd_metric<int>()(im1,im2, cost_mat,THRESHOLD);
//	timer.toc();
//	std::cout << "emd_hat_gd_metric time in seconds: " << timer.totalTimeSec() << std::endl;

//	        timer.clear();
//	        timer.tic();
//	        int emd_hat_val= emd_hat<int>()(im1,im2, cost_mat,THRESHOLD);
//	        timer.toc();
//	        std::cout << "emd_hat time in seconds: " << timer.totalTimeSec() << std::endl;








//	 for(int i=0;i<vecPairs.size();i++){
//		for(int j=0;j<vecPairs[i].second.size();j++){
//			cout<<vecPairs[i].second[j]<<" - ";
//		}
//		cout<<vecPairs[i].first->m_iNbState<<endl;
//	}
//
//    int n = sizeof(vecPairs)/sizeof(vecPairs[0]);
//    cout << "Count of unique pairs is ";
//    vector<pairOfPairDIDBeliefs> vecOfVecPairs = listPairs(vecPairs, vecPairs.size());
//    cout <<" == "<<vecOfVecPairs.size();



//	vector<CForest *> pForestArr(m_iNbOthers);
//	for(int i=0;i<m_iNbOthers;i++){
//		pForestArr.push_back(NULL);
//		bDIDArr[i]->SolveDID(fileParaArr[i],&pForestArr[i],method);
//	}
//	cout<<"Finished solving level-0 DIDs for other agents.."<<endl<<endl;


//	// Solve all models of each of the other agents and update a vector of pForests
//	cout<<"Solving all level-0 models of other agents.."<<endl;
//	cout<<line<<endl;
//	vector<CForest *> pForestArr(m_iNbOthers);
//	for(int i=0;i<m_iNbOthers;i++){
//		pForestArr.push_back(NULL);
//		bDIDArr[i]->SolveDID(fileParaArr[i],&pForestArr[i],method);
//	}
//	cout<<"Finished solving level-0 DIDs for other agents.."<<endl<<endl;
//
//	cout<<"Solving level-1 model of the subject agent.."<<endl;
//	cout<<line<<endl;
//
//	start = clock();
//	advDID.ExpandModelwithForest(pForestArr,"Debug/Expanded_DID_Model/test.net");
//	final = clock();
//	cout<<"\tThe time consumed for expanding the I-DID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl;
//	cout<<"Finished expanding the I-DID over time.. "<<endl<<endl;
}


void EvaluateAllinOne(int nTimes, string nSteps, string nPara,Merge_Method method, string nAgents)
{
	string line = "-----------------------------------------------";
	string smallline = "------------";

	cout<<line<<endl;
	string fileIDID="";

	/**
	 * m_iNbOthers : number of other agents
	 */
	int m_iNbOthers = atoi(nAgents.c_str())-1;

	vector<string> fileDIDArr(m_iNbOthers,"");
	vector<string> fileParaArr(m_iNbOthers,"");
	vector<float> res;
	clock_t start,final;

	/**
	 * fileDIDArr : take in a vector of DIDs of others (at level 0)
	 * eg: T03.net, T13.net for 3 agents (0,1 are indices of the other agents) and 3 steps
	 **/
	for(int i=0;i<m_iNbOthers;i++){
		stringstream ss;
		ss << "Debug/DID_Model/T" << i << nSteps << ".net";
		fileDIDArr[i] = ss.str();
		cout<<"DID (Other Agent "<<i<<"): "<<fileDIDArr[i]<<endl;
	}

	stringstream ss;
	ss << "Debug/IDID_Model/TIDID" << nSteps << "_" << nAgents.c_str() << "N.net";
	fileIDID = ss.str();
	cout<<"IDID (Subject Agent): "<<fileIDID<<endl;

	/**
	 * nParaArr : vector of parameter file names of others'
	 *            level 0 beliefs (over physical states) separated by commas
	 * eg: 'para0,para1' for 2 other agents
	 **/
	vector<string> nParaArr = split(nPara,',');
	for(int i=0;i<m_iNbOthers;i++){
		fileParaArr[i] = "Debug/para/" + nParaArr[i] + ".txt";
		cout<<"Parameter file (Other Agent "<<i<<"): "<<fileParaArr[i]<<endl;
	}
	cout<<line<<endl<<endl;

	cout<<"Instantiating decision networks for agents.."<<endl;
	cout<<line<<endl;

	/**
	 * bDIDArr : vector of CBasDID objects; one for each other agent
	 */
	vector<CBasDID*> bDIDArr(m_iNbOthers);
	for(int i=0;i<m_iNbOthers;i++){
		cout <<"\tInstantiating "<< fileDIDArr[i] <<".."<< endl;
		bDIDArr[i]=new CBasDID(fileDIDArr[i]);
	}
	cout <<"\tInstantiating "<< fileIDID <<".."<< endl;
	CAdvDID advDID(fileIDID, m_iNbOthers);
	cout<<"Finished instantiating decision networks for all agents.."<<endl<<endl;

	if(_IDID_DEBUG_){
		cout<<"Performing a sanity check.."<<endl;
		cout<<line<<endl;
		cout<<"Number of Other Agents: "<<advDID.m_iNbOthers<<endl;
		for(int i=0;i<m_iNbOthers;i++){
			cout<<"-- Other Agent "<<i<<endl
				<<"Number of Actions: "<<bDIDArr[i]->m_iNbAction<<endl
				<<"Number of Observations: "<<bDIDArr[i]->m_iNbObs<<endl
				<<"Number of States: "<<bDIDArr[i]->m_iNbState<<endl
				<<"Number of Horizons: "<<(*bDIDArr[i]).m_iNbSteps<<endl;
		}
		cout<<"Finished performing sanity check.."<<endl<<endl;
	}

	// Solve all models of each of the other agents and update a vector of pForests
	cout<<"Solving all level-0 models of other agents.."<<endl;
	cout<<line<<endl;
	vector<CForest *> pForestArr(m_iNbOthers);
	for(int i=0;i<m_iNbOthers;i++){
		pForestArr.push_back(NULL);
		bDIDArr[i]->SolveDID(fileParaArr[i],&pForestArr[i],method);
	}
	cout<<"Finished solving level-0 DIDs for other agents.."<<endl<<endl;

	cout<<"Solving level-1 model of the subject agent.."<<endl;
	cout<<line<<endl;

	start = clock();
	advDID.ExpandModelwithForest(pForestArr,"Debug/Expanded_DID_Model/test.net");
	final = clock();
	cout<<"\tThe time consumed for expanding the I-DID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s."<<endl;
	cout<<"Finished expanding the I-DID over time.. "<<endl<<endl;

//	if (method == AE || method == E_AE)
//	{
//		start = clock();
//		CGraphForest *pGForest = (CGraphForest *)pForest;
//		advDID.AECompress(*pGForest,"Debug/Expanded_DID_Model/test2.net");
//		final = clock();
//		cout<<"\tThe time consumed for AE compression is "<<final-start<<".\n";
//	}

	cout<<"Evaluating Subject Agent's I-DID"<<endl;
	cout<<line<<endl;
	start = clock();
	for (int i = 0; i < nTimes; i++)
	{
		if (_IDID_OUTPUT_){
			cout<<"Trial "<<i<<endl;
			cout<<smallline<<endl;
		}
		for(int oAg=0;oAg<m_iNbOthers;oAg++){
			srand((unsigned)time(NULL));
			int index = advDID.PickIndexofModel(oAg);
			vector<float> dist = pForestArr[oAg]->GetPhyDist(index);
			bDIDArr[oAg]->ChangePhysicalBelief(dist);
		}
		float r = advDID.Evaluate(bDIDArr);
		if (_IDID_OUTPUT_)
			cout<<"--------========"<<r<<"\n";
		res.push_back(r);
		if (_IDID_OUTPUT_)
			cout<<endl;
	}
	final = clock();
	cout<<"\tThe time consumed for evaluation is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s "<<" for ["<<nTimes<<"] times. ["<<(((float)(final-start))/CLOCKS_PER_SEC)/nTimes<<"] for each\n";
	cout<<"\tThe mean utility is -> "<<vec_Sum(res)/res.size()<<". For each step is "<<vec_Sum(res)/(res.size()*atoi(nSteps.c_str()))<<". \n";

	vector<float> rBase,rTimes;
	sort(res.begin(),res.end());
	rBase.resize(res.size());
	unique_copy(res.begin(),res.end(),rBase.begin());
	float res2 = 0,tp2=0;
	for (size_t i = 0; i < rBase.size();++i)
	{
		int high = vec_FindTimes(res,rBase.at(i));
		if (rBase.at(i)>0)
		{
			cout<<"["<<rBase.at(i)<<"]-["<<high<<"],";
			rTimes.push_back(high);
		}
	}
	for (size_t i = 0; i < rTimes.size();++i)
	{
		if (rTimes.at(i) > nTimes*0.1)
		{
			res2 = res2 + rTimes.at(i)*rBase.at(i);
			tp2 = tp2 + rTimes.at(i);
		}
	}
	cout<<"\nThe utility need to occur more than 10% times in the array, otherwise it is a noise. The smoothed value is "<<(res2/tp2);
	cout<<"\n\n";

}

//void EvaluateAllinOne(int times, string nSteps, string nPara,Merge_Method method)
//{
//	string fileDID,fileIDID,filePara;
//	vector<float> res;
//	clock_t start,final;
//
////	fileDID = "Debug/DID_Model/blah.net";
//	fileDID = "Debug/DID_Model/T" + nSteps + ".net";
//
//	cout<<"DID :"<<fileDID<<endl;
//	fileIDID = "Debug/IDID_Model/TIDID" + nSteps + ".net";
//	cout<<"IDID :"<<fileIDID<<endl;
//	filePara = "Debug/para/" + nPara + ".txt";
//	cout<<"Para :"<<filePara<<endl;
//
//	CBasDID bDID(fileDID);
//
//	cout<<"######################"<<endl;
//	cout<<"Number of Actions: "<<bDID.m_iNbAction<<endl
//			<<"Number of Observations: "<<bDID.m_iNbObs<<endl
//			<<"Number of States: "<<bDID.m_iNbState<<endl
//			<<"Number of Horizons: "<<bDID.m_iNbSteps<<endl;
//	cout<<"######################"<<endl;
//
//	CAdvDID advDID(fileIDID);
//	CForest *pForest = NULL;
//
//	bDID.SolveDID(filePara,&pForest,method);
//	start = clock();
//	advDID.ExpandModelwithForest(*pForest,"Debug/Expanded_DID_Model/test.net");
//	final = clock();
//	cout<<"\tThe time consumed for expanding the IDID is "<<final-start<<".\n";
//
//	if (method == AE || method == E_AE)
//	{
//		start = clock();
//		CGraphForest *pGForest = (CGraphForest *)pForest;
//		advDID.AECompress(*pGForest,"Debug/Expanded_DID_Model/test2.net");
//		final = clock();
//		cout<<"\tThe time consumed for AE compression is "<<final-start<<".\n";
//	}
//
//	start = clock();
//	for (int i = 0; i < times; i++)
//	{
//		srand((unsigned)time(NULL));
//		int index = advDID.PickIndexofModel();
//		vector<float> dist = pForest->GetPhyDist(index);
//		bDID.ChangePhysicalBelief(dist);
//		float r = advDID.Evaluate(bDID);
//		if (_IDID_OUTPUT_)
//			cout<<"--------========"<<r<<"\n";
//		res.push_back(r);
//	}
//	final = clock();
//	cout<<"\tThe time consumed for evaluation is "<<final-start<<" for ["<<times<<"] times. ["<<(final-start)/times<<"] for each\n";
//	cout<<"\tThe mean utility is -> "<<vec_Sum(res)/res.size()<<". For each step is "<<vec_Sum(res)/(res.size()*atoi(nSteps.c_str()))<<". \n";
//
//	vector<float> rBase,rTimes;
//	sort(res.begin(),res.end());
//	rBase.resize(res.size());
//	unique_copy(res.begin(),res.end(),rBase.begin());
//	float res2 = 0,tp2=0;
//	for (size_t i = 0; i < rBase.size();++i)
//	{
//		int high = vec_FindTimes(res,rBase.at(i));
//		if (rBase.at(i)>0)
//		{
//			cout<<"["<<rBase.at(i)<<"]-["<<high<<"],";
//			rTimes.push_back(high);
//		}
//	}
//	for (size_t i = 0; i < rTimes.size();++i)
//	{
//		if (rTimes.at(i) > times*0.1)
//		{
//			res2 = res2 + rTimes.at(i)*rBase.at(i);
//			tp2 = tp2 + rTimes.at(i);
//		}
//	}
//	cout<<"\nThe utility need to occur more than 10% times in the array, otherwise it is a noise. The smoothed value is "<<(res2/tp2);
//	cout<<"\n\n";
//}


//void testDMU()
//{
//	CSequenceTree t1(3,2,3);
//	CSequenceTree t2(3,2,3);
//	CSequenceTree t3(3,2,3);
//	int a1[7] = {2,0,2,2,2,0,2};	//1
//	int a2[7] = {2,2,2,0,2,2,1};	//2
//	int a9[7] = {1,1,2,0,2,2,1};	//3
//
//	int a3[7] = {-1,0,0,1,1,2,2};
//	int a4[7] = {0,1,1,2,2,2,2};
//	vector<int> v1,v2,v3,v4,v5;
//
//	v1.resize(7);
//	v2.resize(7);
//	v3.resize(7);
//	v4.resize(7);
//	v5.resize(7);
//
//	for (int i = 0; i < 7; i++)
//	{
//		v1.at(i) = a1[i];
//		v2.at(i) = a2[i];
//		v3.at(i) = a3[i];
//		v4.at(i) = a4[i];
//		v5.at(i) = a9[i];
//	}
//
//	t1.SetActArr(v1);
//	t1.SetAncArr(v3);
//	t1.SetObsArr(v4);
//	t2.SetActArr(v2);
//	t2.SetAncArr(v3);
//	t2.SetObsArr(v4);
//	t3.SetActArr(v5);
//	t3.SetAncArr(v3);
//	t3.SetObsArr(v4);
//
//	//////////////////////////////////////////////////////////////////////////
//	CGraphForest gForest(t1.GetNumberofLay(),t1.GetNumberofBranch(),t1.GetNumberofAction());
//	gForest.AddTree(&t1,1,DMU);
//	gForest.AddTree(&t2,1,DMU);
//	gForest.AddTree(&t3,1,DMU);
//	gForest.AddTree(&t2,1,DMU);
//
//	gForest.Print();
//
//	//CNormalForest nForest;
//	//nForest.AddTree(&t1);
//	//nForest.AddTree(&t2);
//	//nForest.AddTree(&t3);
//	//nForest.AddTree(&t2);
//
//	CAdvDID advDID("/home/mkran/workspace/IDID-Original/Debug//OModel3.net");
//	advDID.ExpandModelwithForest(gForest,"/home/mkran/workspace/IDID-Original/Debug/test.net");
//
//	advDID.AECompress(gForest,"/home/mkran/workspace/IDID-Original/Debug/test2.net");
//
//	return;
//
//	//vector<int>& vv1 = t1.GetStepAct(3);
//	//vector<int>& vv2 = t2.GetStepAct(3);
//	//
//	//vector<pair<int,int>> graphItem;
//	//vector<int> arrNode;
//	//vector<int> arrLayerIdct;
//	//distingush(vv1,vv2,arrNode);
//	//arrLayerIdct.assign(arrNode.size(),3);
//
//	//pair<int,int> tmpPair;
//	//for (size_t i = 0; i < arrNode.size(); i++)
//	//{
//	//	tmpPair = make_pair(arrNode.at(i),i);
//	//}
//
//	////
//	//vv1 = t1GetStepAct(2);
//	//vv2 = t2.GetStepAct(2);
//}

//void testHugin()
//{
//	int tmp;
//	Domain* m_pDomain;
//	DefaultParseListener pl;
//	m_pDomain = new Domain("C:\\PModel3.net", &pl);
//	m_pDomain->triangulate (H_TM_FILL_IN_WEIGHT);
//	//m_pDomain->compile();
//
//	DiscreteNode* pNode;
//	//Table* pTab;
//	float* pf = NULL;
//	bool res;
//	float ft;
//	int j;
//	pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,"O2");
//	NumberList nbList = pNode->getTable()->getData();
//	//getNodeBelief(pNode,&pf,j);
//	//pNode->selectState(2);
//	//pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,"O2");
//	//pNode->selectState(0);
//
//	m_pDomain->propagate();
//
//	//pf = getNodeBelief(m_pDomain,"D2",&pf,tmp);
//	pf = getNodeUtilities(m_pDomain,"D2",&pf,tmp);
//	j = getBestChoice(m_pDomain,"D2");
//	pf = getNodeUtilities(m_pDomain,"D2",&pf,tmp);
//	pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,"O2");
//	res = pNode->isEvidencePropagated();
//	res = pNode->isEvidenceEntered();
//
//	//m_pDomain->propagate();
//
//	pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,"O2");
//	ft = pNode->getEnteredFinding(0);
//	ft = pNode->getEnteredFinding(1);
//	pNode->retractFindings();
//	ft = pNode->getEnteredFinding(0);
//	ft = pNode->getEnteredFinding(1);
//	//res = pNode->isEvidencePropagated();
//	//res = pNode->isEvidenceEntered();
//	pf = getNodeBelief(m_pDomain,"D2",&pf,tmp);
//	pf = getNodeUtilities(m_pDomain,"D2",&pf,tmp);
//	j = getBestChoice(m_pDomain,"D2");
//	m_pDomain->propagate();
//	pf = getNodeBelief(m_pDomain,"D2",&pf,tmp);
//	pf = getNodeUtilities(m_pDomain,"D2",&pf,tmp);
//	j = getBestChoice(m_pDomain,"D2");
//	pNode->selectState(1);
//	ft = pNode->getEnteredFinding(0);
//	ft = pNode->getEnteredFinding(1);
//	res = pNode->isEvidencePropagated();
//	res = pNode->isEvidenceEntered();
//	m_pDomain->propagate();
//	res = pNode->isEvidencePropagated();
//	res = pNode->isEvidenceEntered();
//	pf = getNodeBelief(m_pDomain,"D2",&pf,tmp);
//	pf = getNodeUtilities(m_pDomain,"D2",&pf,tmp);
//	j = getBestChoice(m_pDomain,"D2");
//
//}
//
//void DoEvaluation()
//{
//	/*start = clock();
//	cout<<"[Original]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,NO_Merge);
//	final = clock();
//	cout<<"\tTime consuming is(ms): "<<final-start<<endl;*/
//	/*clock_t start,final;
//
//	start = clock();
//	cout<<"[BE]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,BE);
//	final = clock();
//	cout<<"\tTime consuming is(ms): -> "<<final-start<<endl;
//
//	start = clock();
//	cout<<"[DMU]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,DMU);
//	final = clock();
//	cout<<"\tTime consuming is(ms): -> "<<final-start<<endl;
//
//	start = clock();
//	cout<<"[AE]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,AE);
//	final = clock();
//	cout<<"\tTime consuming is(ms): -> "<<final-start<<endl;
//
//	start = clock();
//	cout<<"[E_BE]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,E_BE);
//	final = clock();
//	cout<<"\tTime consuming is(ms): -> "<<final-start<<endl;
//
//	start = clock();
//	cout<<"[E_DMU]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,E_DMU);
//	final = clock();
//	cout<<"\tTime consuming is(ms): -> "<<final-start<<endl;
//
//	start = clock();
//	cout<<"[E_AE]";
//	EvaluateAllinOne(nTimes,nSteps,nPara,E_AE);
//	final = clock();
//	cout<<"\tTime consuming is(ms): -> "<<final-start<<endl; */
//}
