#include "stdafx.h"
#include "IDID.h"
#include "SequenceTree.h"



CSequenceTree::CSequenceTree(int step, int nbEvi, int nbAct)
{
	m_iNbEvi = nbEvi;
	m_iNbHorizon = step;
	m_iNbAct = nbAct;
	m_iTreeSize = static_cast<int>((pow((float)nbEvi,(float)step)-1)/(float)(nbEvi-1));

	m_arrAct.resize(m_iTreeSize);
	m_arrAnc.resize(m_iTreeSize);
	m_arrEvi.resize(m_iTreeSize);
	m_arrObs.resize(m_iTreeSize);
	
	InitilizeArrays();
}

CSequenceTree::~CSequenceTree(void)
{
	m_arrEvi.clear();	
	m_arrObs.clear();   
	m_arrAnc.clear();	
	m_arrAct.clear();	

	//m_arrDLabel.clear();
	//m_arrOLabel.clear();
}

void CSequenceTree::InitilizeArrays()
{
	int currEvid = 0;
	int currAnc = 0;
	int currObs = 0;
	int tp = 0;

	m_arrAnc.at(0) = -1;
	m_arrEvi.at(0) = -1;
	m_arrObs.at(0) = 0;

	for (int i = 1; i < m_iTreeSize; i++)
	{
		currEvid = currEvid%m_iNbEvi;
		m_arrEvi.at(i) = currEvid;
		currEvid++;	

		tp++;
		m_arrAnc.at(i) = currAnc;
		if (tp == m_iNbEvi)
		{
			tp = 0;
			currAnc++;
		}		
	}

	//in solving, the first O node[O3] will be omitted
	//we start from O2,so the first index is 1.
	tp = 1;
	int cur=1;
	for (int i = 1; i<m_iNbHorizon; i++)
	{
		int nbLeaves = static_cast<int>(pow(float(m_iNbEvi),float(i)));
		//cout<<nbLeaves<<"\n";
		for (int j = 0; j<nbLeaves; j++)
		{
			m_arrObs.at(cur) = tp;
			cur++;
		}
		tp++;
	}
}



vector<int> CSequenceTree::GetStepEvi( int step )
{
	//check
	return GetStepArray(m_arrEvi, step);
}

vector<int> CSequenceTree::GetStepObs( int step )
{
	return GetStepArray(m_arrObs, step);
}

vector<int> CSequenceTree::GetStepAnc( int step )
{
	return GetStepArray(m_arrAnc, step);
}

vector<int> CSequenceTree::GetStepAct( int step )
{
	return GetStepArray(m_arrAct, step);
}

vector<int> CSequenceTree::GetStepArray(vector<int> arr, int step)
{
	int num = static_cast<int>(pow((float)m_iNbEvi,float(step-1)));
	vector<int> result;
	result.resize(num);

	int start = static_cast<int>((pow((float)m_iNbEvi,(step-1))-1/(m_iNbEvi-1)));

	for (int i = 0; i < num; i++)
	{
		result.at(i) = arr.at(start+i);
	}

	return result;
}

void CSequenceTree::PrintTree(string fileName)
{
	/*ofstream of;
	of.open(fileName.c_str());
	float *res = new float[3];

	of<<m_arrDLabel.at(0)<<"--"<<m_arrAnc.at(0)<<"---"<<m_arrAct.at(0)<<"\n";
	for (int i = 1; i < m_iTreeSize; i++)
	{
		of<<m_arrDLabel.at(m_arrObs.at(i)+1)<<"--"<<m_arrAnc.at(i)<<"---"<<m_arrAct.at(i)<<"\n";
	}

	of.close();*/
}

int CSequenceTree::GetActChildren(int layer,int item, vector<pair<int,int> >& arrAct)
{
	//item < = pow(nbEvi,layer)
	if (item >= pow((float)m_iNbEvi,(float)layer-1))
	{
		cout<<"ERROR:[NormalTree] ---the item exceed the maximal number \n";
		return -1;
	}

	int ind_a;   //index of ancestor
	ind_a = static_cast<int>((pow((float)m_iNbEvi,(float)layer-1)-1)/(m_iNbEvi-1) + item);

	if (ind_a > ((pow((float)m_iNbEvi,(float)m_iNbHorizon-1)-1)/(m_iNbEvi-1))-1)  //this node in the last layer
	{
		return -1;
	}

	for (int i = 0; i < m_iTreeSize; i++)
	{
		if (m_arrAnc.at(i) == ind_a)
		{			
			pair<int,int> tmpPair = make_pair(m_arrAct.at(i),i);
			arrAct.push_back(tmpPair);
		}
	}

	//if (iter+m_iNbEvi-1 < m_arrAct.end())
	//{
		//arrAct.assign(iter,iter+m_iNbEvi);
	//}
	return 0;
}

int* CSequenceTree::GetActChildren(int index)
{
	int* pChild = NULL;
	int cursor = 0;
	if (index > ((pow((float)m_iNbEvi,(float)m_iNbHorizon-1)-1)/(m_iNbEvi-1))-1)  //this node in the last layer
	{		
		return pChild;
	}

	pChild = new int[m_iNbEvi];
	for (int i = 0; i < m_iTreeSize; i++)
	{
		if (m_arrAnc.at(i) == index)
		{			
			pChild[cursor] = i;
			cursor++;
			
			if (cursor == m_iNbEvi)
				break;      //at most m_iNbEvi children.			
		}
	}
	
	return pChild;
}
