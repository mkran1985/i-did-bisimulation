#pragma once
#include "IDID.h"
#include "Forest.h"
#include "SequenceTree.h"
#include "string.h"
using namespace std;


class GF_Node  
{
public:
	int action;			//current action
	int layer;			//which layer this node is.  
	int pFather;  //this link is a temp link, only available before merge.
	//the number of children are the same for every node.
	GF_Node* *ppChildren; //the array of pointers to children
	

	GF_Node()
	{
		action = -1;
		layer = 0;
		ppChildren = NULL;
	}
	GF_Node(int nbEvi)
	{
		action = -1;
		layer = 0;	
		ppChildren = new GF_Node*[nbEvi];
		memset(ppChildren,NULL,nbEvi*sizeof(GF_Node*));	
		pFather = NULL;
	}
	~GF_Node()
	{
		if (ppChildren)
		{
			delete[] ppChildren;
			ppChildren = NULL;
		}		
	}
};

//use action array... DMU
class CGraphForest:public CForest
{

private:
	vector<GF_Node*> m_arrNode;					//the nodes of graph,one vector for one layer.
	vector<int> m_arrIdcLayer;					//indicator of layers, it is redundant to the layer in GF_node
	//the difference between model is their physical believes.
	//after compression, we can use fewer models to stand for all models
	//each original model will map to a new one.
	//the array below stored this mapping relation
	//use the value here to get the distribution in DID
	//vector<int> m_arrModelMap;  

public:
	CGraphForest(void);
	CGraphForest(int horizon,int nbEvi, int nbAct);
	~CGraphForest(void);
	
	void AddTree(CSequenceTree* pTree, size_t index_tree,Merge_Method method);
	void Print();
	
	//functions from CForest.
	//which 
	int GetMap(int layer,int index,int trans);
	//the size of each layer.
	int GetFMap(int layer,int index);

	int GetLayerSize(int layer);
	vector<int> GetStepAct(int step);

private:
	//the current action should be the same, 
	//and the next step actions should also be the same with given various observations
	//if we can find any 'same' action in the forest...
	//the current action will be inserted
	//true already exist
	bool ExistSameAction(GF_Node* pNode, int& index);

	//given a action, find its index in the specific layer.
	//-1 means not exist.
	//int GetIndexofChild(pair<int,int> prAct,int layer, CNormalTree* pTree);

	//equal true, 
	bool GFNodeEqual(GF_Node* pNode1,GF_Node* pNode2);
	
	//get the pointer to a node, which is specified by layer and the index in this layer
	//layer is 1 base, index is 0 base
	GF_Node* FindNode(int layer,int index);

	//get the index of a node in its layer.
	//layer is 1 base, index is 0 base
	int FindIndex(GF_Node* pNode, int layer);

	int GetTreeIndex(int index);
};
