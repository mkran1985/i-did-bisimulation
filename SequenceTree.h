#pragma once
#include "stdafx.h"

//sequence tree uses array to sort every node of a tree by the order of width first traverse
//since the original policy tree is a full tree.
//Obviously, there are more than 1 number needed to be saved in each tree node,
//so, there are more than 1 array, 
//given an index, one tree node includes all elements in arrays at that position. 
//I am still thinking about the advantage of this data structure. Does is necessary?
class CSequenceTree
{
private:
	int m_iNbHorizon;		//how many steps do we need to consider.
	int m_iNbEvi;			//number of evidence, i.e. number of branches of policy tree,,
	int m_iTreeSize;        //e.x. 3^10-1   [ m_iNbEvi^m_iNbHorizon - 1 ]
	int m_iNbAct;           //how many option in one decision node.

	//the index of evidence, 
	vector<int> m_arrEvi;	//array of evidence;  
	//the evidence belongs to this observation, the index of observation label in m_arrOLabel.
	vector<int> m_arrObs;   //array of observation;	
	vector<int> m_arrAnc;	//array of ancestor;	
	vector<int> m_arrAct;	//array of action;

public:
	//CCompactTree(void);
	~CSequenceTree(void);
	CSequenceTree(int horizon, int nbEvi,int nbAct);


	//we use several arrays to store the links and the nodes
	//do initialization here.
	void InitilizeArrays();

	void PrintTree(string fileName);

	//property(s)
	int GetNumberofBranch() {return m_iNbEvi;}
	int GetNumberofLay() {return m_iNbHorizon;}
	int GetSizeofTree() {return m_iTreeSize;}
	int GetNumberofAction() { return m_iNbAct; }

	//get arrays
	vector<int>& GetEviArr() {return m_arrEvi; }
	vector<int>& GetObsArr() {return m_arrObs; }
	vector<int>& GetAncArr() {return m_arrAnc; }
	vector<int>& GetActArr() {return m_arrAct; }

	//set arrays
	void SetEviArr(vector<int> arr) { m_arrEvi = arr; }
	void SetObsArr(vector<int> arr) { m_arrObs = arr; }
	void SetAncArr(vector<int> arr) { m_arrAnc = arr; }
	void SetActArr(vector<int> arr) { m_arrAct = arr; }

	//get one layer of the tree. [start from 1]
	//since we only need one layer of a tree,
	//we cannot just return a reference.
	vector<int> GetStepEvi(int step);
	vector<int> GetStepObs(int step);
	vector<int> GetStepAnc(int step);
	vector<int> GetStepAct(int step);

	//specify which layer and which one in this layer
	//layer is base on 1
	//item is base on 0
	int GetActChildren(int layer,int item, vector<pair<int,int> >& arrAct);
	
	//given the node's index, return its children's index(s)
	//the length of that array should the be m_iNbObservation
	int* GetActChildren(int index);

private:
	vector<int> GetStepArray(vector<int> arr, int step);
	
};
