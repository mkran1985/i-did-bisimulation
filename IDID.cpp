#pragma once
#include "stdafx.h"
#include "IDID.h"
#include "AdvDID.h"
#include "Forest.h"
#include "BatOp.h"
//i want to use this as a switch of log output(to screen)



int mainf(int argc, char *argv[])
{
	//int round, int step, para file, method(Capital)
	if (argc < 5)
	{
		cout<<"There are five mandatory and two optional parameters to be specified---------------\n"
			<<" 1st: number of iterations;\n "
			<<" 2nd: number of steps in the DID and IDID; \n"
			<<" 3rd: the parameter files separated by commas; \n"
			<<" 4th: the method of compression. [NO,BE,DMU,E_BE,E_DMU,BIS], CASE SENSITIVE!.\n"
			<<" 5th: number of agents; \n"
			<<" [If Method = BIS] 6th: discount factor; \n"
			<<" [If Method = BIS] 7th: epsilon_c;"<<endl;
		return 0;
	}

//	clock_t start,final;
	int nTimes = 0;
	string nSteps,nPara,method,sMethod,nAgents,lambda,epsilon_c;
	nTimes = atoi(argv[1]);
	nSteps = string(argv[2]);
	//comma delimiter
	nPara = string(argv[3]);
	sMethod = string(argv[4]);
	nAgents = string(argv[5]);

	if (argc > 6){
		lambda = string(argv[6]);
		epsilon_c = string(argv[7]);
	}

	if (argc > 6 && (lambda == "" || epsilon_c == "")){
		cout<<"If the merge method is BIS, two additional parameters have to be specified---------------\n"
			<<" [If Method = BIS] 6th: discount factor; \n"
			<<" [If Method = BIS] 7th: epsilon_c; \n";
		return 0;
	}

	cout<<"Evaluation Summary: \n\t["<<nAgents<<"-Agents] Running for ["<<nTimes<<
		"] times; ["<<nSteps<<"] Steps; With parameter ["<<nPara<<"]; and with ["<<
		sMethod<<"] method";

	if (argc > 6){
		cout<<"; Discount factor = "<<lambda<<" and Epsilon = "<<epsilon_c<<endl<<endl;
	}else{
		cout<<endl<<endl;
	}

	//then I want to encode several parameters into one integer, 
	//by making full use of every bit.
//	int tt = 0x00000011;
//	int tt1 = 0x10000001;
//	int m = 1<<1;
//	int r  = tt|m;
//
//	int iStep = atol(nSteps.c_str());
	
	if (!strcmp(argv[4],"NO"))
	{
		cout<<"NO\n";
//		EvaluateAllinOne(nTimes,nSteps,nPara,NO_Merge);
		EvaluateAllinOne(nTimes,nSteps,nPara,NO_Merge,nAgents);
	}
	else if (!strcmp(argv[4],"BE"))
	{
		cout<<"BE\n";
//		EvaluateAllinOne(nTimes,nSteps,nPara,BE);
		EvaluateAllinOne(nTimes,nSteps,nPara,BE,nAgents);
	}
	else if (!strcmp(argv[4],"DMU"))
	{
		cout<<"Method: DMU\n";

//		EvaluateAllinOne(nTimes,nSteps,nPara,DMU);
		EvaluateAllinOne(nTimes,nSteps,nPara,DMU,nAgents);
	}
//	else if (!strcmp(argv[4],"AE"))
//	{
//		cout<<"AE\n";
////	EvaluateAllinOne(nTimes,nSteps,nPara,AE);
//		EvaluateAllinOne(nTimes,nSteps,nPara,AE,nAgents);
//	}
	else if (!strcmp(argv[4],"E_BE"))
	{
		cout<<"E_BE\n";
//		EvaluateAllinOne(nTimes,nSteps,nPara,E_BE);
		EvaluateAllinOne(nTimes,nSteps,nPara,E_BE,nAgents);
	}
	else if (!strcmp(argv[4],"E_DMU"))
	{
		cout<<"E_DMU\n";
//		EvaluateAllinOne(nTimes,nSteps,nPara,E_DMU);
		EvaluateAllinOne(nTimes,nSteps,nPara,E_DMU,nAgents);
	}
//	else if (!strcmp(argv[4],"E_AE"))
//	{
//		cout<<"E_AE\n";
////		EvaluateAllinOne(nTimes,nSteps,nPara,E_AE);
//		EvaluateAllinOne(nTimes,nSteps,nPara,E_AE,nAgents);
//	}
	else if (!strcmp(argv[4],"BIS"))
	{
		cout<<"Bisimulation\n";
		EvaluateAllinOne(nTimes, nSteps, nPara, BIS, nAgents, lambda, epsilon_c);
	}
	else
	{
		cout<<"unknown method, sorry!\n";
	}

	return 0;
}


