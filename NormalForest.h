#pragma once
#include "IDID.h"
#include "Forest.h"
#include "SequenceTree.h"
//given a parameter file which containing different distributions of physical state.
//a group of policy trees will be derived through the solving.
//one distribution for one tree.

class CNormalForest:public CForest
{
	//we assume that the structure of trees in the forest are identical.
private:
	vector<CSequenceTree*> m_pTrees; 
	//vector<Number> m_arrWeight;     //the weight of each tree. used to set the distribution of Mod(n).

public:
	CNormalForest(void);
	~CNormalForest(void);
	
	void AddTree( CSequenceTree* pTree, size_t index,Merge_Method method);
	
	//how many trees, i.e., how many states in Mod1.
	int GetSize() { return static_cast<int>(m_pTrees.size());}
	//int GetNumberofBranch() {return m_pTrees.at(0)->GetNumberofBranch();}
	//int GetNumberofLay() {return m_pTrees.at(0)->GetNumberofLay();}
	int GetNumberofAction() { return m_pTrees.at(0)->GetNumberofAction(); }

	vector<int> GetStepEvi(int step);
	vector<int> GetStepObs(int step);
	vector<int> GetStepAnc(int step);
	vector<int> GetStepAct(int step);
	
	
	//virtual functions
	int GetLayerSize(int layer);
	//comments in *.cpp
	int GetMap(int layer,int index,int trans);
	int GetFMap(int layer,int index);
private:
	//compress the forest depending on the Behavior Equivalence
	//the main idea of BE is that:
	//if 2 tree a BE, i.e. they are totally identical, 
	//that one of them is redundant and it will be discarded
	//P.S. the sum of weights[distribution] of trees need to be the same.
	//return: the index of the same tree which has already in the forest
	//	-1: not exist in forest, we need to insert it into.
	int BECheck(CSequenceTree* pTree);
	
};
