#include "stdafx.h"
#include "NormalForest.h"

CNormalForest::CNormalForest(void)
{
	m_eType = NORMAL_F;
}

CNormalForest::~CNormalForest(void)
{
	for (size_t i = 0; i < m_pTrees.size(); i++)
	{
		delete m_pTrees.at(i);
	}
	m_pTrees.clear();
}

vector<int> CNormalForest::GetStepAnc( int step )
{
	size_t numTree = m_pTrees.size();
	CSequenceTree* currTree;
	vector<int> res,temp;

	for (size_t i = 0; i<numTree;i++)
	{
		currTree = m_pTrees.at(i);
		temp = currTree->GetStepAnc(step);
		res.insert(res.end(),temp.begin(),temp.end());
	}
	return res;
}
vector<int> CNormalForest::GetStepObs( int step )
{
	size_t numTree = m_pTrees.size();
	CSequenceTree* currTree;
	vector<int> res,temp;

	for (size_t i = 0; i<numTree;i++)
	{
		currTree = m_pTrees.at(i);
		temp = currTree->GetStepObs(step);
		res.insert(res.end(),temp.begin(),temp.end());
	}
	return res;
}

vector<int> CNormalForest::GetStepAct( int step )
{
	size_t numTree = m_pTrees.size();
	CSequenceTree* currTree;
	vector<int> res,temp;

	for (size_t i = 0; i<numTree;i++)
	{
		currTree = m_pTrees.at(i);
		temp = currTree->GetStepAct(step);
		res.insert(res.end(),temp.begin(),temp.end());
	}
	return res;
}

vector<int> CNormalForest::GetStepEvi( int step )
{
	size_t numTree = m_pTrees.size();
	CSequenceTree* currTree;
	vector<int> res,temp;

	for (size_t i = 0; i<numTree;i++)
	{
		currTree = m_pTrees.at(i);
		temp = currTree->GetStepEvi(step);
		res.insert(res.end(),temp.begin(),temp.end());
	}
	return res;
}

void CNormalForest::AddTree( CSequenceTree* pTree, size_t index_tree,Merge_Method method)
{
	int ind;
	switch (method)
	{
	case NO_Merge:
		m_pTrees.push_back(pTree);
		break;
	case BE:
		ind = BECheck(pTree);
		if (ind >= 0)  //exist 
			m_arrWeight.at(index_tree) = ind;
		else  // insert		
		{	
			m_arrWeight.at(index_tree) = GetSize();    //to make the weight array is 0-base
			m_pTrees.push_back(pTree);
		}
		break;
	case E_BE:
		ind = BECheck(pTree);
		if (ind >= 0)  //exist 
		{	
			replace(m_arrWeight.begin(),m_arrWeight.end(),m_arrIndofCombd.at(index_tree),m_arrIndofCombd.at(ind));
			m_arrIndofCombd.at(index_tree) = m_arrIndofCombd.at(ind);   //7->0
			//m_arrWeight.at(index_tree) = ind;
		}
		else  // insert		
		{	
			//m_arrWeight.at(index_tree) = GetSize();    //to make the weight array is 0-base
			m_pTrees.push_back(pTree);	
		}
		break;
	default:
		cout<<"Warning:[NormalForest] ---- unknown merge method \n";
		break;
	}
}

int CNormalForest::BECheck( CSequenceTree* pTree )
{
	vector<int>& actObj = pTree->GetActArr();
	int ind = -1;

	for (int i = 0; i < GetSize(); i++)
	{
		vector<int>& actRef = m_pTrees.at(i)->GetActArr();
		if (equal(actRef.begin(),actRef.end(),actObj.begin()))
			ind = i;		
	}
	return ind;
}

int CNormalForest::GetLayerSize( int layer )
{
	return static_cast<int>(pow((float)m_iNbBranch,(float)layer-1)*GetSize());
}

//the implementation of GetMap in normal forest is not very good.
//different idea but the same result.
//since we take advantage of the full tree's forest
int CNormalForest::GetMap(int layer,int index,int trans)
{
	//return 1;
	return index*GetNbofBranch()+trans;
}
//since we take advantage of the full tree's forest
int CNormalForest::GetFMap(int layer,int index)
{
	//return 1;
	return 32;
}
