#pragma once
#include "IDID.h"
#include "SequenceTree.h"

//this class the the base class of GraphForest and NormalForest which is available currently.
//the common part of different kind of forest will be handled in the base class

class CForest
{

protected:
	int m_iNbBranch;			//observations, it is the up limitation
	int m_iNbHorizon;			//steps, time slice
	int m_iNbAction;			//possible actions, 
	Forest_Type m_eType;
	//the weight here indicate the tree[i] can be represented by which tree
	//rather than contain every tree's weight.
	//for example, if every tree is 'different'', the arrWeight will be [0,1,2,3,...]
	//if the first 2 tree are the 'same' the arrWeight could be [0,0,2,3,..]
	//here, the 'same' and 'different' will have different criteria in different compression algorithm.
	//the reason why we do it in this way, because after the compression and expansion, we still need to 
	//find the original model in the evaluation procedure.
	//Please let me know, if you have any good idea about the name of this variable. Tak!
	vector<int> m_arrWeight;    
	vector<vector<float> > m_arrPhyDist; //the distribution of the physical state for different l-0 models.
	vector<vector<float> > m_arrCombinedDist;
	//the index of combined distribution in the original array.
	vector<int> m_arrIndofCombd;  

public:
	CForest(void);
	~CForest(void);
	
	int GetNbofLayer() {return m_iNbHorizon;}
	void SetNbofLayer(int val) { m_iNbHorizon = val;}
	
	int GetNbofBranch() { return m_iNbBranch;}
	void SetNbofBranch(int val) {m_iNbBranch = val;}

	Forest_Type GetType() { return m_eType;}

	vector<int>& GetWeight() {return m_arrWeight;}

	int GetNbofAction() { return m_iNbAction; }
	void SetNbofAction(int val) { m_iNbAction = val; }
	
	//get the real weight of each tree in the forest, base on "m_arrWeight"
	//e.g. (0,0,2,3) => (2,1,1), 
	void CalWeight(vector<int>& weight);

	vector<vector<float> >* GetPhyDists() { return &m_arrPhyDist;}
	vector<float>& GetPhyDist(size_t index);	
	vector<vector<float> >* GetCombinedDists() { return &m_arrCombinedDist;}

	void DisCombination(Combination_Method method, float pare = 0);

	//virtual functions.
	virtual int GetLayerSize(int layer) = 0;
	virtual int GetMap(int layer,int index,int trans)=0;
	virtual vector<int> GetStepAct(int step) = 0;
	virtual void AddTree( CSequenceTree* pTree, size_t index_tree,Merge_Method method) = 0;
	virtual int GetFMap(int layer,int index)=0;

	
};
