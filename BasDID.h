#pragma once
#include "IDID.h"
#include "SequenceTree.h"
#include "NormalForest.h"
#include "GraphForest.h"

typedef vector<float> simpleBeliefPoint;
typedef vector<simpleBeliefPoint> simpleBlock;
typedef vector<simpleBlock> _simplePartition;
//this class is a so called Basic DID, means that it is the model for the level-0 agent. A poor zombie! -_-!
//In this model, there are 4 nodes S*,D*,O*,U* in each step slice.

class CBasDID
{
friend class CAdvDID;

public:
	Domain* m_pDomain;
	FILE* m_pFile;					//parameter file
	vector<string> m_arrSNode;		//S
	vector<string> m_arrDNode;		//D*
	vector<string> m_arrUNode;		//U*
	vector<string> m_arrONode;		//O* 
	ofstream m_of;					//log file;
	typedef pair<CBasDID*,vector<float> > pairDIDBeliefs;

public:
	int m_iNbSteps;			//number of steps
	int m_iNbState;			//number of physical states   |S|
	int m_iNbAction;	//number of possible actions  |D|	
	int m_iNbObs;			//number of possible observations |O|      
	
	//vectors<string> m_arrStrDist;		//the distributions string of physical state.
	//vector<vector<float>> m_arrDistri;  //distributions of the physical state.


public:
	//
	CBasDID(void);
	//the txtFile is used to specify the parameter and the CPTs of DID
	CBasDID(string netFile, string txtFile);
	//load DID from a [*.net] file,everything is already in the file
	CBasDID(string netFile);
	//destruct, cleaning.....vector(s) and domain*
	~CBasDID(void);

	//load parameters and CPT from a text file, compare to GetDIDPara();
	Domain* LoadCPT(bool zmbFlag);
	
	//solve a DID and build a policy tree. a new instance of tree will be created.
	CSequenceTree* SolveDID();

	//the paraFile has different believes about the physical state.
	//CNormalForest& SolveDID( string paraFile, CNormalForest& forest,Merge_Method method);
	CForest* SolveDID( string paraFile, CForest** ppForest, Merge_Method method);
	CForest* SolveDID( string paraFile, _simplePartition P, CForest** ppForest, Merge_Method method);
	CGraphForest& SolveDIDDMU( string paraFile, CGraphForest& forest);
	CSequenceTree* SolveDID(vector<float> sbp);
//	CForest* SolveDID(CForest** ppForest, vector<CSequenceTree*> pTrees, Merge_Method method);
	CForest* SolveDID(string paraFile, CForest** ppForest, vector<CSequenceTree*> pTrees, Merge_Method method);
	

	//get one trace, here trace means that one possible solution of this DID,and we don't need to solve it.
	vector<int>& GetTrace(vector<int>& trace);
	vector<int>& GetTrace(vector<int>& trace, vector<float>& dist);
	
	//change the distribution of node s[n]
	int ChangePhysicalBelief(vector<float>& fltArr);

	int getSerialRewardsIndex(int stateIndex, int actionIndex);
	vector<float> GetUpdatedBelief(vector<float> initialBelief, int action, int obs);
	double GetObservationProbability(int nextState, int action, int obs);
	double GetTransitionProbability(int nextState, int action, int obs);
	double GetBeliefTransitionProbability(vector<float> initialBelief, int action, vector<float> nextBelief);

protected:
	//void InitilizeLabels();
	void LoadFileHeader();
	void SetNodeState(bool ODFlag);

	//the slice for the first action,
	void SetLastSlice();
	//the slices from 1 to n-2 are identical, copy without modify
	void CopyPreviousSlices();
	//the CPTs of the second last slice will load from file,
	void SetSecondLastSlice2();
	//get parameters from DID(.net) file, compare to LoadCPT(bool)
	int GetBasDIDPara(); 
	

};
