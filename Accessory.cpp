#pragma once
#include "stdafx.h"
#include "Accessory.h"

int findMax(float *arr,int len)
{
	int ind = 0;
	for (int i = 1; i < len; i++)
	{
		if (arr[i]>arr[ind])
		{
			ind = i;
		}		
	}
	return ind;
}


//************************************
// Method:    InitNodeLabels
// FullName:  InitNodeLabels
// Access:    public 
// Returns:   int, 0 if success,-1 otherwise.
// Qualifier:
// Parameter: vector<string> & arrNode, output,it should be initilize before the calling. 
// Parameter: string prefix, the prefix of the node label. Eg,Mod, OD, S,.... 
// Parameter: int steps, less than 999 
// Comments: always, the count of such kind of node we need is as the same as the steps'[n] except Node'OO'[n-1],
//************************************
int InitNodeLabels(vector<string>& arrNode, string prefix, int steps)
{
//	for ( int j = 0; j < arrNode.size(); j++ ){
//	  cout <<j<<" "<< arrNode[j] << endl;
//   }
//	cout<<"Inside InitNodeLabels : "<<endl;
	char *tmpString = new char[5];    //it means that the bound of step is 99999  
	string str;
//	cout<<"Here 1"<<endl;
	for (int i = steps; i>=1; i--)
	{
//		cout<<"Here 1-"<<i<<endl;
		//_itoa_s(i,tmpString,5,10);
		sprintf(tmpString, "%d", i);
//		cout<<"Here 1-"<<i<<"-a"<<endl;
		str = prefix + string(tmpString);
//		cout<<"Here 1-"<<i<<"-b: "<<str<<endl;
		arrNode.push_back(str);
//		cout<<"Here 1-"<<i<<"-c"<<endl;
	}
	delete[] tmpString;
//	cout<<"exiting InitNodeLabels"<<endl;
//	for ( int i = 0; i < m_arrModNode.size(); i++ ){
//	   	for ( int j = 0; j < arrNode.size(); j++ ){
//		  cout <<j<<" "<< arrNode[j] << endl;
//	   }
//	   cout << endl;
//	}
	return 0;
}

vector<string>& stringsplit(const string& str, 
							vector<string>& tokens, const string& delimiters)
{
	// Skip delimiters at beginning. 
	string::size_type lastPos = str.find_first_not_of(delimiters, 0); 
	// Find first "non-delimiter". 
	string::size_type pos     = str.find_first_of(delimiters, lastPos); 

	while (string::npos != pos || string::npos != lastPos) 
	{ 
		// Found a token, add it to the vector. 
		tokens.push_back(str.substr(lastPos, pos - lastPos)); 
		// Skip delimiters.  Note the "not_of" 
		lastPos = str.find_first_not_of(delimiters, pos); 
		// Find next "non-delimiter" 
		pos = str.find_first_of(delimiters, lastPos); 
	} 

	return tokens;
}

bool IsFileExist(string fileName)
{
	//FILE* pf = NULL;
	//fopen_s(&pf,fileName.c_str(),"r");
	FILE* pf = fopen(fileName.c_str(),"r");
	return pf;
}

vector<float>& str2num(vector<string>& strArr,vector<float>& fltArr)
{
	for (size_t i = 0; i < strArr.size(); i++)
	{
		fltArr.push_back(static_cast<float>(atof(strArr.at(i).c_str())));
	}
	return fltArr;
}

void distingush( vector<int>& arr1, vector<int>& arr2, vector<int>& diff )
{
	//merge
	diff.insert(diff.end(),arr1.begin(),arr1.end());
	diff.insert(diff.end(),arr2.begin(),arr2.end());

	//remove the same ones
	sort(diff.begin(),diff.end());
	vector<int>::iterator new_pos;
	new_pos = unique(diff.begin(),diff.end());
	diff.erase(new_pos,diff.end());
}


float cal_Distance(vector<float>& item1,vector<float>& item2)
{
	float distance=0;

	if (item1.size()!= item2.size())
		distance=-1;
	else
	{
		for(size_t i = 0; i < item2.size(); ++i)
			distance=distance + pow(item1.at(i)-item2.at(i),2);
	}
	return distance;
}


vector<float>& cal_mean(vector<float>& srcPoint, vector<float>& newPoint, int weight)
{
	if (srcPoint.size() != newPoint.size())
		return srcPoint;	
	
	for (size_t i = 0; i < srcPoint.size(); ++i)
	{
		srcPoint.at(i)=(srcPoint.at(i)*weight+newPoint.at(i))/(weight+1);
	}	
	return srcPoint;
}

bool AEIndex( vector<int>& arrAct,vector<int>& indice)
{
	int len = static_cast<int>(arrAct.size());
	
	for (int i = 0; i < len; i++)
	{
		indice.clear();
		FindIndice(arrAct,arrAct.at(i),indice);
		if (indice.size() > 1)
		{
			//more than 1 same element. it could be compressed.
			//and remove the redundant elements in arrAct
			for (int j = static_cast<int>(indice.size()-1); j > 0 ; j--)
			{
				arrAct.erase(arrAct.begin()+indice.at(j));
			}
			return true;
		}
	}
	return false;
}

void FindIndice( vector<int>& arr, int value, vector<int>& indice )
{
	int len = static_cast<int>(arr.size());	

	for (int i = 0; i < len; i++)
		if (arr.at(i) == value)	
			indice.push_back(i);	
}

vector<float>& vec_PMultiply( vector<float>& arr,float ratio )
{
	for (size_t i = 0; i < arr.size(); i++)
	{
		arr.at(i) = arr.at(i)*ratio;
	}
	return arr;
}
void arr_PMultiply(float* arrO,float* arrD, float ratio,int num)
{
	float* temp_O=arrO;
	float* temp_D=arrD;
	for (int i=0;i<num;i++)
	{
		*temp_D=(*temp_O)*ratio;
		temp_O++;
		temp_D++;
	}
	
}

float vec_Sum( vector<float>& arr )
{
	float res = 0;
	for (size_t i = 0; i < arr.size(); i++)
	{
		res = res + arr.at(i);
	}	
	return res;
}

float vec_Sum( vector<float>& arr, vector<int>& indice )
{
	float res = 0;
	for (size_t i = 0; i < indice.size(); i++)
	{
		res = res + arr.at(indice.at(i));
	}	
	return res;
}
float vec_Mean( vector<float>& arr )
{
	return vec_Sum(arr)/static_cast<float>(arr.size());
}

vector<float>& vec_PAdd(vector<float>& res,vector<float>& arr)
{
	if (res.size() != arr.size())
	{
		res.resize(arr.size());
	}

	for (size_t i = 0; i < arr.size(); i++)
	{
		res.at(i) = res.at(i)+arr.at(i);
	}
	return res;
}

vector<float>& vec_EraseItem( vector<float>& arr, int index )
{
	if (index >= 0)
	{
		arr.erase(arr.begin()+index);
	}
	return arr;
}


vector<int>& select_k_items(vector<float> dis,int k,int item_size,vector<int>& k_item)
{
	vector<int> index_temp;
	float p=0;	
	if(item_size<k)
	{		
		for (int i=0;i<item_size;i++)
			k_item.push_back(i);		
		return k_item;
	}
	
	for (int j=0;j<item_size;j++)		
	{
		index_temp.push_back(j);
	}

	for(int i=0;i<k;i++)
	{
		//d_temp=*(index_temp+i+1);
		p=dis.at(index_temp.at(i));
		for(int j=i+1;j<item_size;j++)
		{
			if(p<dis.at(index_temp.at(j)))
			{
				p=dis.at(index_temp.at(j));
				int temp_index=index_temp.at(j);
				index_temp.at(j)=index_temp.at(i);
				index_temp.at(i)=temp_index;
			}
		}
		k_item.push_back(index_temp.at(i));
	}
	index_temp.clear();
	return k_item;
}

int throwDice(float* dist, int len)
{
	float *pdf = new float[len];
	pdf[0] = dist[0];

	for (int i = 1; i < len; i++)
	{
		pdf[i] = dist[i] + pdf[i-1];
	}

	float r = (float)rand()/(float)RAND_MAX;
	for (int i = 0; i < len; ++i)
	{
		if (r <= pdf[i])   //in case of r = 1.0
		{
			return i;
		}
	}
	return len-1;     //return -1;  
}

vector<int>& vec_WeightUpdate( vector<int>& weight,vector<int>& indice )
{
	for (size_t i = 1; i < indice.size(); ++i)
	{
		replace(weight.begin(),weight.end(),indice.at(i),indice.at(0));
	}
	return weight;
}

vector<int> vec_FindSubVec( vector<int>& weight, int index )
{
	vector<int> res;
	for (size_t i = 0; i < weight.size(); ++i)
	{
		if (weight.at(i) == index)
		{
			res.push_back(i);
		}
	}
	return res;
}
