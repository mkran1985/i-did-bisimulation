#include "stdafx.h"
#include "string.h"
#include "MyHuginEx.h"



#pragma once;
#include "IDID.h"

float* getNodeUtilities(Domain *domain, string label,float** result,int& nbState)
{
	//float* result;
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(domain,label);	
	return getNodeUtilities(pNode, result, nbState);
}
float* getNodeUtilities(DiscreteNode* pNode, float** result, int& nbState)
{
	if (pNode)
	{
		nbState = pNode->getNumberOfStates();
		//The Utilities[3] of this node are: 1.0, 1.0, 2.0.
		if(_IDID_OUTPUT_)	
			cout<<"The Utilities("<<nbState<<") of this node ["<<pNode->getLabel()<<"] are:"; 
	
		if (!(*result))
		{
			*result = new float[nbState];
		}
		for (int i = 0; i<nbState; i++)
		{
			(*result)[i] = pNode->getExpectedUtility(i);
		if(_IDID_OUTPUT_)
			cout<<(*result)[i]<<", ";

		}
		if(_IDID_OUTPUT_)
			cout<<"\n";
		return *result;
	}
	return NULL;
}
void setNodeState(Domain* pDomain, string label, int state)
{
	DiscreteNode* dNode = (DiscreteNode*)getNodeByLabel(pDomain,label);
	if (dNode)
	{
		dNode->selectState(state);
	}	
}

Node* getNodeByLabel(Domain *domain, string label)
{
//	cout<<"Here..3-1"<<endl;
	NodeList ndList = domain->getNodes();
//	cout<<"Here..3-2"<<endl;
	size_t len = ndList.size();
//	cout<<"Here..3-3"<<endl;
	Node* node = NULL;
	string str;
	for (size_t i = 0; i<len;i++)
	{
		node = ndList[i];
		str = node->getLabel();

		if (!strcmp(str.c_str(),label.c_str()))
		{
//			cout<<str.c_str()<<"=="<<label.c_str()<<endl;
			return node;
		}else{
//			cout<<str.c_str()<<"!="<<label.c_str()<<endl;
		}
	}
//	cout<<"Here..3-4"<<endl;
	return NULL;
}


float* getNodeBelief(Domain *domain, string label,float** result, int& nbState)
{
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(domain,label);
	return getNodeBelief(pNode, result, nbState);
	
}
float* getNodeBelief(Domain *domain, string label,float** result)
{
	int nbState;
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(domain,label);
	return getNodeBelief(pNode, result, nbState);

}
float* getNodeBelief(DiscreteNode* pNode,float** result, int& nbState)
{
	if (pNode)
	{
		nbState = pNode->getNumberOfStates();
		if(_IDID_OUTPUT_)
			cout<<"The Beliefs ("<<nbState<<") of this node ["<<pNode->getLabel()<<"] are:";
		if (!(*result))
		{
			*result = new float[nbState];
		}		
		for (int i = 0; i<nbState; i++)
		{
			(*result)[i] = pNode->getBelief(i);
			if(_IDID_OUTPUT_)
				cout<<(*result)[i]<<", ";
		}
		if(_IDID_OUTPUT_)
			cout<<"\n";
		return *result;
	}
	return NULL;
}

size_t getBestChoice(Domain *domain, string label)
{
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(domain,label);
	return getBestChoice(pNode);
}
size_t getBestChoice(DiscreteNode* pNode)
{
	int nbState = pNode->getNumberOfStates();
	float* pf = new float[nbState];
	getNodeUtilities(pNode,&pf,nbState);
	return findMax(pf,nbState);
}
HAPI::Number getTableItem(const Table* table, int row,int col,int nbRow )
{
	NumberList numList;
	NodeList ndList;
	Number result;
	//DiscreteNode* currnode;
	int nbCol;  //the count of rows,cols
	int index;

	if (nbRow < 0)  //not specified
		getTableSize(table,nbRow,nbCol);

	numList = table->getData();	
	index = col*nbRow+row;
	result = numList.at(index);
	
	return result;
}
void setTableColumn(Table* table,int colIndex,Number val, int nbRow)
{
	//use vector.insert will imporve the performance a lot.
	NumberList numList;
	NodeList ndList;	
	//DiscreteNode* currnode;
	int nbCol = 0;  //the count of rows,cols
	int index;

	if (nbRow < 0)  //not specified
		getTableSize(table,nbRow,nbCol);

	numList = table->getData();
	index =  colIndex*nbRow;
	numList.erase(numList.begin()+index,numList.begin()+index+nbRow);
	numList.insert(numList.begin()+index,nbRow,val);            // .... 
	table->setData(numList); 
	return;
}

void setTableColumn( Table* table,int colIndex,vector<int> valarr,int nbRow )
{
	NumberList numList;
	NodeList ndList;	
	//DiscreteNode* currnode;
	int nbCol = 0;  //the count of rows,
	int index;
	
	if (nbRow < 0)  //not specified			
		getTableSize(table,nbRow,nbCol);	

	numList = table->getData();
	index =  colIndex*nbRow;
	numList.erase(numList.begin()+index,numList.begin()+index+nbRow);
	// .... 
	numList.insert(numList.begin()+index,valarr.begin(),valarr.end());
	table->setData(numList); 
	return;
}
void setTableItem(Table* table,int row,int col,Number val, int nbRow)
{
	NumberList numList;
	NodeList ndList;	
	//DiscreteNode* currnode;
	int nbCol;  //the count of rows,
	int index;

	if (nbRow < 0)  //not specified			
		getTableSize(table,nbRow,nbCol);	

	numList = table->getData();
	index = col*nbRow+row;
	numList.at(index) = val;
	table->setData(numList);

	return;
}

void getTableSize( const Table* table, int& nbRow, int& nbCol )
{
	NumberList numList;
	NodeList ndList;	
	DiscreteNode* currnode;
	
	numList = table->getData();
	ndList = table->getNodes();
	currnode = (DiscreteNode*)ndList.at(ndList.size()-1);   //the last one of the nodeList,
	nbRow = currnode->getNumberOfStates();
	nbCol = numList.size()/nbRow;
	
	return;
}

void setModelMapColumn( Table* table,int col,int row,int nbRow )
{
	setTableColumn(table,col,0,nbRow);    
	setTableItem(table,row,col,1,nbRow);
}


void setNumberOfStatesEx( DiscreteNode* pNode,int n )
{
	if(_IDID_DEBUG_){
		cout<<"Inside setNumberOfStatesEx: "<<endl;
		cout<<pNode->getLabel()<<" "<<n<<endl;
	}
	char *tmp = new char[10];

	if(_IDID_DEBUG_){
		cout<<"before pNode->setNumberOfStates(n)"<<endl;
	}
	pNode->setNumberOfStates(n);
	if(_IDID_DEBUG_){
		cout<<"after pNode->setNumberOfStates(n)"<<endl;
	}
	for (int i = 0; i < n; i++)
	{
		if(_IDID_DEBUG_){
			cout<<"i = "<<i<<endl;
		}
		//_itoa_s(i,tmp,10,10);
		sprintf(tmp, "%d", i);
		pNode->setStateLabel(i,tmp);
	}	
	delete[] tmp;
	if(_IDID_DEBUG_){
		cout<<"leaving setNumberOfStatesEx"<<endl;
	}
}

//************************************
// Method:    getNbUtilityNode
// FullName:  getNbUtilityNode
// Access:    public 
// Returns:   int, the number of utility nodes, i.e., the number of steps.
// Qualifier: 
// Parameter: Domain * pDomain -- the net file
// Comments: the number of utility nodes can be used to get the number of steps.
//************************************
int getNbUtilityNode(Domain* pDomain)
{
	if(_IDID_DEBUG_){
		cout<<"Inside getNbUtilityNode.."<<endl;
	}
	NodeList ndList = pDomain->getNodes();
	size_t len = ndList.size();
	if(_IDID_DEBUG_){
		cout<<"len:"<<len<<endl;
	}
	Node* node = NULL;
	Category cate;
	int num = 0;
	for (size_t i = 0; i < len;i++)
	{
		node = ndList[i];
		if(_IDID_DEBUG_)
			cout<<node->getLabel()<<endl;
		cate = node->getCategory();
		if(_IDID_DEBUG_)
			cout<<"cate:"<<cate<<endl;

		if (cate == H_CATEGORY_UTILITY)
			num++;
	}

//	NodeList ndList = pDomain->getNodes();
//	size_t len = ndList.size();
//	cout<<"len:"<<len<<endl;
//	int count =0;
//	for (NodeList::const_iterator it = ndList.begin(); it != ndList.end(); ++it){
//		cout<<count<<endl;
//		Node *node = *it;
//
//		Category category = node->getCategory();
//		char type = (category == H_CATEGORY_CHANCE ? 'C'
//			     : category == H_CATEGORY_DECISION ? 'D'
//			     : category == H_CATEGORY_UTILITY ? 'U' : 'F');
//
//		cout << "\n[" << type << "] " << node->getLabel()
//		     << " (" << node->getName() << ")\n";
//
//		count++;
//	}

	return num;
}

//************************************
// Method:    getNbDecisionNode
// FullName:  getNbDecisionNode
// Access:    public
// Returns:   int, the number of decision nodes, i.e., the number of steps.
// Qualifier:
// Parameter: Domain * pDomain -- the net file
// Comments: the number of desicion nodes can be used to get the number of steps.
//************************************
int getNbDecisionNode(Domain* pDomain)
{
	if(_IDID_DEBUG_)
		cout<<"Inside getNbDecisionNode.."<<endl;
	NodeList ndList = pDomain->getNodes();
	size_t len = ndList.size();
	if(_IDID_DEBUG_)
		cout<<"Number of nodes in the network:"<<len<<endl;
	Node* node = NULL;
	Category cate;
	int num = 0;
	for (size_t i = 0; i < len;i++)
	{
		node = ndList[i];
		if(_IDID_DEBUG_)
			cout<<node->getLabel()<<endl;
		cate = node->getCategory();
		if(_IDID_DEBUG_)
			cout<<"cate:"<<cate<<endl;

		if (cate == H_CATEGORY_DECISION)
			num++;
	}

	return num;
}

int getNbObservation(Domain * pDomain)
{
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(pDomain,"O1");
	return pNode->getNumberOfStates();

}
vector<float>& getTableColumn( Table* table,int colIndex,vector<float>& val, int nbRow /*= -1*/ )
{
	//use vector.insert will imporve the performance a lot.
	NumberList numList;
	//NodeList ndList;	
	//DiscreteNode* currnode;
	int nbCol = 0;  //the count of rows,cols
	int index;

	if (nbRow < 0)  //not specified
		getTableSize(table,nbRow,nbCol);

	numList = table->getData();
	index =  colIndex*nbRow;
	val.resize(nbRow);
	copy(numList.begin()+index,numList.begin()+index+nbRow,val.begin());

	return val;
}

int pickNodeState( DiscreteNode* pNode )
{
	int currState;
	if (!pNode)
		return -1;

	float* pDist = NULL;
	int nbState;
	getNodeBelief(pNode,&pDist,nbState);
	if (pDist)
	{
		currState = throwDice(pDist,nbState);
		if (currState < 0)
		{
			cout<<"Get a very strange result...";
		}
		delete[] pDist;
		return currState;
	}
	else
	{
		delete[] pDist;
		return -1;
	}
}
