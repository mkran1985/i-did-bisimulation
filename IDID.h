#pragma once;

#include "hugin"
#include <iostream>
#include <cmath>

#include <math.h>
#include <time.h>
#include "utility"
#include <fstream>
//#include <Windows.h>
#include <algorithm>
#include <map>
using namespace HAPI;
using namespace std;

#include "Accessory.h"
#include "MyHuginEx.h"

//print more detail information.
#define _IDID_OUTPUT_  false

//print more detail information.
#define _IDID_EVAL_OUTPUT_  false

//print more detail information.
#define _IDID_EXPAND_OUTPUT_  true

//print more detail information.
#define _IDID_DEBUG_  false

//print the time consuming
#define _IDID_TIME_  true

//measure of distance between distributions
//whether they are close enought
#define _IDID_EPSILON_ (0.1)    

//enum Merge_Method { NO_Merge = 0, BE, DMU, AE, E_BE,E_DMU, E_AE};
enum Merge_Method { NO_Merge = 0, BE, DMU, AE, E_BE,E_DMU, E_AE, BIS};
enum Forest_Type {NORMAL_F,GRAPH_F};
enum Combination_Method {NO_Combine,Epsilon_Dis};



