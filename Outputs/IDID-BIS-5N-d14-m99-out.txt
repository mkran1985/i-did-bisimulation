Evaluation Summary: 
	[5-Agents] Running for [10] times; [3] Steps; With parameter [para2_99_7,para2_99_7_r,para2_99_7_r,para2_99_7]; and with [BIS] method; Discount factor = 0.9 and Epsilon = 0.14

Bisimulation
-----------------------------------------------
Epsilon = 0.14
DID (Other Agent 0): Debug/DID_Model/T03.net
DID (Other Agent 1): Debug/DID_Model/T13.net
DID (Other Agent 2): Debug/DID_Model/T23.net
DID (Other Agent 3): Debug/DID_Model/T33.net
IDID (Subject Agent): Debug/IDID_Model/TIDID3_5N.net
Parameter file (Other Agent 0): Debug/para/para2_99_7.txt
Parameter file (Other Agent 1): Debug/para/para2_99_7_r.txt
Parameter file (Other Agent 2): Debug/para/para2_99_7_r.txt
Parameter file (Other Agent 3): Debug/para/para2_99_7.txt
-----------------------------------------------

Instantiating decision networks for agents..
-----------------------------------------------
	Instantiating Debug/DID_Model/T03.net..
	Instantiating Debug/DID_Model/T13.net..
	Instantiating Debug/DID_Model/T23.net..
	Instantiating Debug/DID_Model/T33.net..
	Instantiating Debug/IDID_Model/TIDID3_5N.net..
Finished instantiating decision networks for all agents..

Computing the stochastic bisimulation partition of the belief simplex..
-----------------------------------------------
Split #: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 
Final Partition: [{(0.25,0.75) (0.375,0.625)} {(0.625,0.375) (0.75,0.25)} {(0,1) (0.0625,0.9375)} {(0.0625,0.9375) (0.125,0.875)} {(0.125,0.875) (0.1875,0.8125)} {(0.375,0.625) (0.4375,0.5625)} 
{(0.5625,0.4375) (0.625,0.375)} {(0.8125,0.1875) (0.875,0.125)} {(0.875,0.125) (0.9375,0.0625)} {(0.9375,0.0625) (1,0)} {(0.1875,0.8125) (0.21875,0.78125)} 
{(0.21875,0.78125) (0.25,0.75)} {(0.4375,0.5625) (0.46875,0.53125)} {(0.46875,0.53125) (0.5,0.5)} {(0.5,0.5) (0.53125,0.46875)} {(0.53125,0.46875) (0.5625,0.4375)} 
{(0.75,0.25) (0.78125,0.21875)} {(0.78125,0.21875) (0.8125,0.1875)}]
	Final Partition Size: 18
	The time consumed for computing the stochastic bisimulation partition is 801s.

Grouping the candidate models of each agent (from Parameter file) according to the bisimulation partition..done!

Solving 1 candidate model per block in the bisimulation partition..
-----------------------------------------------
Passing the obtained policy trees to create the forest ..done!
	Total number of other agents' models solved: 18
	The time consumed for solving 18 models is 0.02s.
Finished solving level-0 DIDs for other agents..

Solving level-1 model of the subject agent..
-----------------------------------------------
Expanding the I-DID over time.. 
	The time consumed for expanding the I-DID is 0.15s.
Finished expanding the I-DID over time.. 

Evaluating Subject Agent's I-DID
-----------------------------------------------
Trial #:0 1 2 3 4 5 6 7 8 9 ..done!
	The time consumed for evaluation is 2390000 for [10] times. [239000] for each
	The mean utility is -> 26. For each step is 8.66667. 
[4]-[6],[59]-[4],
The utility need to occur more than 10% times in the array, otherwise it is a noise. The smoothed value is 26

