#include "GraphForest.h"

CGraphForest::CGraphForest(void)
{
	m_eType = GRAPH_F;
}

CGraphForest::CGraphForest(int horizon,int nbEvi,int nbAct)
{	
	m_eType = GRAPH_F;
	m_iNbHorizon = horizon;
	m_iNbBranch = nbEvi;
	m_iNbAction = nbAct;
}
CGraphForest::~CGraphForest(void)
{
	for (size_t i = 0; i < m_arrNode.size(); i++)
	{
		delete m_arrNode.at(i);
	}
	m_arrNode.clear();
	m_arrIdcLayer.clear();
	m_arrWeight.clear();
}

void CGraphForest::AddTree(CSequenceTree* pTree, size_t index_tree,Merge_Method method)
{	
	GF_Node* pTmpNode;	
	size_t prevSize = m_arrNode.size();
	m_arrNode.resize(prevSize+pTree->GetSizeofTree());
	m_arrIdcLayer.resize(prevSize+pTree->GetSizeofTree());

	//insert nodes into forest
	for (int i = pTree->GetSizeofTree()-1; i >= 0; i--)
	{
		pTmpNode = new GF_Node(m_iNbBranch);
		pTmpNode->action = pTree->GetActArr().at(i);
		pTmpNode->layer = pTree->GetObsArr().at(i)+1;
		if (pTree->GetAncArr().at(i) == -1)		//root
			pTmpNode->pFather = -1;	
		else		
			pTmpNode->pFather = pTree->GetAncArr().at(i)+prevSize;		

		m_arrIdcLayer.at(i+prevSize) = pTree->GetObsArr().at(i)+1;

		int *pChild = pTree->GetActChildren(i);   //new pChild
		if (pChild)  //has children
		{
			for (int j = 0; j < m_iNbBranch; j++)
			{
				pTmpNode->ppChildren[j] = m_arrNode.at(prevSize + pChild[j]);
			}			
		}		
		m_arrNode.at(prevSize+i) = pTmpNode;
		delete[] pChild;
	}
	//m_arrNode.at(prevSize) is the node of the tree which has been inserted just now.
	//m_arrWeight.push_back(prevSize);

	//merge
	for(int i = pTree->GetSizeofTree()-1; i >= 0; i--)
	{
		pTmpNode = m_arrNode.at(prevSize+i);
		int index = prevSize+i;
		if (ExistSameAction(pTmpNode,index))
		{	
			//there are more than 1 child, we need to find out which transition shall be changed.
			int trans = -1;; 
			GF_Node* pFNode; 
			if (pTmpNode->pFather >= 0)
			{
				pFNode = m_arrNode.at(pTmpNode->pFather);
				//change ancestor's links
				for (int j = 0; j < m_iNbBranch; j++)
				{
					if (pFNode->ppChildren[j] == pTmpNode)
					{
						trans = j;
						break;
					}
				}
				pFNode->ppChildren[trans] = m_arrNode.at(index);
			}
			else  //pTmpNode is the root,don't need to change the ancestor's links
			{
				int oldVal = GetTreeIndex(index);
				switch (method)
				{
				case DMU:
					m_arrWeight.at(index_tree) = oldVal;  
					break;
				case AE:
					m_arrWeight.at(index_tree) = oldVal;  
					break;
				case E_DMU:
					replace(m_arrWeight.begin(),m_arrWeight.end(),m_arrIndofCombd.at(index_tree),m_arrIndofCombd.at(oldVal));
					m_arrIndofCombd.at(index_tree) = m_arrIndofCombd.at(oldVal);
					break;
				case E_AE:
					replace(m_arrWeight.begin(),m_arrWeight.end(),m_arrIndofCombd.at(index_tree),m_arrIndofCombd.at(oldVal));
					m_arrIndofCombd.at(index_tree) = m_arrIndofCombd.at(oldVal);
					break;
				}				
			}
			
			delete pTmpNode;
			m_arrNode.at(prevSize+i) = NULL;
			m_arrIdcLayer.at(prevSize+i) = 0;			
		}
		else if (pTmpNode->layer == 1)
		{
			switch (method)
			{
			case DMU:
				m_arrWeight.at(index_tree) = GetLayerSize(1)-1;
				break;
			case AE:
				m_arrWeight.at(index_tree) = GetLayerSize(1)-1;
				break;
			case E_DMU:
				break;
			case E_AE:
				break;
			}
			//
		}
	}//for

	//some nodes have been removed
	//but their pointer[Null pointer] are still in the vector
	//copy and clean
	vector<GF_Node*> newArr;
	vector<int> newIdc;
	for (size_t i = 0; i < m_arrNode.size(); i++)
	{
		pTmpNode = m_arrNode.at(i);
		if (pTmpNode)
		{
			newArr.push_back(pTmpNode);
			newIdc.push_back(m_arrIdcLayer.at(i));
		}
	}
	m_arrNode.clear();
	m_arrNode.assign(newArr.begin(),newArr.end());
	m_arrIdcLayer.clear();
	m_arrIdcLayer.assign(newIdc.begin(),newIdc.end());
}


bool CGraphForest::ExistSameAction(GF_Node* pNode, int& index)
{
	GF_Node* pTmpNode;
	for (size_t i = 0; i < m_arrNode.size(); i++)
	{
		pTmpNode = m_arrNode.at(i);
		if (GFNodeEqual(pNode,pTmpNode))		
		{	
			if (i == index)			
				continue;    //itself			
			else
			{
				index = i;   //return the object index
				return true;
			}				
		}	
	}
	return false;
}


bool CGraphForest::GFNodeEqual(GF_Node* pNode1, GF_Node* pNode2 )
{
	if (!pNode1||!pNode2)   //one node is empty
	{
		return false;
	}
	
	if (!pNode1&&!pNode2)   //2 empty nodes are the same
	{
		return true;
	}

	if (pNode1->layer != pNode2->layer)	
		return false;	
	if (pNode1->action != pNode2->action)	
		return false;

	for (int i = 0; i < m_iNbBranch; i++)
	{
		if (pNode1->ppChildren[i] != pNode2->ppChildren[i])	  //depanding on the address	
			return false;		
	}
	return true;
}

void CGraphForest::Print()
{
	cout<<".\n.\n.\n--------This is a Graph Forest-----\n";
	cout<<"-----Steps: "<<m_iNbHorizon<<"\n";
	cout<<"-----Evidence(Branches)"<<m_iNbBranch<<"\n";
	for (size_t i = 0; i < m_arrNode.size(); i++)
	{
		GF_Node* pTmpNode;
		pTmpNode = m_arrNode.at(i);
		cout<<"The["<<i<<"/"<<m_arrNode.size()<<"] node--";
		cout<<"Address--"<<pTmpNode;
		cout<<",Action:_["<<pTmpNode->action<<"]"<<",Layer:_["<<pTmpNode->layer<<"]\n";
		for (int j = 0; j<m_iNbBranch; j++)
		{
			cout<<"_____the["<<j<<"/"<<m_iNbBranch<<"] child"<<pTmpNode->ppChildren[j]<<"\n";
		}
	}

}


int CGraphForest::GetLayerSize( int layer )
{
	int res = 0;

	for (size_t i = 0; i < m_arrIdcLayer.size(); i++)
		if (m_arrIdcLayer.at(i) == layer)
			res++;		

	return res;
}

vector<int> CGraphForest::GetStepAct( int step )
{
	vector<int> arrAct;
	for (size_t i = 0; i < m_arrIdcLayer.size(); i++)
	{	
		if (m_arrIdcLayer.at(i) == step)
		{
			arrAct.push_back(m_arrNode.at(i)->action);
		}
	}
	return arrAct;
}

//the index of return value of ExistSameAction() the index of node in the nodes' array
//but we need the index of the tree.
int CGraphForest::GetTreeIndex(int index)
{
	int res = 0;
	for (size_t i = 0; i < index; i++)
	{
		if (m_arrIdcLayer.at(i) == 1)
		{
			++res;
		}
	}
	return res;
}

GF_Node* CGraphForest::FindNode( int layer,int index )
{
	GF_Node* pNode;
	int count = 0;
	for (size_t i = 0; i < m_arrIdcLayer.size(); i++)
	{
		if (m_arrIdcLayer.at(i) == layer)
		{	
			if(index == count)
				pNode = m_arrNode.at(i);
			count++;
		}
	}
	return pNode;
}

int CGraphForest::FindIndex( GF_Node* pNode, int layer )
{
	int count = -1;
	for (size_t i = 0; i < m_arrIdcLayer.size(); i++)
	{
		if (m_arrIdcLayer.at(i) == layer)
		{	
			count++;
			if (pNode == m_arrNode.at(i))
			{
				return count;
			}
		}
	}
	return -1;
}

int CGraphForest::GetMap( int layer,int index,int trans )
{

	GF_Node* pNode = FindNode(layer,index);
	cout<<"here2"<<endl;
	GF_Node* pCNode = pNode->ppChildren[trans];
	cout<<"here3"<<endl;
	//cout<<"here"<<endl;
	//for(int k=0;k<1000000;k++);
	int res = FindIndex(pCNode,layer+1);
	cout<<"here4"<<endl;
	return res;
}
int CGraphForest::GetFMap( int layer,int index )
{
	GF_Node* pNode = FindNode(layer,index);
	//GF_Node* pCNode = pNode->pFather;
	//int res = FindIndex(pCNode,layer-1);
	return m_arrNode.at(pNode->pFather)->action;
}

