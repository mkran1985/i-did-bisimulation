
#include "AdvDID.h"

#include "GraphForest.h"

CAdvDID::CAdvDID(void)
{
}

//use the ctor of base class to finish the main job
//add 3 extra node.
//CAdvDID::CAdvDID( string netFile ):CBasDID(netFile)
//{
//	InitNodeLabels(m_arrODNode[0],"OD",m_iNbSteps);
//	InitNodeLabels(m_arrOONode[0],"OO",m_iNbSteps-1);   //OO is start from n-1
//	InitNodeLabels(m_arrModNode[0],"Mod",m_iNbSteps);
//}

CAdvDID::CAdvDID( string netFile, int m_iNbOthers ):CBasDID(netFile)
{
	CAdvDID::m_iNbOthers = m_iNbOthers;
	vector<vector<string> > m_arrODNode(m_iNbOthers);
	vector<vector<string> > m_arrOONode(m_iNbOthers);
	vector<vector<string> > m_arrModNode(m_iNbOthers);

	if(_IDID_DEBUG_)
		cout<<"Inside CAdvDID.."<< netFile <<endl;

	for(int oAg=0;oAg<m_iNbOthers; oAg++){
		if(_IDID_DEBUG_)
			cout<<"Other agent "<<oAg<<endl;

		stringstream ss;
		ss << "OD" << oAg;
		if(_IDID_DEBUG_)
			cout<<ss.str()<<endl;
		InitNodeLabels(m_arrODNode[oAg],ss.str(),m_iNbSteps);

		stringstream ss1;
		ss1 << "OO" << oAg;
		if(_IDID_DEBUG_)
			cout<<ss1.str()<<endl;
		InitNodeLabels(m_arrOONode[oAg],ss1.str(),m_iNbSteps-1);   //OO is start from n-1

		stringstream ss2;
		ss2 << "Mod" << oAg;
		if(_IDID_DEBUG_)
			cout<<ss2.str()<<endl;
		InitNodeLabels(m_arrModNode[oAg],ss2.str(),m_iNbSteps);
	}
	CAdvDID::m_arrODNode = m_arrODNode;
	CAdvDID::m_arrOONode = m_arrOONode;
	CAdvDID::m_arrModNode = m_arrModNode;

	if(_IDID_DEBUG_){
		cout<<"m_arrModNode.size()....."<<m_arrModNode.size()<<endl;
		for ( int i = 0; i < m_arrModNode.size(); i++ ){
			cout<<"m_arrModNode[i].size()......"<<m_arrModNode[i].size()<<endl;
		   for ( int j = 0; j < m_arrModNode[i].size(); j++ ){
			  cout <<i<<" "<<j<<" "<< m_arrModNode[i][j] << endl;
		   }
		   cout << endl;
		}
	}
}
CAdvDID::~CAdvDID(void)
{
}

void CAdvDID::ExpandModelwithForest(vector<CForest*> forestArr, string destFile){
	#define _IDID_DEBUG_ true
	#define _IDID_OUTPUT_  true
	cout<<"Expanding the I-DID over time.. "<<endl;

	vector<DiscreteNode*> ModelNodeArr(m_iNbOthers);
	vector<DiscreteNode*> ODNodeArr(m_iNbOthers);
	vector<DiscreteNode*> pTmpNodeArr(m_iNbOthers);
	string line = "--------------";

	m_pDomain->uncompile();
	for(int oAg=0;oAg<m_iNbOthers;oAg++){
		if(_IDID_DEBUG_){
			cout<<"-- Other Agent "<<oAg<<endl;
			cout<<line<<endl;
		}
		CForest& forest = *(forestArr[oAg]);
		if(_IDID_DEBUG_)
			cout<<"Here..1"<<endl;
		//how many steps
		int steps = forest.GetNbofLayer();
		if(_IDID_DEBUG_)
			cout<<"Here..2  steps = "<<steps<<endl;

		Table *tab;
		NodeList ndList;
		int currNumofModel,nbBranch;

		vector<int> currentDicision;
		vector<int> lastDecision;


		nbBranch = forest.GetNbofBranch();
		if(_IDID_DEBUG_)
			cout<<"Here..3  nbBranch = "<<nbBranch<<endl;

		if(_IDID_DEBUG_){
			for ( int i = 0; i < m_arrModNode.size(); i++ ){
				for ( int j = 0; j < m_arrModNode[i].size(); j++ ){
					cout << m_arrModNode[i][j] << ' ';
				}
				cout << endl;
			}
		}

		//the first step is special, no model mapping..

		if(_IDID_DEBUG_)
			cout<<"Here..4  m_arrModNode["<<oAg<<"][0] = "<<m_arrModNode[oAg][0]<<endl;
		ModelNodeArr[oAg] = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode[oAg][0]);
		if(_IDID_DEBUG_)
			cout<<"------ Model Node Arr["<<oAg<<"] Label: "<<ModelNodeArr[oAg]->getLabel()<<endl;

		ODNodeArr[oAg] = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode[oAg][0]);
		if(_IDID_DEBUG_)
			cout<<"Here..5  m_arrODNode["<<oAg<<"][0] = "<<m_arrODNode[oAg][0]<<endl;

		currNumofModel = forest.GetLayerSize(1);  //---
		if(_IDID_EXPAND_OUTPUT_)
			cout<<"currNumofModel [0] = "<<currNumofModel<<endl;

		if(_IDID_DEBUG_)
			cout<<"Here..7a  setNumberOfStatesEx"<<endl;
		setNumberOfStatesEx(ModelNodeArr[oAg],currNumofModel);
		if(_IDID_DEBUG_)
			cout<<"Here..7b  setNumberOfStatesEx"<<endl;

		//set model weight;
		vector<int> weight;
		forest.CalWeight(weight);
		tab = ModelNodeArr[oAg]->getTable();
		if (tab->getNodes().size() > 1){   //there is a link between S[n] and Mod[n]
			pTmpNodeArr[oAg] = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(0));
			for (size_t i = 0; i < pTmpNodeArr[oAg]->getNumberOfStates(); i++){
				setTableColumn(tab,i,weight,currNumofModel);
			}
		}
		else{
			setTableColumn(tab,0,weight,currNumofModel);
		}

		if(_IDID_DEBUG_)
			cout<<"Here..8  set model weight"<<endl;
		currentDicision = forest.GetStepAct(1);

		if(_IDID_DEBUG_){
			cout<<"Here..9  currentDecision"<<endl;
			for(int i=0;i<currentDicision.size();i++){
				cout<<currentDicision[i]<<",";
			}
			cout<<endl;
		}
		tab = ODNodeArr[oAg]->getTable();
		//only one column, of course, set the first column,...
		for (size_t i = 0; i < currentDicision.size(); i++){
			setModelMapColumn(tab,i,currentDicision.at(i),m_iNbAction);
		}

		if(_IDID_OUTPUT_)
			cout<<"Step [1 out of "<<steps<<"] has finished"<<endl;

		size_t cntTop;
		DiscreteNode* topNode;
		vector<int> inds,mapping;
		for (int i = 1; i < steps; i++)  //second step  and so on.
		{
			//Mod[i], model node
			ModelNodeArr[oAg] = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode[oAg].at(i));
			currNumofModel = forest.GetLayerSize(i+1);	//--
			if(_IDID_EXPAND_OUTPUT_)
				cout<<"currNumofModel [" <<i<<"] "<<" = "<<currNumofModel<<endl;
			//how many states does this node should have.
			setNumberOfStatesEx(ModelNodeArr[oAg],currNumofModel);

			//this the (i)th element, the (i+1)th layer.
			lastDecision = forest.GetStepAct(i);
			if(_IDID_DEBUG_)
				cout<<"Here..9-1"<<endl;
			tab = ModelNodeArr[oAg]->getTable();
			if(_IDID_DEBUG_)
				cout<<"Here..9-2"<<endl;
			//getTableSize(tab,nbRow,nbCol);
			ndList = tab->getNodes();
			if(_IDID_DEBUG_)
							cout<<"Here..9-3"<<endl;
			topNode = (DiscreteNode*)(ndList.at(0));
			if(_IDID_DEBUG_)
							cout<<"Here..9-4"<<endl;
			cntTop = (topNode->getNumberOfStates());
			if(_IDID_DEBUG_)
							cout<<"Here..9-5-cntTop: "<<cntTop<<endl;
			inds.clear();
			mapping.clear();
			vector<int> l_mapping;
			if(_IDID_DEBUG_)
							cout<<"Here..9-6"<<endl;
			for (int j = 0; j < cntTop; j++){
				cout<<"Here..9-6-1++"<<j<<endl;
				cout<<"LD Size: "<<lastDecision.size()<<endl;

				cout<<"Here..9-6-1++father++"<<forest.GetFMap(i+1,j)<<endl;
				int tmpCol = j*m_iNbAction + forest.GetFMap(i+1,j); //lastDecision.at(j);
				cout<<"Here..9-6-2++"<<j<<endl;
				for (int jj = 0; jj < nbBranch; jj++){
					cout<<"Here..9-6-3---"<<jj<<endl;
					inds.push_back((tmpCol)*nbBranch+jj);
					cout<<"Here..9-6-4---"<<jj<<endl;
					int tmp = forest.GetMap(i,j,jj);
					cout<<"Here..9-6-5---map "<<tmp<<endl;
					cout<<"Here..9-6-5---"<<jj<<endl;
					mapping.push_back(tmp);
					cout<<"Here..9-6-6---"<<jj<<endl;
				}
			}
			if(_IDID_DEBUG_)
							cout<<"Here..9-6-inds.size: "<<inds.size()<<endl;

			if(_IDID_DEBUG_)
							cout<<"Here..9-7"<<endl;
			//set mapping

			for (size_t j = 0; j<inds.size();j++){
				if(_IDID_DEBUG_){
							cout<<"Here..9-7-"<<j<<currNumofModel<<endl<<flush;
							//flush();
				}

				setModelMapColumn(tab,inds.at(j),mapping.at(j),currNumofModel);
			}

			if(_IDID_DEBUG_)
							cout<<"Here..9-8"<<endl;

			//decision node "OD"
			ODNodeArr[oAg] = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode[oAg].at(i));
			tab = ODNodeArr[oAg]->getTable();
			lastDecision = forest.GetStepAct(i+1);
			for (size_t j = 0; j<lastDecision.size();j++){
				setModelMapColumn(tab,j,lastDecision.at(j),m_iNbAction);
			}
			if(_IDID_OUTPUT_)
				cout<<"Step ["<<i+1<<"out of "<<steps<<"] has finished"<<endl;
		}
	}

		//we have added some states and updated the corresponding CPTs.
		//compile and propagate it, otherwise, the new CPTs are not accessible. from where?
		cout<<"I-DID has begun triangulating ...!"<<endl;

		m_pDomain->triangulate(H_TM_CLIQUE_SIZE);
			cout<<"I-DID has triangulated successfully!"<<endl;

		m_pDomain->compile();
		cout<<"I-DID has compiled successfully!"<<endl;
		m_pDomain->compress();
		cout<<"I-DID has compressed successfully!"<<endl;
		//m_pDomain->initialize();
		m_pDomain->propagate();
		cout<<"I-DID has propagated successfully!"<<endl;

		//inter media file
//		m_pDomain->saveAsNet(destFile);
		#define _IDID_DEBUG_  false
		#define _IDID_OUTPUT_  false


}

int CAdvDID::PickIndexofModel(int oAg)
{
//	cout<<"Inside PickIndexofModel "<<oAg<<endl;
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode[oAg].at(0));
	int index = pickNodeState(pNode);

	if (index == -1){
//		cout<<"Leaving PickIndexofModel "<<oAg<<" Index = "<<index<<endl;
		return -1;
	}
	else{
//		cout<<"Leaving PickIndexofModel "<<oAg<<" Index = "<<index<<endl;
		return index;
	}
}

float CAdvDID::Evaluate(vector<CBasDID*> bDIDArr){

	float totalUti = 0;
	size_t S_State, currState;
	vector<size_t> OD_state(m_iNbOthers);
	DiscreteNode *pSNode_a,*pNode;
	vector<DiscreteNode*> pSNode_b(m_iNbOthers);
	UtilityNode *pUNode;

	/*
	 * STEP 1a:	Initialize I-DID
	 */
	m_pDomain->initialize();
	if(_IDID_EVAL_OUTPUT_)
		cout<<"[0] STEP 1a:	Initialize I-DID"<<endl;

	/*
	 * STEP 1b:	Initialize the DIDs of others
	 * STEP 2: 	Find best action for others and enter it as evidence in the D node in their corresponding DIDs.
	 * 			Then, propagate the DIDs.
	 */
	for(int i=0;i<m_iNbOthers;i++){
		bDIDArr[i]->m_pDomain->initialize();
		//DID D
		pNode = (DiscreteNode*)getNodeByLabel(bDIDArr[i]->m_pDomain,bDIDArr[i]->m_arrDNode.at(0));
		OD_state[i] = getBestChoice(pNode);
		pNode->selectState(OD_state[i]);
		bDIDArr[i]->m_pDomain->propagate();
	}
	if(_IDID_EVAL_OUTPUT_){
		cout<<"[0] STEP 1b:	Initialize the DIDs of others"<<endl
			<<"[0] STEP 2: 	Find best action for others and enter it as evidence in the D node in their corresponding DIDs. Then, propagate the DIDs."<<endl;
	}

	/*
	 * STEP 3: 	Find best action for subject agent and enter it as evidence in the D node in its I-DID.
	 */
	//IDID D
	pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(0));
	currState = getBestChoice(pNode);
	pNode->selectState(currState);
	if(_IDID_EVAL_OUTPUT_)
		cout<<"[0] STEP 3: 	Find best action for subject agent and enter it as evidence in the D node in its I-DID."<<endl;

	/*
	 * STEP 4: 	Enter best actions of others found (in STEP 2) as evidence in the corresponding OD nodes
	 * 			in the subject agent's I-DID.
	 */
	//IDID OD
	for(int i=0;i<m_iNbOthers;i++){
		pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode[i].at(0));
		pNode->selectState(OD_state[i]);
	}
	if(_IDID_EVAL_OUTPUT_)
		cout<<"[0] STEP 4: 	Enter best actions of others found (in STEP 2) as evidence in the corresponding OD nodes in the subject agent's I-DID."<<endl;

	/*
	 * STEP 5: 	Sample a state from the subject agent's S node in the I-DID
	 * 			and enter it as evidence in the same (S node).
	 */
	//IDID S
	pSNode_a = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(0));
	S_State = pickNodeState(pSNode_a);
	pSNode_a->selectState(S_State);
	if(_IDID_EVAL_OUTPUT_)
		cout<<"[0] STEP 5: 	Sample a state from the subject agent's S node in the I-DID and enter it as evidence in the same (S node)."<<endl;

	/*
	 * STEP 6: 	Propagate the subject agent's I-DID and get expected utility from the U node.
	 */
	//IDID U
	pUNode = (UtilityNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(0));
	m_pDomain->propagate();
	if(_IDID_OUTPUT_){
		cout<<"\t\tThe utility of step [1 out of "<<m_iNbSteps<<"] is "<<pUNode->getExpectedUtility()<<endl<<endl;
	}
	totalUti = pUNode->getExpectedUtility()+totalUti;
	if(_IDID_EVAL_OUTPUT_)
		cout<<"[0] STEP 6: 	Propagate the subject agent's I-DID and get expected utility from the U node."<<endl;

	for (int step = 1; step < m_iNbSteps; ++step)
	{
		/*
		 * STEP 7: 	Sample a state from the subject agent's S node in the I-DID
		 * 			and enter it as evidence in the same (S node).
		 * 			Then, propagate the I-DID.
		 */
		//IDID S
		pSNode_a= (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(step));
		S_State = pickNodeState(pSNode_a);
		pSNode_a->selectState(S_State);
//		if(pSNode_a->isEvidenceEntered()){
//			cout<<"IDID S Node: "<<pSNode_a->getLabel()<<" state selected: "<<S_State<<endl;
//		}
		m_pDomain->propagate();
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 7:	Sample a state from the subject agent's S node in the I-DID and enter it as evidence in the same (S node). Then, propagate the I-DID."<<endl;

		/*
		 * STEP 8: 	Enter the state sampled from the subject agent's S node in the I-DID (in STEP 7)
		 * 			and enter it as evidence in the S node of the other agents' corresponding DIDs.
		 * 			Then, propagate the DIDs.
		 */
		//DID S
		for(int i=0;i<m_iNbOthers;i++){
			pSNode_b[i] = (DiscreteNode*)getNodeByLabel(bDIDArr[i]->m_pDomain,bDIDArr[i]->m_arrSNode.at(step));
			pSNode_b[i]->selectState(S_State);
//			if(pSNode_b->isEvidenceEntered()){
//				cout<<"DID S Node: "<<pSNode_b->getLabel()<<" state selected: "<<S_State<<endl;
//			}
			bDIDArr[i]->m_pDomain->propagate();
		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 8:	Enter the state sampled from the subject agent's S node in the I-DID (in STEP 7) and enter it as evidence in the S node of the other agents' corresponding DIDs. Then, propagate the DIDs."<<endl;

		/*
		 * STEP 9:	Sample an observation from the subject agent's O node in the I-DID
		 * 			and enter it as evidence in the same (O node).
		 */
		//IDID O
		pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(step));
		currState = pickNodeState(pNode);
		pNode->selectState(currState);
//		if(pNode->isEvidenceEntered()){
//			cout<<"IDID O Node: "<<pNode->getLabel()<<" state selected: "<<currState<<endl;
//		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 9:	Sample an observation from the subject agent's O node in the I-DID and enter it as evidence in the same (O node)."<<endl;

		/*
		 * Step 10: Sample an observation from each of the other agent's O node in its
		 * 			corresponding DID and enter it as evidence in the same (O node).
		 */
		//DID O
		for(int i=0;i<m_iNbOthers;i++){
			pNode = (DiscreteNode*)getNodeByLabel(bDIDArr[i]->m_pDomain,bDIDArr[i]->m_arrONode.at(step));
			currState = pickNodeState(pNode);
			pNode->selectState(currState);
//			if(pNode->isEvidenceEntered()){
//				cout<<"DID O Node: "<<pNode->getLabel()<<" state selected: "<<currState<<endl;
//			}
		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 10: Sample an observation from each of the other agent's O node in its corresponding DID and enter it as evidence in the same (O node)."<<endl;

		/*
		 * STEP 11: Retract all findings from the S node of the subject agent's I-DID
		 */
		pSNode_a->retractFindings();
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 11: Retract all findings from the S node of the subject agent's I-DID."<<endl;

		/*
		 * STEP 12:	Retract all findings from the corresponding S nodes of the other agents' DIDs.
		 * 			Then, propagate the DIDs.
		 */
		for(int i=0;i<m_iNbOthers;i++){
			pSNode_b[i]->retractFindings();
			bDIDArr[i]->m_pDomain->propagate();
		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 12: Retract all findings from the corresponding S nodes of the other agents' DIDs. Then, propagate the DIDs."<<endl;

		/*
		 * STEP 13: Propagate the subject agent's I-DID
		 */
		m_pDomain->propagate();
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 13: Propagate the subject agent's I-DID."<<endl;

		/*
		 * STEP 14: Find best action for others and enter it as evidence in the D node
		 * 			in their corresponding DIDs.
		 */
		//DID D
		for(int i=0;i<m_iNbOthers;i++){
			pNode = (DiscreteNode*)getNodeByLabel(bDIDArr[i]->m_pDomain,bDIDArr[i]->m_arrDNode.at(step));
			OD_state[i] = getBestChoice(pNode);
			pNode->selectState(OD_state[i]);
//			if(pNode->isEvidenceEntered()){
//				cout<<"DID D Node: "<<pNode->getLabel()<<" state selected: "<<OD_state[i]<<endl;
//			}
		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 14: Find best action for others and enter it as evidence in the D node in their corresponding DIDs."<<endl;

		/*
		 * STEP 15:	Find best action for subject agent and enter it as evidence in the D node in its I-DID.
		 */
		//IDID D
		pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(step));
		currState = getBestChoice(pNode);
		pNode->selectState(currState);
//		if(pNode->isEvidenceEntered()){
//			cout<<"IDID D Node: "<<pNode->getLabel()<<" state selected: "<<currState<<endl;
//		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 15: Find best action for subject agent and enter it as evidence in the D node in its I-DID."<<endl;

		/*
		 * STEP 16:	Enter best actions of others found (in STEP 14) as evidence in the corresponding OD nodes
		 * 			in the subject agent's I-DID.
		 */
		//IDID OD
		for(int i=0;i<m_iNbOthers;i++){
			pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode[i].at(step));
//			OD_state[i]=getBestChoice(pNode);
			pNode->selectState(OD_state[i]);
//			if(pNode->isEvidenceEntered()){
//				cout<<"IDID OD Node: "<<pNode->getLabel()<<" state selected: "<<OD_state[i]<<endl;
//			}
		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 16:	Enter best actions of others found (in STEP 14) as evidence in the corresponding OD nodes in the subject agent's I-DID."<<endl;

		/*
		 * STEP 17:	Enter the state sampled from the subject agent's S node in the I-DID (in STEP 7)
		 * 			and enter it as evidence in the same (S node).
		 */
		pSNode_a->selectState(S_State);
//		if(pSNode_a->isEvidenceEntered()){
//			cout<<"IDID S Node: "<<pSNode_a->getLabel()<<" state selected: "<<S_State<<endl;
//		}
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 17: Enter the state sampled from the subject agent's S node in the I-DID (in STEP 7) and enter it as evidence in the same (S node)."<<endl;

		/*
		 * STEP 18:	Propagate the subject agent's I-DID and get expected utility from the U node.
		 */
		//IDID U
		pUNode = (UtilityNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(step));
		m_pDomain->propagate();
		totalUti = pUNode->getExpectedUtility() + totalUti;
		if(_IDID_EVAL_OUTPUT_)
			cout<<"["<<step<<"] STEP 18: Propagate the subject agent's I-DID and get expected utility from the U node."<<endl;
		if(_IDID_OUTPUT_)
			cout<<"\t\tThe utility of step ["<<step+1<<" out of "<<m_iNbSteps<<"] is "<<pUNode->getExpectedUtility()<<endl<<endl;
	}

	stringstream out;
	out << "Debug/Output_IDID/OutIDID_"<<m_iNbSteps<<"Hor_"<<m_iNbOthers+1 <<"Agents.net";

//	m_pDomain->saveAsNet(out.str());

	return totalUti;
}

//int CAdvDID::PickIndexofModel()
//{
//	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(0));
//	int index = pickNodeState(pNode);
//
//	if (index == -1)
//		return -1;
//	else
//		return index;
//}

//void CAdvDID::ExpandModelwithForest(CForest& forest, string destFile)
//{
//	//how many steps
//	int steps = forest.GetNbofLayer();
//
//	DiscreteNode* ModelNode = NULL;
//	DiscreteNode* ODNode = NULL;
//	DiscreteNode* pTmpNode = NULL;
//	Table *tab;
//	NodeList ndList;
//	int currNumofModel,nbBranch;
//
//	vector<int> currentDicision;
//	vector<int> lastDecision;
//	m_pDomain->uncompile();
//
//	nbBranch = forest.GetNbofBranch();
//
//	//the first step is special, no model mapping..
//	ModelNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(0));
//	ODNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(0));
//	currNumofModel = forest.GetLayerSize(1);  //---
//	setNumberOfStatesEx(ModelNode,currNumofModel);
//	//set model weight;
//	vector<int> weight;
//	forest.CalWeight(weight);
//	tab = ModelNode->getTable();
//	if (tab->getNodes().size() > 1)   //there is a link between S[n] and Mod[n]
//	{
//		pTmpNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(0));
//		for (size_t i = 0; i < pTmpNode->getNumberOfStates(); i++)
//		{
//			setTableColumn(tab,i,weight,currNumofModel);
//		}
//	}
//	else  //
//	{
//		setTableColumn(tab,0,weight,currNumofModel);
//	}
//
//
//	currentDicision = forest.GetStepAct(1);
//	tab = ODNode->getTable();
//	//only one column, of course, set the first column,...
//	for (size_t i = 0; i < currentDicision.size(); i++)
//	{
//		setModelMapColumn(tab,i,currentDicision.at(i),m_iNbAction);
//	}
//	if(_IDID_OUTPUT_)
//		cout<<"the[1//"<<steps<<"]th step has finished\n";
//	//m_pDomain->saveAsNet("c:\\nn1.net");
//	size_t cntTop;
//	DiscreteNode* topNode;
//	vector<int> inds,mapping;
//	for (int i = 1; i < steps; i++)  //second step  and so on.
//	{
//		//Mod[i], model node
//		ModelNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(i));
//		currNumofModel = forest.GetLayerSize(i+1);	//--
//		//how many states does this node should have.
//		setNumberOfStatesEx(ModelNode,currNumofModel);
//
//		//this the (i)th element, the (i+1)th layer.
//		lastDecision = forest.GetStepAct(i);
//
//		tab = ModelNode->getTable();
//		//getTableSize(tab,nbRow,nbCol);
//		ndList = tab->getNodes();
//		topNode = (DiscreteNode*)(ndList.at(0));
//		cntTop = (topNode->getNumberOfStates());
//		inds.clear();
//		mapping.clear();
//		vector<int> l_mapping;
//		for (int j = 0; j < cntTop; j++)
//		{
//			int tmpCol = j*m_iNbAction + lastDecision.at(j);
//			for (int jj = 0; jj < nbBranch; jj++)
//			{
//				inds.push_back((tmpCol)*nbBranch+jj);
//				int tmp = forest.GetMap(i,j,jj);
//				mapping.push_back(tmp);
//			}
//		}
//
//		//set mapping
//		for (size_t j = 0; j<inds.size();j++)
//		{
//			setModelMapColumn(tab,inds.at(j),mapping.at(j),currNumofModel);
//		}
//
//		//decision node "OD"
//		ODNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(i));
//		tab = ODNode->getTable();
//		lastDecision = forest.GetStepAct(i+1);
//		for (size_t j = 0; j<lastDecision.size();j++)
//		{
//			setModelMapColumn(tab,j,lastDecision.at(j),m_iNbAction);
//		}
//		if(_IDID_OUTPUT_)
//			cout<<"the["<<i+1<<"//"<<steps<<"]th step has finished\n";
//		//m_pDomain->saveAsNet("c:\\nn2.net");
//	}
//
//	//we have added some states and updated the corresponding CPTs.
//	//compile and propagate it, otherwise, the new CPTs are not accessible. from where?
//
//	m_pDomain->compile();
//	//m_pDomain->initialize();
//	m_pDomain->propagate();
//
//	//inter media file
//	m_pDomain->saveAsNet(destFile);
//}

//vector<float>& CAdvDID::GetNewProbDistri( vector<int> indice, vector<float> weights,
//						  DiscreteNode* pMNode,vector<float>& res,CGraphForest& forest)
//{
//	vector<float> subMat;
//
//	for (size_t i = 0; i < indice.size(); i++)
//	{
//		GetSubMatrix(indice.at(i),pMNode,subMat,forest);
//		vec_PMultiply(subMat,weights.at(i));
//		vec_PAdd(res,subMat);
//	}
//	if (vec_Sum(weights))
//	{
//		vec_PMultiply(res,1.0F/vec_Sum(weights));
//	}
//	return res;
//}

//void CAdvDID::GetSubMatrix(int index,DiscreteNode* pMNode, vector<float>& subMat,CGraphForest& forest)
//{
//	Table* pTab;
//	pTab = pMNode->getTable();
//
//	int start;
//	size_t sz;
//	//The size of sub-matrix
//	sz = forest.GetNbofAction()*forest.GetNbofBranch()*pMNode->getNumberOfStates();
//
//	if (index >= 0)
//		start = index*sz;
//	else
//		return;
//
//	subMat.resize(sz);
//	NumberList list;
//	list = pTab->getData();
//	vector<double> dsubMat(subMat.begin(), subMat.end());
//	pTab->getData(dsubMat,start,sz);
//
//	return;
//}

//float CAdvDID::Evaluate(CBasDID& bDID)
//{
//	size_t OD_state,S_State, currState;
//	float *pFlt=NULL, totalUti = 0;
//	//int nbState;
//	DiscreteNode *pSNode_a,*pSNode_b,*pNode;  //s1 for
//	UtilityNode *pUNode;
//
//	m_pDomain->initialize();
//	bDID.m_pDomain->initialize();
//	//DID D
//	pNode = (DiscreteNode*)getNodeByLabel(bDID.m_pDomain,bDID.m_arrDNode.at(0));
//	OD_state = getBestChoice(pNode);
//	pNode->selectState(OD_state);
//	bDID.m_pDomain->propagate();
//	//IDID D
//	pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(0));
//	currState = getBestChoice(pNode);
//	pNode->selectState(currState);
//	//IDID OD
//	pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(0));
//	pNode->selectState(OD_state);
//	//IDID S
//	pSNode_a = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(0));
//	S_State = pickNodeState(pSNode_a);
//	pSNode_a->selectState(S_State);
//	//IDID U
//	pUNode = (UtilityNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(0));
//
//	m_pDomain->propagate();
//
//	if(_IDID_OUTPUT_)
//		cout<<"\t\tThe utility of the 1th step is "<<pUNode->getExpectedUtility()<<"\n\n";
//	totalUti = pUNode->getExpectedUtility()+totalUti;
//
//	for (int i = 1; i < m_iNbSteps; ++i)
//	{
//		//IDID S
//		pSNode_a= (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(i));
//		S_State = pickNodeState(pSNode_a);
//		pSNode_a->selectState(S_State);
//		m_pDomain->propagate();
//		//DID S
//		pSNode_b = (DiscreteNode*)getNodeByLabel(bDID.m_pDomain,bDID.m_arrSNode.at(i));
//		pSNode_b->selectState(S_State);
//		bDID.m_pDomain->propagate();
//		//IDID O
//		pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(i));
//		currState = pickNodeState(pNode);
//		pNode->selectState(currState);
//		//DID O
//		pNode = (DiscreteNode*)getNodeByLabel(bDID.m_pDomain,bDID.m_arrONode.at(i));
//		currState = pickNodeState(pNode);
//		pNode->selectState(currState);
//		pSNode_a->retractFindings();
//		pSNode_b->retractFindings();
//		bDID.m_pDomain->propagate();
//		m_pDomain->propagate();
//		//DID D
//		pNode = (DiscreteNode*)getNodeByLabel(bDID.m_pDomain,bDID.m_arrDNode.at(i));
//		OD_state = getBestChoice(pNode);
//		pNode->selectState(OD_state);
//		//IDID D
//		pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(i));
//		currState = getBestChoice(pNode);
//		pNode->selectState(currState);
//		//IDID OD
//		pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(i));
//		pNode->selectState(OD_state);
//		pSNode_a->selectState(S_State);
//		//IDID U
//		pUNode = (UtilityNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(i));
//
//		m_pDomain->propagate();
//
//
//		totalUti = pUNode->getExpectedUtility() + totalUti;
//		if(_IDID_OUTPUT_)
//			cout<<"\t\tThe utility of the "<<i+1<<"th step is "<<pUNode->getExpectedUtility()<<"\n\n";
//	}
//
//
//
//	return totalUti;
//}



//void CAdvDID::ExpandModelwithForest(CForest& forest, string destFile)
//{
//	//how many steps
//	int steps = forest.GetNbofLayer();
//
//	DiscreteNode* ModelNode = NULL;
//	DiscreteNode* ODNode = NULL;
//	DiscreteNode* pTmpNode = NULL;
//	Table *tab;
//	NodeList ndList;
//	int currNumofModel,nbBranch;
//
//	vector<int> currentDicision;
//	vector<int> lastDecision;
//	m_pDomain->uncompile();
//
//	nbBranch = forest.GetNbofBranch();
//
//	//the first step is special, no model mapping..
//	ModelNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(0));
//	ODNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(0));
//	currNumofModel = forest.GetLayerSize(1);  //---
//	setNumberOfStatesEx(ModelNode,currNumofModel);
//	//set model weight;
//	vector<int> weight;
//	forest.CalWeight(weight);
//	tab = ModelNode->getTable();
//	if (tab->getNodes().size() > 1)   //there is a link between S[n] and Mod[n]
//	{
//		pTmpNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(0));
//		for (size_t i = 0; i < pTmpNode->getNumberOfStates(); i++)
//		{
//			setTableColumn(tab,i,weight,currNumofModel);
//		}
//	}
//	else  //
//	{
//		setTableColumn(tab,0,weight,currNumofModel);
//	}
//
//
//	currentDicision = forest.GetStepAct(1);
//	tab = ODNode->getTable();
//	//only one column, of course, set the first column,...
//	for (size_t i = 0; i < currentDicision.size(); i++)
//	{
//		setModelMapColumn(tab,i,currentDicision.at(i),m_iNbAction);
//	}
//	if(_IDID_OUTPUT_)
//		cout<<"the[1//"<<steps<<"]th step has finished\n";
//	//m_pDomain->saveAsNet("c:\\nn1.net");
//	size_t cntTop;
//	DiscreteNode* topNode;
//	vector<int> inds,mapping;
//	for (int i = 1; i < steps; i++)  //second step  and so on.
//	{
//		//Mod[i], model node
//		ModelNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(i));
//		currNumofModel = forest.GetLayerSize(i+1);	//--
//		//how many states does this node should have.
//		setNumberOfStatesEx(ModelNode,currNumofModel);
//
//		//this the (i)th element, the (i+1)th layer.
//		lastDecision = forest.GetStepAct(i);
//
//		tab = ModelNode->getTable();
//		//getTableSize(tab,nbRow,nbCol);
//		ndList = tab->getNodes();
//		topNode = (DiscreteNode*)(ndList.at(0));
//		cntTop = (topNode->getNumberOfStates());
//		inds.clear();
//		mapping.clear();
//		vector<int> l_mapping;
//		for (int j = 0; j < cntTop; j++)
//		{
//			int tmpCol = j*m_iNbAction + lastDecision.at(j);
//			for (int jj = 0; jj < nbBranch; jj++)
//			{
//				inds.push_back((tmpCol)*nbBranch+jj);
//				int tmp = forest.GetMap(i,j,jj);
//				mapping.push_back(tmp);
//			}
//		}
//
//		//set mapping
//		for (size_t j = 0; j<inds.size();j++)
//		{
//			setModelMapColumn(tab,inds.at(j),mapping.at(j),currNumofModel);
//		}
//
//		//decision node "OD"
//		ODNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(i));
//		tab = ODNode->getTable();
//		lastDecision = forest.GetStepAct(i+1);
//		for (size_t j = 0; j<lastDecision.size();j++)
//		{
//			setModelMapColumn(tab,j,lastDecision.at(j),m_iNbAction);
//		}
//		if(_IDID_OUTPUT_)
//			cout<<"the["<<i+1<<"//"<<steps<<"]th step has finished\n";
//		//m_pDomain->saveAsNet("c:\\nn2.net");
//	}
//
//	//we have added some states and updated the corresponding CPTs.
//	//compile and propagate it, otherwise, the new CPTs are not accessible. from where?
//
//	m_pDomain->compile();
//	//m_pDomain->initialize();
//	m_pDomain->propagate();
//
//	//inter media file
//	m_pDomain->saveAsNet(destFile);
//}


//void CAdvDID::AECompress( CGraphForest& forest, string netFile )
//{
//	m_pDomain->uncompile();
//	for (int i = 1; i <= forest.GetNbofLayer()-1; i++)
//	{
//		AECompress_Step(forest, i);
//		m_pDomain->saveAsNet(netFile);
//	}
//
//	m_pDomain->saveAsNet(netFile);
//	m_pDomain->compile();
//}

//the following slices are different from the first one.
//P(M3|S3)   ->  p(M2|M3,OD3,OO2)
//P(M2|M3,OD3,OO2) -> P(M1|M2,OD2,OO1)
//void CAdvDID::AECompress_Step(CGraphForest& forest, int iStep)
//{
//	vector<int> arrAct;
//	vector<int> indice;
//	vector<float> weight,res;
//	//int* pArrState;
//	DiscreteNode *pM2Node, *pM1Node;
//	//Table* pTab;
//
//	pM2Node = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(iStep-1));	//Mod2
//	pM1Node = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrModNode.at(iStep));		//Mod1
//
//	arrAct = forest.GetStepAct(iStep);
//
//	while (AEIndex(arrAct,indice))
//	{
//		if (iStep == 1)  //if this is the first step, we need to update the weight array.
//		{
//			vector<int> iarr;
//			iarr.resize(forest.GetWeight().size());    //redundant
//			unique_copy(forest.GetWeight().begin(),forest.GetWeight().end(),iarr.begin());
//			//vec_SubVector(iarr,indice);
//			vec_WeightUpdate(forest.GetWeight(),vec_SubVector(iarr,indice));
//		}
//		res.clear();
//		//p(m(i)|...)
//		//ok. start compressing....
//		//e.g.   step1: get-
//		//					1. p(m3|s3)
//		//					2. P(M2|M3=1*,OD3,OO2)
//		//		set -
//		//			p(m3=1*|s3)
//		NumberList nbList_Mi = pM2Node->getTable()->getData();
//		int nbCol,nbRow;
//		getTableSize(pM2Node->getTable(),nbRow,nbCol);
//		for (int i = 0; i < nbCol; i++)
//		{
//			getTableColumn(pM2Node->getTable(),i,weight,nbRow);
//			vec_SubVector(weight,indice);
//
//			//P(M2|M3,OD3,OO2)
//			//for each indice[k], we can get a sub-matrix for
//			//P(M2=*|M3 = indice[k],OD3=*,OO2=*),as a vector,
//			//pMNode = Mod2;  if there are 3 steps
//			vector<float> tmp;
//			GetNewProbDistri(indice,weight,pM1Node,tmp,forest);
//			vec_PAdd(res,vec_PMultiply(tmp,1.0F/nbCol));
//			tmp.clear();
//
//			//update p(m3=1*|s3)
//			//pMNode = Mod3;  if there are 3 steps
//			float r;
//			r = vec_Sum(weight);
//			int index = i*nbRow + indice.at(0);
//			nbList_Mi.at(index) = r;
//			//set the others to be -1, since -1 can't be in the CPT
//			for (size_t j = 1; j < indice.size();j++)
//			{
//				//		         col                            row
//				nbList_Mi.at(i*nbRow + indice.at(j)) = -1;
//			}//j
//		}//i
//
//		//p(m(i-1)|m(i),..,..)
//		//for m[i-1]
//		//step2, update- and remove states
//		//the table of M2 will be changed when we remove the state of M3
//		//so, get m2's table firstly
//		NumberList nbList_Mii = pM1Node->getTable()->getData();
//
//		//MOD	I
//		pM2Node->setNumberOfStates(pM2Node->getNumberOfStates() - indice.size() + 1);
//		NumberList tmpList; //for
//		for (NumberList::iterator iter = nbList_Mi.begin(); iter != nbList_Mi.end(); iter++)
//		{
//			if (*iter != -1)
//				tmpList.push_back(*iter);
//		}
//		pM2Node->getTable()->setData(tmpList);
//		nbList_Mi.clear();
//
//		//ODi
//		DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrODNode.at(iStep-1));
//		for (size_t i = 0; i < arrAct.size(); i++)
//		{
//			setModelMapColumn(pNode->getTable(),i,arrAct.at(i),forest.GetNbofAction());
//		}
//
//		//Mod i-1
//		int sz = forest.GetNbofAction()*forest.GetNbofBranch()*pM1Node->getNumberOfStates();
//		for (int i = static_cast<int>(indice.size()-1); i >= 0; i--)
//		{
//			nbList_Mii.erase(nbList_Mii.begin() + indice.at(i)*sz,
//				nbList_Mii.begin() + (indice.at(i)+1)*sz);
//		}//i
//		nbList_Mii.insert(nbList_Mii.begin()+indice.at(0)*sz,res.begin(),res.end());
//		NumberList nblistt = pM1Node->getTable()->getData();      //for debug,
//		pM1Node->getTable()->setData(nbList_Mii);
//		nbList_Mii.clear();
//
//	}//while
//}
