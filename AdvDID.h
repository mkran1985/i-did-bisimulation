#pragma once

#include "BasDID.h"
#include "GraphForest.h"

//the class of so called advanced DID, which is the model of Level-1 agent
//of course, it will take advantage of the level-0 agent.
//The level-1 agent has several additional nodes: OD* OO* Mod*, which will be defined and manipulated following...
//Most importantly, in order to initialize the CPTs of the new nodes, the AdvDID will need the result[policy tree(s)] derived from BasDID.
//We call this procedure [expand the model with tree/forest]
class CAdvDID :public CBasDID
{
	//property:
private:
//	vector<string> m_arrODNode;
//	vector<string> m_arrOONode;
//	vector<string> m_arrModNode;

	vector<vector<string> > m_arrODNode;
	vector<vector<string> > m_arrOONode;
	vector<vector<string> > m_arrModNode;

	//public member function
public:
	int m_iNbOthers;			//number of others
	//default construct
	CAdvDID(void);
	//with a given .net file
	CAdvDID(string netFile);
	//with a given .net file
	CAdvDID(string netFile, int m_iNbOthers);
	//with a given .net file and a data file which containing the parameters and CPTs.
	CAdvDID(string netFile, string paraFile);

	~CAdvDID(void);

	//Given the solutions of several level-0 agents, whose policy trees are in the forest
	//we need to initialize the Mod* OO* and OD* nodes with them.
	void ExpandModelwithForest(CForest& forest, string destFile);
	void ExpandModelwithForest(vector<CForest *> forestArr, string destFile);
	//void ExpandModelwithGForest(CGraphForest& forest, string destFile);

	//of course, we need to solve the level-1 agent, but the 
	//solver of level-0 agent still works well for level-1.
	//void SolveAdvDID()/(file); 

	//compress the advanced did by Action Equivalence
	void AECompress(CGraphForest& forest, string netFile);

	float Evaluate(CBasDID& bDID);
	float Evaluate(vector<CBasDID*> bDIDArr);

	//pick one of candidate model level-0 .
	int PickIndexofModel();
	int PickIndexofModel(int otherAgentIndex);

private:
	//return = sigma(P(M2|M3=i,OD3,OO2)*P(M3=i|S3)) for every M2,OD3,OO3
	//the sub-matrix for m3 = 1* "[1+2=1*]"
	//this solution depends on the order of M3,OD3,OO2,
	//i think this order is well designed.
	vector<float>& GetNewProbDistri(vector<int> indice, vector<float> weights, 
		DiscreteNode* pMNode, vector<float>& res,CGraphForest& forest);

	//get P(M2=*|M3 = index,OD3=*,OO2=*)
	void GetSubMatrix(int index,DiscreteNode* pMNode,vector<float>& subMat,CGraphForest& forest);

	//when we use AE-criterion to compress the graph, 
	//we need to do it step by step.
	void AECompress_Step(CGraphForest& forest,int iStep);

	

};
