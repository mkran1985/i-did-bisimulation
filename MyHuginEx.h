#pragma once
#include "stdafx.h"
#include "IDID.h"

//The operation related to Hugin API, so I call this file is my Hugin extension.
//no object here, only several functions.

//Queries about the nodes
//get the Utilities of a discrete node.
//result is allocate OUT of this function
float* getNodeUtilities(Domain* domain, string label, float** result,int& nbState);
float* getNodeUtilities(DiscreteNode* pNode, float** result, int& nbState);

//get believes of a discrete node
//result will be allocated IN this function
float* getNodeBelief(Domain *domain, string label,float** result, int& nbState);
float* getNodeBelief(Domain *domain, string label,float** result);
float* getNodeBelief(DiscreteNode* pNode,float** result, int& nbState);

//label != name
Node* getNodeByLabel(Domain* pDomain, string label);
//return the index of the state whose utility is the biggest.
// return the number of the observation state
int getNbObservation(Domain * pDomain);

size_t getBestChoice(Domain* pDomain, string label);
size_t getBestChoice(DiscreteNode* pNode);

void setNodeState(Domain* pDomain, string label, int state);
//pick a state of the given node randomly.
int pickNodeState(DiscreteNode* pNode);
//void InitilizeEviandAnc(int* arrEvi, int* arrAnc, int* arrObs, int len, int nbEvid,int nbStep);

//pNode will have n states,and the labels of states will be 0 -- (n-1). 
//setNumberOfStates is a member of Node in Hugin,
//which doesn't initialize the labels.
void setNumberOfStatesEx(DiscreteNode* pNode,int n);

//**********************Table operation... 
//the table here looks like a 2-dimension array, but in fact, it is a 1-D array, one column follows another column...
//so, we can assign the value to a column of an table.
Number getTableItem(const Table* table, int row,int col,int nbRow = -1);

void setTableItem(Table* table,int row,int col,Number val,int nbRow = -1);
void getTableSize(const Table* table, int& nbRow, int& nbCol);

//set the specific column of the table with identical val.
void setTableColumn(Table* table,int colIndex,Number val, int nbRow = -1);
//set the specific column of the table with values.
void setTableColumn(Table* table,int colIndex,vector<int> valarr, int nbRow = -1);
//get....
vector<float>& getTableColumn(Table* table,int colIndex,vector<float>& val, int nbRow = -1);
//**********************Table operation... 



//set the specific item in this column 1, and others 0. 
//with the given nbRow, we don't need to calculate it from the Table
void setModelMapColumn(Table* table,int col,int row,int nbRow = -1);

//return the number of decision node, how many steps means how many decision nodes.
int getNbDecisionNode(Domain* pDomain);

//return the number of utility nodes, how many steps means how many decision nodes (this equals number of utility nodes in most cases).
int getNbUtilityNode(Domain* pDomain);

