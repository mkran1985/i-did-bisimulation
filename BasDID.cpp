#include "stdafx.h"
#include "BasDID.h"
#include "Accessory.h"
#include "MyHuginEx.h"

typedef pair<CBasDID*,vector<float> > beliefPoint;


CBasDID::CBasDID(void)
{
}

CBasDID::CBasDID(string netFile, string txtFile)
{
	FILE* pf=fopen(netFile.c_str(),"r");
	if (!pf){
		cout<<"where is the net file? \n";
		return;
	}

	DefaultParseListener pl;
	m_pDomain = new Domain(netFile, &pl);
	m_pDomain->triangulate (H_TM_FILL_IN_WEIGHT);
	m_pDomain->compile();

	m_of.open("c:\\log.txt");

	m_pFile=fopen(txtFile.c_str(),"r+");
	if (!m_pFile){
		cout<<"Error:[CBasDID]   ---- where is the text file? \n";
		return;
	}

	m_iNbSteps = getNbDecisionNode(m_pDomain);
}

CBasDID::CBasDID(string netFile)
{
	if(_IDID_DEBUG_)
		cout<<"Initializing DID (inside CBasDID constructor)"<<endl;

	FILE* pf=fopen(netFile.c_str(),"r");
	if(_IDID_DEBUG_)
		cout<<"Here..1"<<endl;

	if (!pf){
		cout<<"Error:[CBasDID]   ---- where is the net file? \n";
		return;
	}

	if(_IDID_DEBUG_)
		cout<<"Here..2"<<endl;

	DefaultParseListener pl;
	m_pDomain = new Domain(netFile, &pl);
	m_pDomain->triangulate (H_TM_FILL_IN_WEIGHT);
	m_pDomain->compile();

	m_iNbSteps = getNbDecisionNode(m_pDomain);
	if(_IDID_DEBUG_)
		cout<<"Here..2a"<<m_iNbSteps<<endl;

	m_pFile = NULL;

	m_of.open("c:\\log.txt");

	InitNodeLabels(m_arrDNode,"D",m_iNbSteps);
	InitNodeLabels(m_arrSNode,"S",m_iNbSteps);
	InitNodeLabels(m_arrUNode,"U",m_iNbSteps);
	InitNodeLabels(m_arrONode,"O",m_iNbSteps);

	if(_IDID_DEBUG_)
		cout<<"Here..3"<<m_iNbSteps<<endl;
	GetBasDIDPara();

	if(_IDID_DEBUG_)
		cout<<"Here..4"<<endl;
}
CBasDID::~CBasDID(void)
{
	m_arrSNode.clear();		
	m_arrDNode.clear();		
	m_arrUNode.clear();		
	m_arrONode.clear();		

	delete m_pDomain;
	m_pDomain = NULL;
	if (m_pFile)
	{
		fclose(m_pFile);
		m_pFile = NULL;
	}
	m_of.close();	
}	

vector<float> CBasDID::GetUpdatedBelief(vector<float> initialBelief, int action, int obs){
	vector<float> nextBelief(initialBelief.size());

	DiscreteNode* SNode;

	ChangePhysicalBelief(initialBelief);

	//enter obs as evidence
	setNodeState(m_pDomain,m_arrONode.at(0),obs);

	//enter decision as evidence
	setNodeState(m_pDomain,m_arrDNode.at(0),action);

	m_pDomain->propagate();

//	DiscreteNode* SNode;
	SNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(1));
	float* pDist2 = NULL;
	int nbState2;
	getNodeBelief(SNode,&pDist2,nbState2);

//	cout<<"Updated belief: ";
	for(int i=0;i<nextBelief.size();i++){
		nextBelief[i] = pDist2[i];
//		cout<<i<<" = "<<nextBelief[i]<<",";
	}
//	cout<<endl;

	return nextBelief;
}

double CBasDID::GetObservationProbability(int nextState, int action, int obs){
	double obsProb = 0.;
	DiscreteNode* ONode;
	NumberList nbList;
	ONode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(1));
	nbList = ONode->getTable()->getData();

//	for(int i=0;i<nbList.size();i++){
//		cout<<nbList[i]<<endl;
//	}
	int obsIndex = nextState*(m_iNbAction*m_iNbObs) + action*m_iNbObs + obs;
	obsProb = nbList[obsIndex];
//	cout<<"s'="<<nextState<<" a="<<action<<" o="<<obs<<" O(s',a,o)="<<obsProb<<" NBListSize="<<nbList.size()<<endl;;
	return obsProb;
}

double CBasDID::GetTransitionProbability(int state, int action, int nextState){
	double transProb = 0.;
	DiscreteNode* SNode;
	NumberList nbList;
	SNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(1));
	nbList = SNode->getTable()->getData();

//	for(int i=0;i<nbList.size();i++){
//		cout<<nbList[i]<<endl;
//	}
	int nsIndex = state*(m_iNbAction*m_iNbState) + action*m_iNbState + nextState;
	transProb = nbList[nsIndex];
//	cout<<"s="<<state<<" a="<<action<<" s'="<<nextState<<" T(s,a,s')="<<transProb<<" NBListSize="<<nbList.size()<<endl;

	return transProb;
}

double CBasDID::GetBeliefTransitionProbability(vector<float> initialBelief, int action, vector<float> nextBelief){
	double bTransProb = 0.;

	vector<vector<float> > updatedBeliefVec(m_iNbObs);

	for(int i=0;i<m_iNbObs;i++){
		updatedBeliefVec[i] = GetUpdatedBelief(initialBelief,action,i);
	}

	for(int o=0;o<m_iNbObs;o++){
		double pr_nb_a_b_o = 0;
		if(updatedBeliefVec[o] == nextBelief){
			pr_nb_a_b_o = 1.;
		}

		double sum_ns = 0.;
		for(int ns=0;ns<m_iNbState;ns++){
			double sum_s = 0.;
			for(int s=0;s<m_iNbState;s++){
				sum_s += GetTransitionProbability(s,action,ns) * initialBelief[s];
			}
			sum_ns += GetObservationProbability(ns,action,o) * sum_s;
		}
		bTransProb +=  pr_nb_a_b_o * sum_ns;
	}

	return bTransProb;
}


void CBasDID::SetLastSlice()
{
	//uniform.....
	DiscreteNode* DNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(0));
	NumberList nbList = DNode->getTable()->getData();
	size_t len = nbList.size();
	nbList.clear();
	nbList.insert(nbList.begin(),len,1);
	DNode->getTable()->setData(nbList);
}




//given a DID , initialize it with a file which specify the parameters and CPTs, 
void CBasDID::SetSecondLastSlice2()
{
	//one slice
	//load CPTs
	char buffer[BUFSIZ];
	int currAction,currAction2,startState,endState;
	while (fscanf(m_pFile, "%s", buffer) != EOF)
	{
		DiscreteNode* disNode;
		NumberList nbList;
		int tmpIndex;
		float tmpProb;

		switch (buffer[0])
		{
		case '#':
			fgets(buffer,BUFSIZ,m_pFile);
			break;
		case 'O':			
			fscanf(m_pFile,"%i",&currAction);
			fscanf(m_pFile,"%s",buffer);
			if (buffer[0] == '*')
			{//
				startState = -1;
				endState = -1; 
			}
			else
			{			}

			disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(1));
			nbList = disNode->getTable()->getData();

			for (int i = 0; i < m_iNbState; i++)
			{
				//the index is not right.....
				tmpIndex = i*(m_iNbAction*m_iNbObs) + currAction*m_iNbObs;
				for (int j = 0; j < m_iNbObs; j++)
				{
					fscanf(m_pFile,"%f",&tmpProb);
					nbList.at(tmpIndex + j) = tmpProb;
				}
			}
			disNode->getTable()->setData(nbList);
			break;

		case 'T':
			fscanf(m_pFile,"%i",&currAction);
			fscanf(m_pFile,"%s",buffer);
			if (buffer[0] == '*')
			{//
				startState = -1;
				endState = -1; 

				disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(1));
				nbList = disNode->getTable()->getData();
				for (int i = 0; i < m_iNbState; i++)
				{
					tmpIndex = i*(m_iNbAction*m_iNbState) + currAction*m_iNbState;
					for (int j = 0; j < m_iNbState; j++)
					{
						fscanf(m_pFile,"%f",&tmpProb);
						nbList.at(tmpIndex + j) = tmpProb;
					}
				}
				disNode->getTable()->setData(nbList);
			}
			else
			{	
				currAction2 = atoi(buffer);
				disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(1));
				nbList = disNode->getTable()->getData();

				for (int i = 0; i < m_iNbState; i++)
				{					
					tmpIndex =  currAction2*m_iNbState*m_iNbAction*m_iNbState 
						+ i*m_iNbAction*m_iNbState + currAction*m_iNbState;
					for (int j = 0; j < m_iNbState; j++)
					{
						fscanf(m_pFile,"%f",&tmpProb);
						nbList.at(tmpIndex + j) = tmpProb;
					}
				}
				disNode->getTable()->setData(nbList);
			}			
			break;
		case 'R':
			fscanf(m_pFile,"%i",&currAction);
			fscanf(m_pFile,"%s",buffer);
			if (buffer[0] == '*')
			{//
				startState = -1;
				endState = -1; 

				disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(1));
				nbList = disNode->getTable()->getData();
				for (int i = 0; i < m_iNbState; i++)
				{					
					tmpIndex = i*(m_iNbAction) + currAction;
					fscanf(m_pFile,"%f",&tmpProb);
					nbList.at(tmpIndex) = tmpProb;				
				}
				disNode->getTable()->setData(nbList);
			}
			else
			{	
				currAction2 = atoi(buffer);
				disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(1));
				nbList = disNode->getTable()->getData();
				for (int i = 0; i < m_iNbState; i++)
				{					
					tmpIndex = currAction2*m_iNbState*m_iNbAction + i*m_iNbAction + currAction; 
					fscanf(m_pFile,"%f",&tmpProb);
					nbList.at(tmpIndex) = tmpProb;				
				}
				disNode->getTable()->setData(nbList);
			}			
			break;
		default:
			break;
		}	//switch	

	}//while
}

Domain* CBasDID::LoadCPT(bool zmbFlag)
{
	//file header
	LoadFileHeader();	
	//set nodes states number
	SetNodeState(zmbFlag);	//???  zmbFlag  what does it mean??? 
	//the first decision,but it is the last slice,E.X. [S3,O3,D3,U3];
	SetLastSlice();
	SetSecondLastSlice2();
	CopyPreviousSlices();	
	return m_pDomain;
}

void CBasDID::SetNodeState(bool ODFlag)
{
	DiscreteNode* disNode;
	int i;
	//s	
	for (i = 0; i< m_iNbSteps; i++)
	{
		disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(i));
		setNumberOfStatesEx(disNode,m_iNbState);
	}

	//o
	for (i = 0; i< m_iNbSteps; i++)
	{
		disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(i));
		setNumberOfStatesEx(disNode,m_iNbObs);
	}

	//D
	for (i = 0; i< m_iNbSteps; i++)
	{
		disNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(i));
		setNumberOfStatesEx(disNode,m_iNbAction);
	}
}

void CBasDID::LoadFileHeader()
{
	char buffer[BUFSIZ];

	//file header.
	while (fscanf(m_pFile, "%s", buffer) != EOF)
	{
		if (!strcmp(buffer,"FRAME"))
		{
			fgets(buffer,BUFSIZ,m_pFile);
			continue;   
		}
		else if (!strcmp(buffer,"STATES"))
		{
			fscanf(m_pFile, "%s", buffer);
			fscanf(m_pFile, "%i", &m_iNbState);
			continue;
		}
		else if (!strcmp(buffer,"OBSERVATIONS"))
		{
			fscanf(m_pFile, "%s", buffer);
			fscanf(m_pFile, "%i", &m_iNbObs);
			continue;
		}
		else if (!strcmp(buffer,"ACTIONS"))
		{
			fscanf(m_pFile, "%s", buffer);
			fscanf(m_pFile, "%i", &m_iNbAction);
			continue;
		}
		else if (!strcmp(buffer,"OTHERS_ACTIONS"))
		{
			fscanf(m_pFile, "%s", buffer);
			int tmp;
			fscanf(m_pFile, "%i", &tmp);
			continue;
		}
		else if (!strcmp(buffer,"FRAMES"))
		{
			fgets(buffer,BUFSIZ,m_pFile);
			continue;
		}
		else if (!strcmp(buffer,"LEVELS"))
		{
			fgets(buffer,BUFSIZ,m_pFile);
			continue;
		}
		else
		{	
			fgets(buffer,BUFSIZ,m_pFile);
			break;
		}
	}//while
}

void CBasDID::CopyPreviousSlices()
{
	//the last slice is special,
	//the second last slice has been initialized,
	//the rest are as the same as the second last one.
	NumberList nbListSrcS,nbListSrcU,nbListSrcO,nbListDes;
	DiscreteNode *SNode,*UNode,*ONode;
	SNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(1));
	UNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(1));
	ONode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(1));
	nbListSrcS = SNode->getTable()->getData();
	nbListSrcU = UNode->getTable()->getData();
	nbListSrcO = ONode->getTable()->getData();

	for (int i = 2; i < m_iNbSteps; i++)
	{
		SNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(i));
		UNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(i));
		ONode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(i));
		SNode->getTable()->setData(nbListSrcS);
		UNode->getTable()->setData(nbListSrcU);
		ONode->getTable()->setData(nbListSrcO);
	}
	UNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrUNode.at(0));
	UNode->getTable()->setData(nbListSrcU);
}

int CBasDID::GetBasDIDPara()
{
	if(_IDID_DEBUG_)
		cout<<"Inside CBasDID::GetBasDIDPara().."<<endl;
	Node* pNode = NULL;

	if(_IDID_DEBUG_){
		cout<<"Here..3b"<<endl;
		cout<<m_arrSNode[0]<<endl;
	}

	pNode = getNodeByLabel(m_pDomain,m_arrSNode[0]);
	if(_IDID_DEBUG_)
		cout<<pNode->getLabel()<<endl;

	m_iNbState = static_cast<int>(((DiscreteNode*)pNode)->getNumberOfStates()); 

	if(_IDID_DEBUG_)
		cout<<"Here..3c"<<endl;

	pNode = getNodeByLabel(m_pDomain,m_arrDNode[0]);
	m_iNbAction = static_cast<int>(((DiscreteNode*)pNode)->getNumberOfStates()); 

	if(_IDID_DEBUG_)
		cout<<"Here..3d"<<endl;

	pNode = getNodeByLabel(m_pDomain,m_arrONode[0]);
	m_iNbObs = static_cast<int>(((DiscreteNode*)pNode)->getNumberOfStates()); 

	if(_IDID_DEBUG_)
		cout<<"Here..3e"<<endl;
	return 0;
}

CSequenceTree* CBasDID::SolveDID()
{
	//pre-allocate the space for the tree and some other initialization
	CSequenceTree* pPolicyTree = new CSequenceTree(m_iNbSteps,m_iNbObs,m_iNbAction);
	
	vector<int>& arrEvi = pPolicyTree->GetEviArr();
	vector<int>& arrObs = pPolicyTree->GetObsArr();
	vector<int>& arrAnc = pPolicyTree->GetAncArr();
	vector<int>& arrAct = pPolicyTree->GetActArr();

	int TreeSize = pPolicyTree->GetSizeofTree();
	int arrActCur = 0;

	//first decision
	size_t rootAct;
	rootAct = getBestChoice(m_pDomain,m_arrDNode.at(0));
	setNodeState(m_pDomain,m_arrDNode.at(0),rootAct);
	m_pDomain->propagate();
	arrAct.at(arrActCur) = rootAct;
	arrActCur++;

	vector<pair<int,int> > arrPair;
	float *DNodeUtility = new float[m_iNbAction];

	for (int i = 1; i < TreeSize; i++)
	{
		m_pDomain->initialize();		
		arrPair.clear();

		int currAnc = arrAnc[i];
		pair<int,int> tmpPair;
		arrPair.push_back(make_pair(arrEvi[i],arrObs[i]));		
		while(currAnc > 0)
		{
			arrPair.push_back(make_pair(arrEvi[currAnc],arrObs[currAnc]));
			arrPair.push_back(make_pair(arrAct[currAnc],arrObs[currAnc]));
			currAnc = arrAnc[currAnc];
		};
		size_t len = arrPair.size();
		tmpPair = arrPair.at(0);
		string str;
		str = m_arrONode.at(tmpPair.second);
		setNodeState(m_pDomain,str,tmpPair.first);

		for (size_t ii = 1; ii<len; ii=ii+2)
		{
			tmpPair = arrPair.at(ii);
			string str = m_arrONode.at(tmpPair.second);
			setNodeState(m_pDomain,str,tmpPair.first);
		}
		for (size_t ii = 2; ii<len; ii=ii+2)
		{
			tmpPair = arrPair.at(ii);
			string str = m_arrDNode.at(tmpPair.second);
			setNodeState(m_pDomain,str,tmpPair.first);
		}
		setNodeState(m_pDomain,m_arrDNode.at(0),rootAct);


		m_pDomain->propagate();

		getNodeUtilities(m_pDomain,m_arrDNode.at(arrObs[i]),&DNodeUtility,m_iNbAction);

		int ind = findMax(DNodeUtility,m_iNbAction); //the (ind)th action.
		//cout<<i<<"--<"<<ind<<">-"<<m_arrDNode.at(arrObs[i])<<":___[";
		//m_of<<i<<"--<"<<ind<<">-"<<m_arrDNode.at(arrObs[i])<<":___[";
		/*for (int j = 0; j < m_iNbAction; j++)
		{
		cout<<DNodeUtility[j]<<",";
		m_of<<DNodeUtility[j]<<",";
		}*/
		//cout<<"]\n";
		/*if (!(i%1000))
		{
		cout<<i<<"--<"<<ind<<"->"<<m_arrDNode.at(arrObs[i])<<">-\n";
		}*/

		arrAct[i] = ind;				
	}
	delete[] DNodeUtility;

	pPolicyTree->SetActArr(arrAct);
	pPolicyTree->SetAncArr(arrAnc);
	pPolicyTree->SetObsArr(arrObs);
	pPolicyTree->SetEviArr(arrEvi);

	return pPolicyTree;
}



//the main idea is: 
//	0. load the file. get the vector<distribution>
//	1. set the distribution
//	2. solve it as usual, and get a tree
//	3. throw this tree into forest.(tree container)
CForest* CBasDID::SolveDID( string paraFile, CForest** ppForest,Merge_Method method)
{
	if (!IsFileExist(paraFile))
	{
		cout<<"ERROR:[BasDID] ---- where is the parameter file? \n";
		return *ppForest;
	}
	
	clock_t start,final;
	start = clock();

	switch (method)
	{
	case NO_Merge:   //return a normal forest
		(*ppForest) = new CNormalForest();
		(*ppForest)->SetNbofLayer(m_iNbSteps);
		(*ppForest)->SetNbofBranch(m_iNbObs);
		break;
	case BE:
		*ppForest = new CNormalForest();
		(*ppForest)->SetNbofLayer(m_iNbSteps);
		(*ppForest)->SetNbofBranch(m_iNbObs);
		break;
	case E_BE:
		*ppForest = new CNormalForest();
		(*ppForest)->SetNbofLayer(m_iNbSteps);
		(*ppForest)->SetNbofBranch(m_iNbObs);
		break;
	case DMU:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	case E_DMU:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	case AE:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	case E_AE:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	default:
		cout<<"Warning:[BasDID] ---- unknown merge method \n";
	}

	//step 0:-----
	ifstream ifile(paraFile.c_str());
	string str;
	vector<string> arrStr;
	vector<float> arrFlt;
	vector<vector<float> >* pArrDist = (*ppForest)->GetPhyDists();
	size_t treeInd = 0;
	vector<int>& rArrWeight = (*ppForest)->GetWeight();
	if (ifile.is_open())
	{
		while (!ifile.eof())
		{
			getline(ifile,str);
			arrStr.clear();
			stringsplit(str,arrStr,",");
			arrFlt.clear();
			str2num(arrStr,arrFlt);
			pArrDist->push_back(arrFlt);
			rArrWeight.push_back(treeInd++);   //			
		}
	}
	ifile.close();

	if (method >= E_BE )
	{
		(*ppForest)->DisCombination(Epsilon_Dis,_IDID_EPSILON_);
		pArrDist = (*ppForest)->GetCombinedDists();		
		cout<<"\tThe physical distributions have been combined from "<<rArrWeight.size()<<
			" to "<<pArrDist->size()<<" \n";
	}

	CSequenceTree* pTree;
	for (size_t i = 0; i < pArrDist->size(); i++)
	{

		//reset
		m_pDomain->initialize();
		//step - 1		x
		ChangePhysicalBelief(pArrDist->at(i));

		//step - 2
		pTree = SolveDID();

		//step - 3;
		(*ppForest)->AddTree(pTree,i,method);
		
		if (method == DMU)
		{				
			delete pTree;
		}		
		//we don't need to delete pTree, here. if 
		//since, the normal forest, just contains the pointers of trees.
		//If we delete the pTree, the tree in forest will be deleted,too.
		//but we need to delete it if the method is DMU.
	}
	m_pDomain->initialize();
	
	final = clock();
	cout<<"\tThe time consumed for SolveDID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s.\n";

	return *ppForest;
}	

//FOR BISIMULATION
//the main idea is:
//	0. load the file. get the vector<distribution>
//	1. set the distribution
//	2. solve it as usual, and get a tree
//	3. throw this tree into forest.(tree container)
CForest* CBasDID::SolveDID(string paraFile, CForest** ppForest, vector<CSequenceTree*> pTrees, Merge_Method method)
{
	if (!IsFileExist(paraFile))
	{
		cout<<"ERROR:[BasDID] ---- where is the parameter file? \n";
		return *ppForest;
	}

//	clock_t start,final;
//	start = clock();

	switch (method)
	{
	case NO_Merge:   //return a normal forest
		(*ppForest) = new CNormalForest();
		(*ppForest)->SetNbofLayer(m_iNbSteps);
		(*ppForest)->SetNbofBranch(m_iNbObs);
		break;
	case BE:
		*ppForest = new CNormalForest();
		(*ppForest)->SetNbofLayer(m_iNbSteps);
		(*ppForest)->SetNbofBranch(m_iNbObs);
		break;
	case E_BE:
		*ppForest = new CNormalForest();
		(*ppForest)->SetNbofLayer(m_iNbSteps);
		(*ppForest)->SetNbofBranch(m_iNbObs);
		break;
	case DMU:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	case E_DMU:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	case AE:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	case E_AE:
		*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
		break;
	default:
		cout<<"Warning:[BasDID] ---- unknown merge method \n";
	}

	//step 0:-----
	ifstream ifile(paraFile.c_str());
	string str;
	vector<string> arrStr;
	vector<float> arrFlt;
	vector<vector<float> >* pArrDist = (*ppForest)->GetPhyDists();
	size_t treeInd = 0;
	vector<int>& rArrWeight = (*ppForest)->GetWeight();
	if (ifile.is_open())
	{
		while (!ifile.eof())
		{
			getline(ifile,str);
			arrStr.clear();
			stringsplit(str,arrStr,",");
			arrFlt.clear();
			str2num(arrStr,arrFlt);
			pArrDist->push_back(arrFlt);
//			rArrWeight.push_back(treeInd++);   //
		}
	}
	ifile.close();

	if (method >= E_BE )
	{
		(*ppForest)->DisCombination(Epsilon_Dis,_IDID_EPSILON_);
		pArrDist = (*ppForest)->GetCombinedDists();
		cout<<"\tThe physical distributions have been combined from "<<rArrWeight.size()<<
			" to "<<pArrDist->size()<<" \n";
	}

//	cout<<"pArrDist->size(): "<<pArrDist->size()<<" = "<<pTrees.size()<<endl;
	CSequenceTree* pTree;
	for (size_t i = 0; i < pTrees.size(); i++)
	{
		rArrWeight.push_back(treeInd++);

		//reset
//		m_pDomain->initialize();
		//step - 1		x
//		ChangePhysicalBelief(pArrDist->at(i));

		//step - 2
//		pTree = SolveDID();
//		pTree = pTrees[i];

		//step - 3;
		(*ppForest)->AddTree(pTrees[i],i,method);

//		if (method == DMU)
//		{
//			delete pTrees[i];
//		}
		//we don't need to delete pTree, here. if
		//since, the normal forest, just contains the pointers of trees.
		//If we delete the pTree, the tree in forest will be deleted,too.
		//but we need to delete it if the method is DMU.
	}
	m_pDomain->initialize();

//	final = clock();
//	cout<<"\tThe time consumed for SolveDID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s.\n";

	return *ppForest;
}

//Step 3
//CForest* CBasDID::SolveDID(CForest** ppForest, vector<CSequenceTree*> pTrees, Merge_Method method)
//{
//
//
//	clock_t start,final;
//	start = clock();
////	cout<<"im inside step 3"<<endl;
//	*ppForest = new CGraphForest(m_iNbSteps,m_iNbObs,m_iNbAction);
////	size_t modelCount = 0;
//	size_t treeInd = 0;
//	vector<int>& rArrWeight = (*ppForest)->GetWeight();
//
//	for (size_t i = 0; i < pTrees.size(); i++)
//	{
//
//		rArrWeight.push_back(treeInd++);
////		cout<<"im inside step 3-"<<i<<"-"<<pTrees[i]<<endl;
////		for(int j=0;j<mapArrAg.at(i).size();j++){
//			//step - 3;
////			cout<<"im inside step 3-"<<i<<"-"<<j<<endl;
//			(*ppForest)->AddTree(pTrees[i],i,DMU);
////			cout<<"im inside step 3-"<<i<<": Added tree"<<endl;
////			modelCount++;
//
////			delete pTrees[i];
//
//			//we don't need to delete pTree, here. if
//			//since, the normal forest, just contains the pointers of trees.
//			//If we delete the pTree, the tree in forest will be deleted,too.
//			//but we need to delete it if the method is DMU.
////		}
//	}
//	m_pDomain->initialize();
//
//	final = clock();
//	cout<<"\tThe time consumed for SolveDID is "<<((float)(final-start))/CLOCKS_PER_SEC<<"s.\n";
//
//	return *ppForest;
//}

// Steps 1 and 2
CSequenceTree* CBasDID::SolveDID(vector<float> sbp)
{
	clock_t start,final;
	start = clock();

	CSequenceTree* pTree;
	//reset
	m_pDomain->initialize();
	//step - 1		x
	ChangePhysicalBelief(sbp);

	//step - 2
	pTree = SolveDID();

//	cout<<"\tSolved model "<<endl;

	return pTree;
}

CGraphForest& CBasDID::SolveDIDDMU( string paraFile, CGraphForest& forest)
{
	if (!IsFileExist(paraFile))
	{
		cout<<"ERROR:[BasDID] ---- where is the parameter file? \n";
		return forest;
	}

	//step 0:-----
	ifstream ifile(paraFile.c_str());
	string str;
	vector<string> arrStr;
	vector<float> arrFlt;
	vector<vector<float> >* pArrDist = forest.GetPhyDists();
	size_t treeInd = 0;
	vector<int>& rArrWeight = forest.GetWeight();
	if (ifile.is_open())
	{
		while (!ifile.eof())
		{
			getline(ifile,str);
			arrStr.clear();
			stringsplit(str,arrStr,",");
			arrFlt.clear();
			str2num(arrStr,arrFlt);
			pArrDist->push_back(arrFlt);
			rArrWeight.push_back(treeInd++);   //			
		}
	}
	ifile.close();
	//m_arrDistri=DisCombination(m_arrDistri,m_arrStrDist,NO_Combine,0.07);

	CSequenceTree* pTree;
	for (size_t i = 0; i < pArrDist->size(); i++)
	{
		//reset
		m_pDomain->initialize();
		//step - 1		
		ChangePhysicalBelief(pArrDist->at(i));

		//step - 2
		pTree = SolveDID();

		//step - 3;
		forest.AddTree(pTree,i,DMU);
		//we need to delete pTree, here.
		delete pTree;
	}
	m_pDomain->initialize();
	return forest;
}	

int CBasDID::ChangePhysicalBelief(vector<float>& fltArr)
{
	DiscreteNode* pNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode[0]);  
	NumberList nbList = pNode->getTable()->getData();
	m_pDomain->uncompile();
	for (size_t i = 0; i < fltArr.size(); i++){
		if(_IDID_DEBUG_)
			cout<<fltArr.at(i)<<" ";
		nbList.at(i) = fltArr.at(i);
	}
	if(_IDID_DEBUG_)
		cout<<endl;
	pNode->getTable()->setData(nbList);

	m_pDomain->compile();
	m_pDomain->propagate();
	//m_pDomain->saveAsNet("c:\\xxx.net");
	return 0;
}

vector<int>& CBasDID::GetTrace(vector<int>& trace)
{
	int currAct,nbState = 0;	
	float* pUtility = new float[m_iNbAction];
	float* pBrief = new float[m_iNbObs];
	DiscreteNode *pDNode,*pSNode,*pONode;
	
	m_pDomain->initialize();
		
	//step -1, s[n], no observation
	pDNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(0));
	currAct = getBestChoice(pDNode);
	if (_IDID_OUTPUT_)
		//The [0/m_iNbSteps] D node's state is currAct
		cout<<"The [0/"<<m_iNbSteps<<"] D node's state is "<<currAct<<".\n";
	pDNode->selectState(currAct);
	m_pDomain->propagate();
	trace.push_back(currAct);

	for(int i = 1; i < m_iNbSteps; i++)
	{		
		//s[i]
		pSNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrSNode.at(i));
		getNodeBelief(pSNode,&pBrief,nbState);
		currAct = throwDice(pBrief,nbState);
		if (_IDID_OUTPUT_)
			//The [i/m_iNbSteps] S node's state is currAct
			cout<<"The ["<<i<<"/"<<m_iNbSteps<<"] S node's state is "<<currAct<<".\n";
		pSNode->selectState(currAct);
		m_pDomain->propagate();

		//o[i]
		pONode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrONode.at(i));
		getNodeBelief(pONode,&pBrief,nbState);
		currAct = throwDice(pBrief,nbState);
		if (_IDID_OUTPUT_)
			//The [0/m_iNbSteps] O node's state is currAct
			cout<<"The ["<<i<<"/"<<m_iNbSteps<<"] O node's state is "<<currAct<<".\n";
		pONode->selectState(currAct);
		
		//-s[i]
		pSNode->retractFindings();
		m_pDomain->propagate();

		//d[i]
		pDNode = (DiscreteNode*)getNodeByLabel(m_pDomain,m_arrDNode.at(i));		
		currAct = getBestChoice(pDNode);
		if (_IDID_OUTPUT_)
			//The [0/m_iNbSteps] D node's state is currAct
			cout<<"The ["<<i<<"/"<<m_iNbSteps<<"] D node's state is "<<currAct<<".\n";
		pDNode->selectState(currAct);
		trace.push_back(currAct);		
	}

	delete[] pUtility;
	delete[] pBrief;
	return trace;
}

vector<int>& CBasDID::GetTrace( vector<int>& trace, vector<float>& dist )
{
	ChangePhysicalBelief(dist);
	GetTrace(trace);
	return trace;
}

int CBasDID::getSerialRewardsIndex(int stateIndex, int actionIndex){
	return stateIndex * m_iNbAction + actionIndex;
}
