#pragma once
#include "IDID.h"
#include <sstream> 

//accessroy.h
//common functions that are being or could be used by more than 1 class, 
//it will be nicer if we can move some of them to a class without any redundency

//return the biggest element's index in a given array
int findMax(float *arr,int len);

//generate the array of labels of various nodes.
int InitNodeLabels(vector<string>& arrNode, string prefix,int steps);

//string split base on a given separator,default separator is "white space "
vector<string>& stringsplit(const string& str, vector<string>& tokens, 
							const string& delimiters = " ");

//check whether the file exist?  true - OK
bool IsFileExist(string fileName);

//the return is as the same as fltArr;
vector<float>& str2num(vector<string>& strArr,vector<float>& fltArr);

//given 2 vectors, merge them and remove the same ones.
void distingush(vector<int>& arr1, vector<int>& arr2, vector<int>& diff);

//given an array of actions, find indice where the actions are the same
//e.g.  input:  0 1 2 2 
//		ouput  <2,3> and 2
//it means that the actions in <2,3> are the same, we can use <2> to represent them.
//return a boolean result to indicate whether exist the possibility to combine 
bool AEIndex(vector<int>& arrAct,vector<int>& indice);

//find the indice of variables whose value are the same in a vector
//here, the first [int]s can be replaced by <Template T>.  a template function will be better
void FindIndice(vector<int>& arr, int value, vector<int>& indice);

//Euclid distance 
float cal_Distance(vector<float>& item1,vector<float>& item2);

//
vector<int>& select_k_items(vector<float> dis,int k,int item_size,vector<int>& k_item);

//we can treat each vector as a point in the space, this is why  the variable name is ***Point;
vector <float>& cal_mean(vector<float>& srcPoint, vector<float>& newPoint, int weight);

//**************vector******************
//I belive that some of the following functions have been defined in <algorithm> already. 
//point muliply
vector<float>& vec_PMultiply(vector<float>& arr,float ratio);
void arr_PMultiply(float* arrO,float* arrD, float ratio,int num);

//sum of all of elements in the arr
float vec_Sum(vector<float>& arr);
//sum of part of elements in the arr,
float vec_Sum(vector<float>& arr, vector<int>& indice);
//mean value of a vector
float vec_Mean(vector<float>& arr);
//res[i] = res[i] + arr[i]; so called "point add" ^-^
vector<float>& vec_PAdd(vector<float>& res,vector<float>& arr);
//remove the specific one from the arr 
vector<float>& vec_EraseItem(vector<float>& arr, int index);

//get a sub-vector from a original vector arr, base on the indice given
//template function, the declaration and implementation are put together, otherwise, there is an error about file dependency
template <class T>
vector<T>& vec_SubVector(vector<T>& arr, vector<int> indice)
{
	int ed = static_cast<int>(indice.size())-1;
	for (int i = static_cast<int>(arr.size()-1); i >= 0; i-- )
	{
		if (ed >= 0 && i == indice.at(ed))  //this node will be reserved
			ed--;
		else
			arr.erase(arr.begin()+i);		
	}
	return arr;
}

//when the models will be combined, the array of weights need be updated. 
vector<int>& vec_WeightUpdate(vector<int>& weight,vector<int>& indice);
vector<int> vec_FindSubVec(vector<int>& weight, int index);


template <class T>
int vec_FindTimes(vector<T>& arr,T item)
{
	int times = 0;
	for (size_t i = 0; i < arr.size(); ++i)
	{
		if (arr.at(i) == item)
		{
			++times;
		}
	}
	return times;
}
//******************vector***************************

//sample a state, from its distribution with a random number.
int throwDice(float* dist, int len);
