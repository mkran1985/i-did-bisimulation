/*
 * convex_hull_containment.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: AIUGA\mkran
 */

// Example: find the points that contribute to a convex combination
#include <cassert>
#include <vector>
#include "IDID.h"
#include <CGAL/Cartesian_d.h>
#include <CGAL/MP_Float.h>
#include "solve_convex_hull_containment_lp.h"
typedef CGAL::Cartesian_d<double> Kernel_d;
typedef Kernel_d::Point_d Point_d;
typedef CGAL::Quadratic_program_solution<CGAL::MP_Float> Solution;
using namespace std;

bool lies_in_convex_hull(Point_d queryPoint, vector<Point_d> points){
	Solution s = solve_convex_hull_containment_lp(queryPoint, points.begin(), points.end(), CGAL::MP_Float());
	if(_IDID_DEBUG_) cout << queryPoint;
	if (s.is_infeasible()){
		if(_IDID_DEBUG_)
			cout << " is not in the convex hull"<<endl;;
		return false;
	}else{
		assert (s.is_optimal());
		if(_IDID_DEBUG_){
			cout << " is a convex combination of the points ";
			Solution::Index_iterator it = s.basic_variable_indices_begin();
			Solution::Index_iterator end = s.basic_variable_indices_end();
			for (; it != end; ++it) cout << *it << " ";
			cout << endl;
		}
		return true;
	}
}

Point_d createPoint_d(int dimension, vector<double> point){
	return Point_d(dimension,point.begin(),point.end());
}

int chmain()
{
	vector<Point_d > points;
	const int D = 2;

	vector<double> pt1;
		pt1.push_back(1.0);
		pt1.push_back(0.0);
//		pt1.push_back(0.0);
		points.push_back(createPoint_d(D,pt1));

	vector<double> pt2;
		pt2.push_back(0.5);
		pt2.push_back(0.5);
//		pt2.push_back(0.34);
		points.push_back(createPoint_d(D,pt2));

	vector<double> pt3;
		pt3.push_back(0.75);
//		pt3.push_back(0.0);
		pt3.push_back(0.25);
		points.push_back(createPoint_d(D,pt3));

	vector<double> pt;
	pt.push_back(0.51);
//	pt.push_back(0.0);
	pt.push_back(0.49);
	Point_d p = createPoint_d(D,pt);

	lies_in_convex_hull(p,points);

  return 0;
}
