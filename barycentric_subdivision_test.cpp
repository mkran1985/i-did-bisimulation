/*
 * barycentric_subdivision_test.cpp
 *
 *  Created on: Feb 17, 2017
 *      Author: AIUGA\mkran
 */

#define CGAL_EIGEN3_ENABLED
#if defined(__GNUC__) && defined(__GNUC_MINOR__) && (__GNUC__ <= 4) && (__GNUC_MINOR__ < 4)
#include <iostream>
int main()
{
  cerr << "NOTICE: This test requires G++ >= 4.4, and will not be compiled." << std::endl;
}
#else

#include <CGAL/Triangulation_data_structure.h>
#include <CGAL/internal/Combination_enumerator.h>
#include <CGAL/Epick_d.h>
#include <CGAL/point_generators_d.h>
#include <CGAL/Triangulation.h>
#include <CGAL/algorithm.h>
#include <CGAL/assertions.h>
#include <iostream>
#include <iterator>
#include <vector>
#include <CGAL/Cartesian_d.h>
#include "IDID.h"
using namespace std;

typedef float											FT;
typedef CGAL::Epick_d<CGAL::Dynamic_dimension_tag>		K;
typedef K::Point_d 										Point_d;
typedef CGAL::Triangulation<K>							Triangulation;

Point_d barycenter(int max_dimension, int current_dimension, const vector<typename Triangulation::Vertex_handle> vertices){

	vector<FT> dims;
	for(int i = 0; i <= max_dimension; i++){
		dims.push_back(0.0);
	}

	FT norm = 0;
	if(_IDID_DEBUG_) cout<<"current dimension is "<< current_dimension<<endl;

	for( int i = 0; i <= current_dimension; i++){
		if(_IDID_DEBUG_) cout<<"vertex "<<i<<" is "<< vertices.at(i)->point()<<endl;

		Point_d it = (Point_d) (vertices.at(i)->point());
		if(_IDID_DEBUG_) cout<<"point "<<i<<" is "<< it<< "of size "<< it.size()<<endl;
		for(int j=0; j<it.size();j++){
			dims[j] += it[j];
		}
		norm += 1;
	}

	Point_d v = Point_d(dims.begin(),dims.end());
	if(_IDID_DEBUG_){
		cout<<"V: "<<v<<endl;
		cout<<"Norm: "<<norm<<endl;
		for (auto ddim = dims.begin(); ddim != dims.end(); ++ddim)
			cout<<"dimsval: "<<*ddim<<endl;
	}

	vector<FT> dims2;
	for(int i=0;i<=max_dimension;i++){
		dims[i] /= norm;
	}

	Point_d barycenter = Point_d(dims.begin(),dims.end());

	if(_IDID_DEBUG_) cout<<barycenter<<endl;

	return barycenter;
}

/*
 * The main goal of this function is to find a full cell that
 * contains a given set of vertices |face_vertices|. Then, it
 * builds a corresponding |face|.
 */
void find_face_from_vertices(const Triangulation & T,
        const std::vector<typename Triangulation::Vertex_handle> & face_vertices,
        typename Triangulation::Face & face){

    typedef typename Triangulation::Vertex_handle           Vertex_handle;
    typedef typename Triangulation::Full_cell_handle        Full_cell_handle;
    typedef typename Triangulation::Full_cell::Vertex_handle_iterator Vertex_h_iterator;

    // get the dimension of the face we want to build
    std::size_t fdim(face_vertices.size() - 1);
    if( fdim <= 0) exit(-1);

    // find all full cells incident to the first vertex of |face|
    typedef std::vector<Full_cell_handle> Cells;
    Cells cells;
    std::back_insert_iterator<Cells> out(cells);
    T.incident_full_cells(face_vertices[0], out);

    // Iterate over the cells to find one which contains the face_vertices
    for( typename Cells::iterator cit = cells.begin(); cit != cells.end(); ++cit){
        // find if the cell *cit contains the Face |face|
        std::size_t i = 0;
        for( ; i <= fdim; ++i ) {
            Vertex_handle face_v(face_vertices[i]);
            bool found(false);
            Vertex_h_iterator vit = (*cit)->vertices_begin();
            for( ; vit != (*cit)->vertices_end(); ++vit ) {
                if( *vit == face_v ) {
                    found = true;
                    break;
                }
            }
            if( ! found )
                break;
        }
        if( i > fdim ) {// the full cell |*cit| contains |face|
            face.set_full_cell(*cit);
            for(size_t i = 0; i <= fdim; ++i){
              face.set_index(static_cast<int>(i),(*cit)->index(face_vertices[i]));
            }
            return;
        }
    }
    cerr << "Could not build a face from vertices"<<std::endl;
    CGAL_assertion(false);
}

/*
 * This function builds the barycentric subdivision of a single
 * finite full cell |fc| from a full triangulation of points with coordinates.
 */
void barycentric_subdivision(Triangulation & T, Triangulation::Full_cell_handle fc){
//#define _IDID_DEBUG_  true
	typedef typename Triangulation::Vertex_handle Vertex_handle;
    typedef typename Triangulation::Face Face;
    const int dim = T.current_dimension();

    // First, read handles to the cell's vertices
    vector<Vertex_handle> vertices;
    vector<Vertex_handle> face_vertices;
    for( int i = 0; i <= dim; ++i ) vertices.push_back(fc->vertex(i));

    //First find the barycenter of the finite full cell
    Point_d bcenter = barycenter(dim,dim,vertices);
    if(_IDID_DEBUG_) cout <<"Here..." <<bcenter << endl;

    // Then, subdivide the cell |fc| once by inserting one vertex
    // (and associate the vertex to the barycenter).
    T.insert_in_full_cell(bcenter,fc);

    // From now on, we can't use the variable |fc|...
    // Then, subdivide faces of |fc| in order of decreasing dimension
    for( int d = dim-1; d > 0; --d )
    {
        face_vertices.resize(d+1);
        // The following class
        // enumerates all (d+1)-tuple of the set {0, 1, ..., dim}
        CGAL::internal::Combination_enumerator combi(d+1, 0, dim);
        while( ! combi.end() )
        {
            for( int i = 0; i <= d; ++i )
                face_vertices[i] = vertices[combi[i]];
            // we need to find a face with face_vertices
            Face face(dim);
            find_face_from_vertices(T, face_vertices, face);

            //Given the vertices of face in dimension d, find its barycenter
            Point_d bcenter_face = barycenter(dim,d,face_vertices);
            if(_IDID_DEBUG_) cout << bcenter_face << endl;

            //Associate this point to a vertex and insert this vertex into the face
            T.insert_in_face(bcenter_face,face);
            ++combi;
        }
    }
}

Triangulation triangulate(int dimension, vector<Point_d > points){
	Triangulation t(dimension);
	CGAL_assertion(t.empty());

	t.insert(points.begin(), points.end());
	CGAL_assertion(t.is_valid());

	return t;
}

vector<vector<Point_d> > subdivide(int dimension, vector<Point_d > points){
//#define _IDID_DEBUG_  true
	Triangulation t = triangulate(dimension,points);

	vector<vector<Point_d> > partition;

	//Sanity Check
	if(_IDID_DEBUG_){
		cout<<"[Before Subdivision] Sanity Check: "<<endl;
		std::cout<<t.number_of_vertices()<<std::endl;
		std::cout<<t.number_of_full_cells()<<std::endl;
		std::cout<<t.number_of_finite_full_cells()<<std::endl;
	}

	Triangulation::Full_cell_handle fc;
	for(Triangulation::Full_cell_iterator it=t.full_cells_begin();it!=t.full_cells_end();it++){
		if(! t.is_infinite(it)){
			if(_IDID_DEBUG_) cout<<"Finite Cell Found!!"<<endl;
			fc = it;
			break;
		}
	}

	barycentric_subdivision(t,fc);

	if(_IDID_DEBUG_){
		cout<<"[After Subdivision] Sanity Check: "<<endl;
		std::cout<<t.number_of_vertices()<<std::endl;
		std::cout<<t.number_of_full_cells()<<std::endl;
		std::cout<<t.number_of_finite_full_cells()<<std::endl;
	}

	if(_IDID_DEBUG_) cout<<"Output the coordinates of "<<t.number_of_finite_full_cells()<<" finite full cells resulting from the subdivision:"<<endl;
	for(Triangulation::Finite_full_cell_iterator it=t.finite_full_cells_begin(); it!=t.finite_full_cells_end(); it++){
		vector<Point_d> block;
		for(int k=0;k<dimension;k++){
			if(_IDID_DEBUG_) cout<<it->vertex(k)->point()<<endl;
			block.push_back(it->vertex(k)->point());
		}
		partition.push_back(block);
	}

	return partition;
}

int main(){
	const int D = 6;
	vector<Point_d > points_d;
    vector<vector<Point_d> > partition;

    //Initializing the points in the convex hull of the original simplex
	for(int kk = 0 ; kk<D; kk++){
		std::vector<float> dims;
		for(int kkk=0;kkk<D;kkk++){
			dims.push_back( kk==kkk ? 1.0 :0.0);
		}
		points_d.push_back(Point_d(dims.begin(),dims.end()));
	}

	partition = subdivide(D,points_d);
	cout<<partition.size()<<endl;

    return 0;
}
#endif






